---

excalidraw-plugin: parsed
tags: [excalidraw]

---
==⚠  Switch to EXCALIDRAW VIEW in the MORE OPTIONS menu of this document. ⚠==


# Text Elements
L ^wsLzNrAe

%%
# Drawing
```json
{
	"type": "excalidraw",
	"version": 2,
	"source": "https://excalidraw.com",
	"elements": [
		{
			"type": "rectangle",
			"version": 219,
			"versionNonce": 1757546968,
			"isDeleted": false,
			"id": "TVWVXrLkFZdrUi9mqJtvl",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 0,
			"opacity": 100,
			"angle": 0,
			"x": -374,
			"y": -251.2421875,
			"strokeColor": "#000000",
			"backgroundColor": "#40c057",
			"width": 528,
			"height": 14,
			"seed": 307961816,
			"groupIds": [],
			"strokeSharpness": "sharp",
			"boundElements": [],
			"updated": 1665930257080,
			"link": null,
			"locked": false
		},
		{
			"type": "rectangle",
			"version": 279,
			"versionNonce": 60694440,
			"isDeleted": false,
			"id": "y0XuFHeVFSBZIfLpVRCUq",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 0,
			"opacity": 100,
			"angle": 0,
			"x": -373,
			"y": -222.2421875,
			"strokeColor": "#000000",
			"backgroundColor": "#40c057",
			"width": 528,
			"height": 14,
			"seed": 2091074472,
			"groupIds": [],
			"strokeSharpness": "sharp",
			"boundElements": [],
			"updated": 1665930257080,
			"link": null,
			"locked": false
		},
		{
			"type": "rectangle",
			"version": 270,
			"versionNonce": 613617368,
			"isDeleted": false,
			"id": "95mSz1Qmh40f5ee23nycO",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 0,
			"opacity": 100,
			"angle": 0,
			"x": -372,
			"y": -160.2421875,
			"strokeColor": "#000000",
			"backgroundColor": "#40c057",
			"width": 528,
			"height": 14,
			"seed": 81930968,
			"groupIds": [],
			"strokeSharpness": "sharp",
			"boundElements": [],
			"updated": 1665930257080,
			"link": null,
			"locked": false
		},
		{
			"type": "rectangle",
			"version": 295,
			"versionNonce": 1829497512,
			"isDeleted": false,
			"id": "6F-TCzh12SuWD8EWjEHVx",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 0,
			"opacity": 100,
			"angle": 0,
			"x": -372,
			"y": -192.2421875,
			"strokeColor": "#000000",
			"backgroundColor": "#40c057",
			"width": 528,
			"height": 14,
			"seed": 1372360360,
			"groupIds": [],
			"strokeSharpness": "sharp",
			"boundElements": [],
			"updated": 1665930257080,
			"link": null,
			"locked": false
		},
		{
			"type": "rectangle",
			"version": 278,
			"versionNonce": 704345048,
			"isDeleted": false,
			"id": "-q449GbnRXpj8hgn1jguO",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 0,
			"opacity": 100,
			"angle": 0,
			"x": -373,
			"y": -131.2421875,
			"strokeColor": "#000000",
			"backgroundColor": "#40c057",
			"width": 528,
			"height": 14,
			"seed": 1551822808,
			"groupIds": [],
			"strokeSharpness": "sharp",
			"boundElements": [],
			"updated": 1665930257080,
			"link": null,
			"locked": false
		},
		{
			"type": "rectangle",
			"version": 220,
			"versionNonce": 478696872,
			"isDeleted": false,
			"id": "xzxwC9huHN3hJtBndiqws",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 0,
			"opacity": 100,
			"angle": 0,
			"x": -374,
			"y": -237.2421875,
			"strokeColor": "#000000",
			"backgroundColor": "#fd7e14",
			"width": 527,
			"height": 17,
			"seed": 139562968,
			"groupIds": [],
			"strokeSharpness": "sharp",
			"boundElements": [],
			"updated": 1665930257080,
			"link": null,
			"locked": false
		},
		{
			"type": "rectangle",
			"version": 182,
			"versionNonce": 252655832,
			"isDeleted": false,
			"id": "h3UtQygMVS98BR7fQArOH",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 0,
			"opacity": 100,
			"angle": 0,
			"x": -371.5,
			"y": -145.7421875,
			"strokeColor": "#000000",
			"backgroundColor": "#fd7e14",
			"width": 527,
			"height": 17,
			"seed": 1749457624,
			"groupIds": [],
			"strokeSharpness": "sharp",
			"boundElements": [],
			"updated": 1665930257081,
			"link": null,
			"locked": false
		},
		{
			"type": "rectangle",
			"version": 169,
			"versionNonce": 390029480,
			"isDeleted": false,
			"id": "vyfml_0kxEJRWAw4MizYS",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 0,
			"opacity": 100,
			"angle": 0,
			"x": -371.5,
			"y": -176.7421875,
			"strokeColor": "#000000",
			"backgroundColor": "#fd7e14",
			"width": 527,
			"height": 17,
			"seed": 421994152,
			"groupIds": [],
			"strokeSharpness": "sharp",
			"boundElements": [],
			"updated": 1665930257081,
			"link": null,
			"locked": false
		},
		{
			"type": "rectangle",
			"version": 259,
			"versionNonce": 123887064,
			"isDeleted": false,
			"id": "DbjhcUrtE-FoJHCTSNLfq",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 0,
			"opacity": 100,
			"angle": 0,
			"x": -373.5,
			"y": -207.7421875,
			"strokeColor": "#000000",
			"backgroundColor": "#fd7e14",
			"width": 527,
			"height": 17,
			"seed": 1197309912,
			"groupIds": [],
			"strokeSharpness": "sharp",
			"boundElements": [],
			"updated": 1665930257081,
			"link": null,
			"locked": false
		},
		{
			"type": "rectangle",
			"version": 277,
			"versionNonce": 1557990312,
			"isDeleted": false,
			"id": "RcmrlgnrAdqkcbbUkbjQc",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 0,
			"opacity": 100,
			"angle": 0,
			"x": -378,
			"y": -31.2421875,
			"strokeColor": "#000000",
			"backgroundColor": "#40c057",
			"width": 581,
			"height": 14,
			"seed": 1816332200,
			"groupIds": [],
			"strokeSharpness": "sharp",
			"boundElements": [],
			"updated": 1665930257081,
			"link": null,
			"locked": false
		},
		{
			"type": "rectangle",
			"version": 347,
			"versionNonce": 872997592,
			"isDeleted": false,
			"id": "6coHhdgZNlUqR4WBvIYqZ",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 0,
			"opacity": 100,
			"angle": 0,
			"x": -377,
			"y": -3.0755208333333712,
			"strokeColor": "#000000",
			"backgroundColor": "#40c057",
			"width": 580.5,
			"height": 14.833333333333314,
			"seed": 856729304,
			"groupIds": [],
			"strokeSharpness": "sharp",
			"boundElements": [],
			"updated": 1665930257081,
			"link": null,
			"locked": false
		},
		{
			"type": "rectangle",
			"version": 338,
			"versionNonce": 395138728,
			"isDeleted": false,
			"id": "tT89Uq0qYIHtSEP68nbDW",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 0,
			"opacity": 100,
			"angle": 0,
			"x": -376,
			"y": 58.091145833333314,
			"strokeColor": "#000000",
			"backgroundColor": "#40c057",
			"width": 577.1666666666667,
			"height": 15.666666666666687,
			"seed": 1087623848,
			"groupIds": [],
			"strokeSharpness": "sharp",
			"boundElements": [],
			"updated": 1665930257081,
			"link": null,
			"locked": false
		},
		{
			"type": "rectangle",
			"version": 357,
			"versionNonce": 1939261400,
			"isDeleted": false,
			"id": "01UhP7NKjfX03dYpM7Ftq",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 0,
			"opacity": 100,
			"angle": 0,
			"x": -376,
			"y": 27.7578125,
			"strokeColor": "#000000",
			"backgroundColor": "#40c057",
			"width": 576.3333333333333,
			"height": 15.666666666666629,
			"seed": 1158024152,
			"groupIds": [],
			"strokeSharpness": "sharp",
			"boundElements": [],
			"updated": 1665930257081,
			"link": null,
			"locked": false
		},
		{
			"type": "rectangle",
			"version": 336,
			"versionNonce": 1274823080,
			"isDeleted": false,
			"id": "tNLqxC4_3kjwD1-yFSU5A",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 0,
			"opacity": 100,
			"angle": 0,
			"x": -377,
			"y": 88.7578125,
			"strokeColor": "#000000",
			"backgroundColor": "#40c057",
			"width": 578.8333333333333,
			"height": 13.166666666666686,
			"seed": 2131967400,
			"groupIds": [],
			"strokeSharpness": "sharp",
			"boundElements": [],
			"updated": 1665930257081,
			"link": null,
			"locked": false
		},
		{
			"type": "rectangle",
			"version": 301,
			"versionNonce": 840961240,
			"isDeleted": false,
			"id": "FrQiDt1ffWwUCUAXxdtHH",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 0,
			"opacity": 100,
			"angle": 0,
			"x": -378,
			"y": -16.2421875,
			"strokeColor": "#000000",
			"backgroundColor": "#fd7e14",
			"width": 581,
			"height": 15,
			"seed": 486985944,
			"groupIds": [],
			"strokeSharpness": "sharp",
			"boundElements": [],
			"updated": 1665930257081,
			"link": null,
			"locked": false
		},
		{
			"type": "rectangle",
			"version": 388,
			"versionNonce": 156617896,
			"isDeleted": false,
			"id": "ABtT-VVRigRIDAl81eTFi",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 0,
			"opacity": 100,
			"angle": 0,
			"x": -376.33333333333326,
			"y": 75.09114583333331,
			"strokeColor": "#000000",
			"backgroundColor": "#fd7e14",
			"width": 577.8333333333333,
			"height": 13.666666666666686,
			"seed": 942491816,
			"groupIds": [],
			"strokeSharpness": "sharp",
			"boundElements": [],
			"updated": 1665930257081,
			"link": null,
			"locked": false
		},
		{
			"type": "rectangle",
			"version": 377,
			"versionNonce": 960016856,
			"isDeleted": false,
			"id": "9IXSDUybzBLjJq_24VloK",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 0,
			"opacity": 100,
			"angle": 0,
			"x": -377.16666666666663,
			"y": 43.25781249999994,
			"strokeColor": "#000000",
			"backgroundColor": "#fd7e14",
			"width": 577.8333333333334,
			"height": 14.5,
			"seed": 581367256,
			"groupIds": [],
			"strokeSharpness": "sharp",
			"boundElements": [],
			"updated": 1665930257081,
			"link": null,
			"locked": false
		},
		{
			"type": "rectangle",
			"version": 381,
			"versionNonce": 424015784,
			"isDeleted": false,
			"id": "RZjDCiwZhh4byYk5qET4x",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 0,
			"opacity": 100,
			"angle": 0,
			"x": -376.66666666666663,
			"y": 12.257812499999943,
			"strokeColor": "#000000",
			"backgroundColor": "#fd7e14",
			"width": 579,
			"height": 14,
			"seed": 2009985960,
			"groupIds": [],
			"strokeSharpness": "sharp",
			"boundElements": [],
			"updated": 1665930257081,
			"link": null,
			"locked": false
		},
		{
			"type": "freedraw",
			"version": 32,
			"versionNonce": 2146744024,
			"isDeleted": false,
			"id": "ypvsJ0lMRZmf7xvy7Vq1z",
			"fillStyle": "hachure",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 0,
			"opacity": 100,
			"angle": 0,
			"x": -375.83333333333326,
			"y": -114.36848958333326,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"width": 0.8333333333333712,
			"height": 0,
			"seed": 2024853976,
			"groupIds": [],
			"strokeSharpness": "round",
			"boundElements": [],
			"updated": 1665930257081,
			"link": null,
			"locked": false,
			"points": [
				[
					0,
					0
				],
				[
					-0.8333333333333712,
					0
				],
				[
					0,
					0
				]
			],
			"lastCommittedPoint": null,
			"simulatePressure": false,
			"pressures": [
				0.0048828125,
				0.076171875,
				0
			]
		},
		{
			"type": "freedraw",
			"version": 39,
			"versionNonce": 804831912,
			"isDeleted": false,
			"id": "IQYc1SeSSTC-fCDSskCHR",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 0,
			"opacity": 100,
			"angle": 0,
			"x": -375.83333333333326,
			"y": -113.53515624999994,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"width": 0.8333333333333712,
			"height": 24.166666666666686,
			"seed": 1586997672,
			"groupIds": [],
			"strokeSharpness": "round",
			"boundElements": [],
			"updated": 1665930257081,
			"link": null,
			"locked": false,
			"points": [
				[
					0,
					0
				],
				[
					0,
					0.8333333333333144
				],
				[
					0,
					3.3333333333333144
				],
				[
					0,
					5.833333333333314
				],
				[
					0,
					8.333333333333314
				],
				[
					0,
					11.666666666666686
				],
				[
					0,
					15
				],
				[
					-0.8333333333333712,
					19.166666666666686
				],
				[
					-0.8333333333333712,
					21.666666666666686
				],
				[
					-0.8333333333333712,
					23.333333333333314
				],
				[
					-0.8333333333333712,
					24.166666666666686
				],
				[
					-0.8333333333333712,
					24.166666666666686
				]
			],
			"lastCommittedPoint": null,
			"simulatePressure": false,
			"pressures": [
				0.021484375,
				0.2685546875,
				0.2705078125,
				0.271484375,
				0.2705078125,
				0.271484375,
				0.2724609375,
				0.271484375,
				0.2705078125,
				0.2568359375,
				0.2177734375,
				0
			]
		},
		{
			"type": "freedraw",
			"version": 39,
			"versionNonce": 2062031832,
			"isDeleted": false,
			"id": "zSzymD755BDVG3RNoMa4G",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 0,
			"opacity": 100,
			"angle": 0,
			"x": 156.66666666666663,
			"y": -114.36848958333326,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"width": 3.3333333333332575,
			"height": 25.83333333333337,
			"seed": 710124248,
			"groupIds": [],
			"strokeSharpness": "round",
			"boundElements": [],
			"updated": 1665930257081,
			"link": null,
			"locked": false,
			"points": [
				[
					0,
					0
				],
				[
					0,
					-0.8333333333333712
				],
				[
					0,
					0
				],
				[
					-0.8333333333332575,
					2.5
				],
				[
					-0.8333333333332575,
					5
				],
				[
					-0.8333333333332575,
					10
				],
				[
					-0.8333333333332575,
					14.166666666666629
				],
				[
					-1.6666666666666288,
					16.66666666666663
				],
				[
					-1.6666666666666288,
					19.16666666666663
				],
				[
					-1.6666666666666288,
					21.66666666666663
				],
				[
					-3.3333333333332575,
					25
				],
				[
					-3.3333333333332575,
					25
				]
			],
			"lastCommittedPoint": null,
			"simulatePressure": false,
			"pressures": [
				0.0087890625,
				0.2080078125,
				0.296875,
				0.3037109375,
				0.3115234375,
				0.31640625,
				0.31640625,
				0.31640625,
				0.314453125,
				0.3046875,
				0.1259765625,
				0
			]
		},
		{
			"type": "freedraw",
			"version": 46,
			"versionNonce": 471477672,
			"isDeleted": false,
			"id": "8KJGKGbrf0Kw1bFo6ySaY",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 0,
			"opacity": 100,
			"angle": 0,
			"x": -364.99999999999994,
			"y": -99.36848958333326,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"width": 11.666666666666686,
			"height": 15,
			"seed": 693254824,
			"groupIds": [],
			"strokeSharpness": "round",
			"boundElements": [],
			"updated": 1665930257081,
			"link": null,
			"locked": false,
			"points": [
				[
					0,
					0
				],
				[
					-0.8333333333333144,
					0.8333333333333144
				],
				[
					-3.3333333333333144,
					2.5
				],
				[
					-4.166666666666686,
					3.3333333333333144
				],
				[
					-5.833333333333314,
					5
				],
				[
					-7.5,
					5.833333333333314
				],
				[
					-9.166666666666686,
					6.666666666666629
				],
				[
					-10,
					7.5
				],
				[
					-10.833333333333314,
					7.5
				],
				[
					-11.666666666666686,
					8.333333333333314
				],
				[
					-11.666666666666686,
					9.166666666666629
				],
				[
					-10.833333333333314,
					10
				],
				[
					-10,
					10.833333333333314
				],
				[
					-8.333333333333314,
					11.666666666666629
				],
				[
					-6.666666666666686,
					13.333333333333314
				],
				[
					-5,
					13.333333333333314
				],
				[
					-3.3333333333333144,
					14.166666666666629
				],
				[
					-1.6666666666666856,
					15
				],
				[
					-0.8333333333333144,
					15
				],
				[
					-0.8333333333333144,
					15
				]
			],
			"lastCommittedPoint": null,
			"simulatePressure": false,
			"pressures": [
				0.0537109375,
				0.2890625,
				0.29296875,
				0.2939453125,
				0.2939453125,
				0.2939453125,
				0.294921875,
				0.294921875,
				0.2958984375,
				0.296875,
				0.2919921875,
				0.291015625,
				0.29296875,
				0.294921875,
				0.296875,
				0.2978515625,
				0.2978515625,
				0.302734375,
				0.2978515625,
				0
			]
		},
		{
			"type": "line",
			"version": 129,
			"versionNonce": 1814487256,
			"isDeleted": false,
			"id": "3LRy3QgzPRGBTMcQG_YuQ",
			"fillStyle": "hachure",
			"strokeWidth": 4,
			"strokeStyle": "solid",
			"roughness": 0,
			"opacity": 100,
			"angle": 0,
			"x": -371.66666666666663,
			"y": -93.53515624999994,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"width": 518.3333333333333,
			"height": 3.3333333333333144,
			"seed": 1387304664,
			"groupIds": [],
			"strokeSharpness": "round",
			"boundElements": [],
			"updated": 1665930257081,
			"link": null,
			"locked": false,
			"startBinding": null,
			"endBinding": null,
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": null,
			"points": [
				[
					0,
					0
				],
				[
					518.3333333333333,
					-3.3333333333333144
				]
			]
		},
		{
			"type": "freedraw",
			"version": 45,
			"versionNonce": 1993750696,
			"isDeleted": false,
			"id": "GB2JI7HFPyIAnAN-_1oSX",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 0,
			"opacity": 100,
			"angle": 0,
			"x": 137.5,
			"y": -105.20182291666663,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"width": 21.666666666666742,
			"height": 19.166666666666686,
			"seed": 1202012376,
			"groupIds": [],
			"strokeSharpness": "round",
			"boundElements": [],
			"updated": 1665930257081,
			"link": null,
			"locked": false,
			"points": [
				[
					0,
					0
				],
				[
					0,
					-0.8333333333333144
				],
				[
					0,
					0
				],
				[
					1.6666666666666288,
					0.8333333333333712
				],
				[
					4.166666666666629,
					3.3333333333333712
				],
				[
					6.666666666666629,
					4.166666666666686
				],
				[
					7.5,
					5
				],
				[
					9.166666666666629,
					5.833333333333371
				],
				[
					10,
					6.666666666666686
				],
				[
					11.666666666666629,
					7.5
				],
				[
					12.5,
					8.333333333333371
				],
				[
					13.333333333333371,
					8.333333333333371
				],
				[
					14.166666666666629,
					9.166666666666686
				],
				[
					15,
					9.166666666666686
				],
				[
					15.833333333333371,
					9.166666666666686
				],
				[
					15.833333333333371,
					10
				],
				[
					15,
					10
				],
				[
					14.166666666666629,
					10
				],
				[
					10.833333333333371,
					10.833333333333371
				],
				[
					8.333333333333371,
					13.333333333333371
				],
				[
					1.6666666666666288,
					15.833333333333371
				],
				[
					-2.5,
					17.5
				],
				[
					-5.833333333333371,
					18.33333333333337
				],
				[
					-5.833333333333371,
					18.33333333333337
				]
			],
			"lastCommittedPoint": null,
			"simulatePressure": false,
			"pressures": [
				0.0263671875,
				0.1826171875,
				0.2255859375,
				0.232421875,
				0.2421875,
				0.248046875,
				0.251953125,
				0.25390625,
				0.2578125,
				0.2607421875,
				0.2626953125,
				0.2646484375,
				0.2666015625,
				0.267578125,
				0.2705078125,
				0.26953125,
				0.3037109375,
				0.3046875,
				0.3046875,
				0.3046875,
				0.3017578125,
				0.27734375,
				0.1376953125,
				0
			]
		},
		{
			"type": "text",
			"version": 23,
			"versionNonce": 860161496,
			"isDeleted": false,
			"id": "wsLzNrAe",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 0,
			"opacity": 100,
			"angle": 0,
			"x": -108.33333333333326,
			"y": -91.03515624999994,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"width": 18,
			"height": 35,
			"seed": 299075752,
			"groupIds": [],
			"strokeSharpness": "sharp",
			"boundElements": [],
			"updated": 1665930257081,
			"link": null,
			"locked": false,
			"fontSize": 28,
			"fontFamily": 1,
			"text": "L",
			"rawText": "L",
			"baseline": 25,
			"textAlign": "left",
			"verticalAlign": "top",
			"containerId": null,
			"originalText": "L"
		},
		{
			"type": "freedraw",
			"version": 39,
			"versionNonce": 1406722984,
			"isDeleted": false,
			"id": "A-mL_rUkaTnHMOa-TBMJS",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 0,
			"opacity": 100,
			"angle": 0,
			"x": 156.66666666666663,
			"y": -47.70182291666663,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"width": 0.8333333333333712,
			"height": 15,
			"seed": 1228373672,
			"groupIds": [],
			"strokeSharpness": "round",
			"boundElements": [],
			"updated": 1665930257081,
			"link": null,
			"locked": false,
			"points": [
				[
					0,
					0
				],
				[
					0,
					1.6666666666666856
				],
				[
					0,
					2.5
				],
				[
					0,
					3.3333333333333144
				],
				[
					0,
					4.166666666666686
				],
				[
					0,
					5
				],
				[
					0,
					5.833333333333314
				],
				[
					0,
					7.5
				],
				[
					0,
					8.333333333333314
				],
				[
					0,
					9.166666666666686
				],
				[
					0,
					10
				],
				[
					0,
					10.833333333333314
				],
				[
					0,
					11.666666666666686
				],
				[
					0,
					12.5
				],
				[
					0.8333333333333712,
					13.333333333333314
				],
				[
					0.8333333333333712,
					14.166666666666686
				],
				[
					0.8333333333333712,
					15
				],
				[
					0.8333333333333712,
					15
				]
			],
			"lastCommittedPoint": null,
			"simulatePressure": false,
			"pressures": [
				0.0009765625,
				0.06640625,
				0.0751953125,
				0.0849609375,
				0.0859375,
				0.087890625,
				0.08984375,
				0.1025390625,
				0.1064453125,
				0.109375,
				0.1103515625,
				0.1123046875,
				0.111328125,
				0.103515625,
				0.1044921875,
				0.103515625,
				0.0849609375,
				0
			]
		},
		{
			"type": "freedraw",
			"version": 36,
			"versionNonce": 492210904,
			"isDeleted": false,
			"id": "1gdvL8SDvtYfiN4Opp6_K",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 0,
			"opacity": 100,
			"angle": 0,
			"x": 205,
			"y": -49.368489583333314,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"width": 0,
			"height": 16.666666666666686,
			"seed": 225250520,
			"groupIds": [],
			"strokeSharpness": "round",
			"boundElements": [],
			"updated": 1665930257081,
			"link": null,
			"locked": false,
			"points": [
				[
					0,
					0
				],
				[
					0,
					0.8333333333333712
				],
				[
					0,
					1.6666666666666856
				],
				[
					0,
					2.5
				],
				[
					0,
					3.3333333333333712
				],
				[
					0,
					4.166666666666686
				],
				[
					0,
					5.833333333333371
				],
				[
					0,
					8.333333333333371
				],
				[
					0,
					10.833333333333371
				],
				[
					0,
					12.5
				],
				[
					0,
					14.166666666666686
				],
				[
					0,
					15
				],
				[
					0,
					15.833333333333371
				],
				[
					0,
					16.666666666666686
				],
				[
					0,
					16.666666666666686
				]
			],
			"lastCommittedPoint": null,
			"simulatePressure": false,
			"pressures": [
				0.0478515625,
				0.1796875,
				0.2265625,
				0.2421875,
				0.2578125,
				0.2607421875,
				0.2705078125,
				0.2744140625,
				0.275390625,
				0.271484375,
				0.2666015625,
				0.265625,
				0.24609375,
				0.078125,
				0
			]
		},
		{
			"type": "freedraw",
			"version": 42,
			"versionNonce": 1900949160,
			"isDeleted": false,
			"id": "oAlz5RWt3zNdoIt5cLJef",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 0,
			"opacity": 100,
			"angle": 0,
			"x": 164.16666666666663,
			"y": -48.53515624999994,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"width": 10,
			"height": 12.5,
			"seed": 275821784,
			"groupIds": [],
			"strokeSharpness": "round",
			"boundElements": [],
			"updated": 1665930257082,
			"link": null,
			"locked": false,
			"points": [
				[
					0,
					0
				],
				[
					0,
					0.8333333333333144
				],
				[
					-0.8333333333332575,
					0.8333333333333144
				],
				[
					-1.6666666666666288,
					1.6666666666666288
				],
				[
					-2.5,
					2.5
				],
				[
					-3.3333333333332575,
					3.3333333333333144
				],
				[
					-5,
					5.833333333333314
				],
				[
					-6.666666666666629,
					6.666666666666629
				],
				[
					-7.5,
					7.5
				],
				[
					-8.333333333333258,
					8.333333333333314
				],
				[
					-9.166666666666629,
					9.166666666666629
				],
				[
					-10,
					10
				],
				[
					-9.166666666666629,
					10.833333333333314
				],
				[
					-7.5,
					10.833333333333314
				],
				[
					-5,
					11.666666666666629
				],
				[
					-3.3333333333332575,
					12.5
				],
				[
					-2.5,
					12.5
				],
				[
					-1.6666666666666288,
					12.5
				],
				[
					-0.8333333333332575,
					12.5
				],
				[
					0,
					12.5
				],
				[
					0,
					12.5
				]
			],
			"lastCommittedPoint": null,
			"simulatePressure": false,
			"pressures": [
				0.0703125,
				0.291015625,
				0.294921875,
				0.298828125,
				0.3037109375,
				0.3046875,
				0.3037109375,
				0.306640625,
				0.306640625,
				0.306640625,
				0.306640625,
				0.306640625,
				0.306640625,
				0.3076171875,
				0.3125,
				0.31640625,
				0.31640625,
				0.3173828125,
				0.3173828125,
				0.2802734375,
				0
			]
		},
		{
			"type": "freedraw",
			"version": 45,
			"versionNonce": 888106968,
			"isDeleted": false,
			"id": "UHaTkg59I_kKSeRkahEna",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 0,
			"opacity": 100,
			"angle": 0,
			"x": 159.16666666666663,
			"y": -40.20182291666663,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"width": 33.33333333333337,
			"height": 2.5,
			"seed": 1303800792,
			"groupIds": [],
			"strokeSharpness": "round",
			"boundElements": [],
			"updated": 1665930257082,
			"link": null,
			"locked": false,
			"points": [
				[
					0,
					0
				],
				[
					0.8333333333333712,
					0
				],
				[
					1.6666666666667425,
					0
				],
				[
					3.3333333333333712,
					0
				],
				[
					5.833333333333371,
					-0.8333333333333144
				],
				[
					7.5,
					-0.8333333333333144
				],
				[
					9.166666666666742,
					-0.8333333333333144
				],
				[
					11.666666666666742,
					-1.6666666666666856
				],
				[
					13.333333333333371,
					-1.6666666666666856
				],
				[
					15,
					-1.6666666666666856
				],
				[
					16.666666666666742,
					-1.6666666666666856
				],
				[
					19.166666666666742,
					-1.6666666666666856
				],
				[
					20.83333333333337,
					-1.6666666666666856
				],
				[
					23.33333333333337,
					-1.6666666666666856
				],
				[
					25,
					-1.6666666666666856
				],
				[
					25.83333333333337,
					-1.6666666666666856
				],
				[
					26.666666666666742,
					-2.5
				],
				[
					27.5,
					-2.5
				],
				[
					29.166666666666742,
					-2.5
				],
				[
					30,
					-2.5
				],
				[
					30.83333333333337,
					-2.5
				],
				[
					32.5,
					-2.5
				],
				[
					33.33333333333337,
					-2.5
				],
				[
					33.33333333333337,
					-2.5
				]
			],
			"lastCommittedPoint": null,
			"simulatePressure": false,
			"pressures": [
				0.0693359375,
				0.2490234375,
				0.267578125,
				0.2734375,
				0.275390625,
				0.275390625,
				0.27734375,
				0.283203125,
				0.296875,
				0.3046875,
				0.3056640625,
				0.306640625,
				0.30859375,
				0.3095703125,
				0.310546875,
				0.310546875,
				0.302734375,
				0.298828125,
				0.2958984375,
				0.294921875,
				0.291015625,
				0.24609375,
				0.123046875,
				0
			]
		},
		{
			"type": "freedraw",
			"version": 38,
			"versionNonce": 1742870952,
			"isDeleted": false,
			"id": "Vf2PUtzOVV1Y1E0YbG6jY",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 0,
			"opacity": 100,
			"angle": 0,
			"x": 190,
			"y": -50.20182291666663,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"width": 15,
			"height": 11.666666666666686,
			"seed": 2145821096,
			"groupIds": [],
			"strokeSharpness": "round",
			"boundElements": [],
			"updated": 1665930257082,
			"link": null,
			"locked": false,
			"points": [
				[
					0,
					0
				],
				[
					1.6666666666666288,
					0
				],
				[
					3.3333333333333712,
					0.8333333333333144
				],
				[
					5,
					2.5
				],
				[
					6.666666666666629,
					3.3333333333333144
				],
				[
					8.333333333333371,
					3.3333333333333144
				],
				[
					10.833333333333371,
					4.166666666666686
				],
				[
					11.666666666666629,
					5
				],
				[
					13.333333333333371,
					5.833333333333314
				],
				[
					15,
					5.833333333333314
				],
				[
					15,
					6.666666666666686
				],
				[
					14.166666666666629,
					6.666666666666686
				],
				[
					11.666666666666629,
					8.333333333333314
				],
				[
					9.166666666666629,
					9.166666666666686
				],
				[
					7.5,
					10
				],
				[
					4.166666666666629,
					11.666666666666686
				],
				[
					4.166666666666629,
					11.666666666666686
				]
			],
			"lastCommittedPoint": null,
			"simulatePressure": false,
			"pressures": [
				0.125,
				0.2431640625,
				0.27734375,
				0.296875,
				0.30078125,
				0.302734375,
				0.3037109375,
				0.3037109375,
				0.3046875,
				0.3046875,
				0.3076171875,
				0.33203125,
				0.3291015625,
				0.3193359375,
				0.3017578125,
				0.07421875,
				0
			]
		},
		{
			"type": "freedraw",
			"version": 29,
			"versionNonce": 304329944,
			"isDeleted": false,
			"id": "efAv6d1URFVH5kwVIPnLc",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 0,
			"opacity": 100,
			"angle": 0,
			"x": 191.66666666666663,
			"y": -42.70182291666663,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"width": 5.833333333333371,
			"height": 1.6666666666666856,
			"seed": 369034408,
			"groupIds": [],
			"strokeSharpness": "round",
			"boundElements": [],
			"updated": 1665930257082,
			"link": null,
			"locked": false,
			"points": [
				[
					0,
					0
				],
				[
					1.6666666666667425,
					-1.6666666666666856
				],
				[
					3.3333333333333712,
					-1.6666666666666856
				],
				[
					4.1666666666667425,
					-1.6666666666666856
				],
				[
					5,
					-1.6666666666666856
				],
				[
					5.833333333333371,
					-1.6666666666666856
				],
				[
					0,
					0
				]
			],
			"lastCommittedPoint": null,
			"simulatePressure": false,
			"pressures": [
				0,
				0.1396484375,
				0.1591796875,
				0.1796875,
				0.1845703125,
				0.1806640625,
				0
			]
		},
		{
			"type": "freedraw",
			"version": 60,
			"versionNonce": 1522574504,
			"isDeleted": false,
			"id": "yFIiY_jxpmf5gd7XlNzWb",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 0,
			"opacity": 100,
			"angle": 0,
			"x": 170,
			"y": -78.53515624999994,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"width": 12.5,
			"height": 22.499999999999943,
			"seed": 25644504,
			"groupIds": [],
			"strokeSharpness": "round",
			"boundElements": [],
			"updated": 1665930257082,
			"link": null,
			"locked": false,
			"points": [
				[
					0,
					0
				],
				[
					0,
					-0.8333333333333144
				],
				[
					0,
					0
				],
				[
					0,
					2.5
				],
				[
					0,
					5
				],
				[
					0,
					8.333333333333314
				],
				[
					0,
					13.333333333333314
				],
				[
					0.8333333333333712,
					15
				],
				[
					0.8333333333333712,
					16.66666666666663
				],
				[
					0.8333333333333712,
					17.5
				],
				[
					1.6666666666666288,
					17.5
				],
				[
					1.6666666666666288,
					16.66666666666663
				],
				[
					1.6666666666666288,
					15.833333333333314
				],
				[
					0.8333333333333712,
					15
				],
				[
					0,
					14.166666666666629
				],
				[
					-0.8333333333333712,
					14.166666666666629
				],
				[
					-2.5,
					13.333333333333314
				],
				[
					-4.166666666666629,
					13.333333333333314
				],
				[
					-5.833333333333371,
					13.333333333333314
				],
				[
					-7.5,
					13.333333333333314
				],
				[
					-8.333333333333371,
					13.333333333333314
				],
				[
					-9.166666666666629,
					13.333333333333314
				],
				[
					-10,
					13.333333333333314
				],
				[
					-10,
					14.166666666666629
				],
				[
					-10.833333333333371,
					15
				],
				[
					-10.833333333333371,
					16.66666666666663
				],
				[
					-10.833333333333371,
					18.333333333333314
				],
				[
					-10.833333333333371,
					19.16666666666663
				],
				[
					-10,
					20.833333333333314
				],
				[
					-8.333333333333371,
					21.66666666666663
				],
				[
					-6.666666666666629,
					21.66666666666663
				],
				[
					-5,
					21.66666666666663
				],
				[
					-4.166666666666629,
					21.66666666666663
				],
				[
					-3.3333333333333712,
					20.833333333333314
				],
				[
					-2.5,
					20.833333333333314
				],
				[
					-0.8333333333333712,
					20
				],
				[
					0,
					20
				],
				[
					0.8333333333333712,
					20
				],
				[
					0.8333333333333712,
					20
				]
			],
			"lastCommittedPoint": null,
			"simulatePressure": false,
			"pressures": [
				0.078125,
				0.255859375,
				0.291015625,
				0.3115234375,
				0.3212890625,
				0.32421875,
				0.3271484375,
				0.3291015625,
				0.3291015625,
				0.3271484375,
				0.32421875,
				0.2998046875,
				0.2998046875,
				0.298828125,
				0.2998046875,
				0.2998046875,
				0.302734375,
				0.3046875,
				0.3095703125,
				0.314453125,
				0.3232421875,
				0.326171875,
				0.328125,
				0.3232421875,
				0.3203125,
				0.32421875,
				0.32421875,
				0.32421875,
				0.3193359375,
				0.3173828125,
				0.3154296875,
				0.314453125,
				0.314453125,
				0.3134765625,
				0.314453125,
				0.3154296875,
				0.3134765625,
				0.310546875,
				0
			]
		},
		{
			"type": "freedraw",
			"version": 41,
			"versionNonce": 713513432,
			"isDeleted": false,
			"id": "ej0qX-i7fHFiQWUOeO9MV",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 0,
			"opacity": 100,
			"angle": 0,
			"x": 181.66666666666663,
			"y": -76.86848958333331,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"width": 12.5,
			"height": 19.166666666666686,
			"seed": 2096820696,
			"groupIds": [],
			"strokeSharpness": "round",
			"boundElements": [],
			"updated": 1665930257082,
			"link": null,
			"locked": false,
			"points": [
				[
					0,
					0
				],
				[
					0,
					0.8333333333333712
				],
				[
					0,
					4.166666666666686
				],
				[
					0,
					7.5
				],
				[
					0,
					10.833333333333371
				],
				[
					0,
					13.333333333333371
				],
				[
					0,
					15.833333333333371
				],
				[
					0,
					18.33333333333337
				],
				[
					0,
					19.166666666666686
				],
				[
					0.8333333333333712,
					19.166666666666686
				],
				[
					1.6666666666667425,
					19.166666666666686
				],
				[
					3.3333333333333712,
					18.33333333333337
				],
				[
					5,
					17.5
				],
				[
					6.6666666666667425,
					17.5
				],
				[
					8.333333333333371,
					16.666666666666686
				],
				[
					10,
					16.666666666666686
				],
				[
					10.833333333333371,
					16.666666666666686
				],
				[
					11.666666666666742,
					16.666666666666686
				],
				[
					12.5,
					16.666666666666686
				],
				[
					12.5,
					16.666666666666686
				]
			],
			"lastCommittedPoint": null,
			"simulatePressure": false,
			"pressures": [
				0.083984375,
				0.291015625,
				0.3095703125,
				0.32421875,
				0.3291015625,
				0.333984375,
				0.333984375,
				0.3349609375,
				0.345703125,
				0.3466796875,
				0.3466796875,
				0.3486328125,
				0.3505859375,
				0.3525390625,
				0.353515625,
				0.35546875,
				0.3564453125,
				0.3564453125,
				0.3232421875,
				0
			]
		},
		{
			"type": "freedraw",
			"version": 61,
			"versionNonce": 664129448,
			"isDeleted": false,
			"id": "Q9DWwiRSASEk4Q5u0xcfq",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 0,
			"opacity": 100,
			"angle": 0,
			"x": -101.66666666666663,
			"y": -83.53515624999994,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"width": 15.833333333333371,
			"height": 20.833333333333314,
			"seed": 665175464,
			"groupIds": [],
			"strokeSharpness": "round",
			"boundElements": [],
			"updated": 1665930257082,
			"link": null,
			"locked": false,
			"points": [
				[
					0,
					0
				],
				[
					0,
					-0.8333333333333144
				],
				[
					-0.8333333333333712,
					-0.8333333333333144
				],
				[
					-0.8333333333333712,
					0
				],
				[
					-0.8333333333333712,
					0.8333333333333144
				],
				[
					-0.8333333333333712,
					2.5
				],
				[
					-1.6666666666666288,
					3.3333333333333144
				],
				[
					-1.6666666666666288,
					5.833333333333314
				],
				[
					-1.6666666666666288,
					6.666666666666629
				],
				[
					-2.5,
					7.5
				],
				[
					-2.5,
					9.166666666666629
				],
				[
					-3.3333333333333712,
					10.833333333333314
				],
				[
					-4.166666666666629,
					12.5
				],
				[
					-4.166666666666629,
					13.333333333333314
				],
				[
					-4.166666666666629,
					14.166666666666629
				],
				[
					-4.166666666666629,
					15
				],
				[
					-5,
					15.833333333333314
				],
				[
					-5,
					16.66666666666663
				],
				[
					-5,
					17.5
				],
				[
					-5,
					18.333333333333314
				],
				[
					-5,
					19.16666666666663
				],
				[
					-5,
					20
				],
				[
					-4.166666666666629,
					20
				],
				[
					-3.3333333333333712,
					19.16666666666663
				],
				[
					-1.6666666666666288,
					19.16666666666663
				],
				[
					0.8333333333333712,
					18.333333333333314
				],
				[
					1.6666666666666288,
					18.333333333333314
				],
				[
					2.5,
					18.333333333333314
				],
				[
					3.3333333333333712,
					18.333333333333314
				],
				[
					4.166666666666629,
					18.333333333333314
				],
				[
					5,
					18.333333333333314
				],
				[
					5.833333333333371,
					18.333333333333314
				],
				[
					6.666666666666629,
					18.333333333333314
				],
				[
					7.5,
					18.333333333333314
				],
				[
					8.333333333333371,
					18.333333333333314
				],
				[
					9.166666666666629,
					18.333333333333314
				],
				[
					10,
					18.333333333333314
				],
				[
					10.833333333333371,
					18.333333333333314
				],
				[
					10.833333333333371,
					19.16666666666663
				],
				[
					10.833333333333371,
					19.16666666666663
				]
			],
			"lastCommittedPoint": null,
			"simulatePressure": false,
			"pressures": [
				0.0185546875,
				0.1904296875,
				0.216796875,
				0.296875,
				0.302734375,
				0.3076171875,
				0.3095703125,
				0.3134765625,
				0.3134765625,
				0.314453125,
				0.31640625,
				0.3173828125,
				0.3173828125,
				0.3173828125,
				0.31640625,
				0.31640625,
				0.314453125,
				0.3134765625,
				0.314453125,
				0.3134765625,
				0.3125,
				0.3037109375,
				0.3134765625,
				0.3173828125,
				0.3173828125,
				0.32421875,
				0.328125,
				0.337890625,
				0.3427734375,
				0.3447265625,
				0.345703125,
				0.345703125,
				0.345703125,
				0.3466796875,
				0.3466796875,
				0.34765625,
				0.3505859375,
				0.3505859375,
				0.0283203125,
				0
			]
		},
		{
			"type": "freedraw",
			"version": 114,
			"versionNonce": 556687064,
			"isDeleted": false,
			"id": "os-tbOfR8uU3I52xz99XC",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 0,
			"opacity": 100,
			"angle": 0,
			"x": -376.66666666666663,
			"y": -251.86848958333326,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"width": 22.5,
			"height": 126.66666666666663,
			"seed": 256437464,
			"groupIds": [],
			"strokeSharpness": "round",
			"boundElements": [],
			"updated": 1665930257082,
			"link": null,
			"locked": false,
			"points": [
				[
					0,
					0
				],
				[
					-0.8333333333333144,
					0
				],
				[
					-1.6666666666666288,
					0
				],
				[
					-2.5,
					0
				],
				[
					-3.3333333333333144,
					0
				],
				[
					-5,
					0.8333333333333144
				],
				[
					-6.666666666666629,
					1.6666666666666572
				],
				[
					-7.5,
					1.6666666666666572
				],
				[
					-9.166666666666629,
					2.5
				],
				[
					-10.833333333333314,
					4.166666666666657
				],
				[
					-12.5,
					5.833333333333314
				],
				[
					-14.166666666666629,
					7.5
				],
				[
					-15,
					9.166666666666657
				],
				[
					-15.833333333333314,
					10
				],
				[
					-16.66666666666663,
					12.5
				],
				[
					-16.66666666666663,
					13.333333333333314
				],
				[
					-17.5,
					14.999999999999972
				],
				[
					-17.5,
					16.666666666666657
				],
				[
					-18.333333333333314,
					19.99999999999997
				],
				[
					-18.333333333333314,
					21.666666666666657
				],
				[
					-18.333333333333314,
					24.166666666666657
				],
				[
					-18.333333333333314,
					26.666666666666657
				],
				[
					-18.333333333333314,
					29.166666666666657
				],
				[
					-18.333333333333314,
					30.833333333333314
				],
				[
					-17.5,
					32.49999999999997
				],
				[
					-16.66666666666663,
					34.16666666666666
				],
				[
					-16.66666666666663,
					35.833333333333314
				],
				[
					-15,
					38.333333333333314
				],
				[
					-14.166666666666629,
					39.99999999999997
				],
				[
					-13.333333333333314,
					41.66666666666666
				],
				[
					-12.5,
					42.49999999999997
				],
				[
					-10.833333333333314,
					44.16666666666666
				],
				[
					-9.166666666666629,
					45.833333333333314
				],
				[
					-8.333333333333314,
					46.66666666666666
				],
				[
					-8.333333333333314,
					48.333333333333314
				],
				[
					-7.5,
					49.16666666666666
				],
				[
					-7.5,
					50.833333333333314
				],
				[
					-7.5,
					54.16666666666666
				],
				[
					-7.5,
					55.833333333333314
				],
				[
					-7.5,
					57.49999999999997
				],
				[
					-7.5,
					58.333333333333314
				],
				[
					-7.5,
					59.16666666666666
				],
				[
					-8.333333333333314,
					59.99999999999997
				],
				[
					-9.166666666666629,
					59.99999999999997
				],
				[
					-10.833333333333314,
					60.833333333333314
				],
				[
					-13.333333333333314,
					62.49999999999997
				],
				[
					-16.66666666666663,
					63.333333333333314
				],
				[
					-18.333333333333314,
					64.99999999999997
				],
				[
					-20.833333333333314,
					65.83333333333331
				],
				[
					-21.66666666666663,
					66.66666666666666
				],
				[
					-22.5,
					66.66666666666666
				],
				[
					-21.66666666666663,
					66.66666666666666
				],
				[
					-20,
					66.66666666666666
				],
				[
					-18.333333333333314,
					67.49999999999997
				],
				[
					-16.66666666666663,
					68.33333333333331
				],
				[
					-15.833333333333314,
					69.16666666666666
				],
				[
					-14.166666666666629,
					70.83333333333331
				],
				[
					-12.5,
					72.49999999999997
				],
				[
					-11.666666666666629,
					74.99999999999997
				],
				[
					-10.833333333333314,
					77.49999999999997
				],
				[
					-10,
					79.16666666666666
				],
				[
					-9.166666666666629,
					82.49999999999997
				],
				[
					-9.166666666666629,
					83.33333333333331
				],
				[
					-9.166666666666629,
					84.16666666666666
				],
				[
					-9.166666666666629,
					85.83333333333331
				],
				[
					-9.166666666666629,
					87.49999999999997
				],
				[
					-9.166666666666629,
					89.16666666666666
				],
				[
					-10,
					90.83333333333331
				],
				[
					-10.833333333333314,
					92.49999999999997
				],
				[
					-12.5,
					94.99999999999997
				],
				[
					-13.333333333333314,
					96.66666666666666
				],
				[
					-14.166666666666629,
					99.16666666666666
				],
				[
					-15,
					100.83333333333331
				],
				[
					-15.833333333333314,
					103.33333333333331
				],
				[
					-16.66666666666663,
					105.83333333333331
				],
				[
					-18.333333333333314,
					109.99999999999997
				],
				[
					-19.16666666666663,
					112.5
				],
				[
					-20,
					115
				],
				[
					-20,
					117.5
				],
				[
					-20,
					119.16666666666663
				],
				[
					-20,
					121.66666666666663
				],
				[
					-20,
					123.33333333333331
				],
				[
					-19.16666666666663,
					124.16666666666663
				],
				[
					-17.5,
					125.83333333333331
				],
				[
					-15.833333333333314,
					125.83333333333331
				],
				[
					-14.166666666666629,
					125.83333333333331
				],
				[
					-12.5,
					125.83333333333331
				],
				[
					-10.833333333333314,
					125.83333333333331
				],
				[
					-8.333333333333314,
					125.83333333333331
				],
				[
					-7.5,
					125.83333333333331
				],
				[
					-5.833333333333314,
					125.83333333333331
				],
				[
					-5,
					126.66666666666663
				],
				[
					-5,
					126.66666666666663
				]
			],
			"lastCommittedPoint": null,
			"simulatePressure": false,
			"pressures": [
				0.0634765625,
				0.3515625,
				0.3564453125,
				0.357421875,
				0.359375,
				0.3603515625,
				0.3623046875,
				0.3623046875,
				0.3623046875,
				0.365234375,
				0.3662109375,
				0.3662109375,
				0.3642578125,
				0.3603515625,
				0.353515625,
				0.3466796875,
				0.33984375,
				0.3388671875,
				0.3359375,
				0.3349609375,
				0.3349609375,
				0.3349609375,
				0.333984375,
				0.3349609375,
				0.3349609375,
				0.3349609375,
				0.3349609375,
				0.3359375,
				0.3359375,
				0.3388671875,
				0.3388671875,
				0.33984375,
				0.33984375,
				0.33984375,
				0.341796875,
				0.3427734375,
				0.3427734375,
				0.3447265625,
				0.3447265625,
				0.3447265625,
				0.345703125,
				0.345703125,
				0.345703125,
				0.345703125,
				0.3447265625,
				0.3427734375,
				0.3427734375,
				0.341796875,
				0.341796875,
				0.3408203125,
				0.3408203125,
				0.3291015625,
				0.3291015625,
				0.3291015625,
				0.3291015625,
				0.328125,
				0.3291015625,
				0.330078125,
				0.33203125,
				0.33203125,
				0.3330078125,
				0.333984375,
				0.3349609375,
				0.3349609375,
				0.3359375,
				0.3388671875,
				0.3388671875,
				0.33984375,
				0.34375,
				0.3447265625,
				0.3466796875,
				0.3515625,
				0.3525390625,
				0.361328125,
				0.3642578125,
				0.369140625,
				0.37109375,
				0.3720703125,
				0.3720703125,
				0.373046875,
				0.3720703125,
				0.3720703125,
				0.3720703125,
				0.36328125,
				0.361328125,
				0.3603515625,
				0.361328125,
				0.3642578125,
				0.3662109375,
				0.3671875,
				0.3662109375,
				0.201171875,
				0
			]
		},
		{
			"type": "freedraw",
			"version": 105,
			"versionNonce": 378378920,
			"isDeleted": false,
			"id": "qhGRkQnAVfB5LNgz8UYcM",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 0,
			"opacity": 100,
			"angle": 0,
			"x": -378.33333333333326,
			"y": -33.53515624999994,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"width": 19.166666666666686,
			"height": 140.83333333333331,
			"seed": 836212696,
			"groupIds": [],
			"strokeSharpness": "round",
			"boundElements": [],
			"updated": 1665930257082,
			"link": null,
			"locked": false,
			"points": [
				[
					0,
					0
				],
				[
					-0.8333333333333712,
					0
				],
				[
					-3.3333333333333712,
					0.8333333333333144
				],
				[
					-4.166666666666686,
					1.6666666666666288
				],
				[
					-5.833333333333371,
					1.6666666666666288
				],
				[
					-7.5,
					3.3333333333333144
				],
				[
					-9.166666666666686,
					5
				],
				[
					-10.833333333333371,
					5.833333333333314
				],
				[
					-12.5,
					8.333333333333314
				],
				[
					-14.166666666666686,
					11.666666666666629
				],
				[
					-15,
					13.333333333333314
				],
				[
					-15.833333333333371,
					15.833333333333314
				],
				[
					-16.666666666666686,
					19.16666666666663
				],
				[
					-17.5,
					21.66666666666663
				],
				[
					-18.33333333333337,
					24.16666666666663
				],
				[
					-18.33333333333337,
					26.66666666666663
				],
				[
					-18.33333333333337,
					28.333333333333314
				],
				[
					-18.33333333333337,
					30
				],
				[
					-18.33333333333337,
					33.333333333333314
				],
				[
					-18.33333333333337,
					35
				],
				[
					-17.5,
					36.66666666666663
				],
				[
					-17.5,
					38.333333333333314
				],
				[
					-16.666666666666686,
					40
				],
				[
					-15.833333333333371,
					41.66666666666663
				],
				[
					-15,
					43.333333333333314
				],
				[
					-14.166666666666686,
					44.16666666666663
				],
				[
					-13.333333333333371,
					45
				],
				[
					-12.5,
					47.5
				],
				[
					-10.833333333333371,
					49.16666666666663
				],
				[
					-10,
					50.833333333333314
				],
				[
					-9.166666666666686,
					52.5
				],
				[
					-8.333333333333371,
					54.16666666666663
				],
				[
					-7.5,
					55.833333333333314
				],
				[
					-7.5,
					57.5
				],
				[
					-7.5,
					58.333333333333314
				],
				[
					-7.5,
					60.833333333333314
				],
				[
					-7.5,
					63.333333333333314
				],
				[
					-8.333333333333371,
					65
				],
				[
					-9.166666666666686,
					65.83333333333331
				],
				[
					-10.833333333333371,
					67.5
				],
				[
					-12.5,
					69.16666666666663
				],
				[
					-15.833333333333371,
					70.83333333333331
				],
				[
					-16.666666666666686,
					72.5
				],
				[
					-17.5,
					72.5
				],
				[
					-18.33333333333337,
					73.33333333333331
				],
				[
					-17.5,
					73.33333333333331
				],
				[
					-16.666666666666686,
					73.33333333333331
				],
				[
					-14.166666666666686,
					73.33333333333331
				],
				[
					-10.833333333333371,
					73.33333333333331
				],
				[
					-9.166666666666686,
					74.16666666666663
				],
				[
					-7.5,
					75.83333333333331
				],
				[
					-5.833333333333371,
					79.16666666666663
				],
				[
					-5,
					81.66666666666663
				],
				[
					-5,
					84.16666666666663
				],
				[
					-4.166666666666686,
					86.66666666666663
				],
				[
					-4.166666666666686,
					89.16666666666663
				],
				[
					-4.166666666666686,
					92.5
				],
				[
					-5,
					94.16666666666663
				],
				[
					-6.666666666666686,
					96.66666666666663
				],
				[
					-7.5,
					99.16666666666663
				],
				[
					-8.333333333333371,
					101.66666666666663
				],
				[
					-9.166666666666686,
					103.33333333333331
				],
				[
					-10,
					105
				],
				[
					-10.833333333333371,
					108.33333333333331
				],
				[
					-11.666666666666686,
					110.83333333333331
				],
				[
					-12.5,
					115
				],
				[
					-13.333333333333371,
					117.5
				],
				[
					-13.333333333333371,
					120.83333333333331
				],
				[
					-14.166666666666686,
					124.16666666666663
				],
				[
					-14.166666666666686,
					128.33333333333331
				],
				[
					-14.166666666666686,
					130
				],
				[
					-14.166666666666686,
					132.5
				],
				[
					-14.166666666666686,
					133.33333333333331
				],
				[
					-13.333333333333371,
					135
				],
				[
					-12.5,
					137.5
				],
				[
					-9.166666666666686,
					139.16666666666663
				],
				[
					-7.5,
					140
				],
				[
					-5.833333333333371,
					140.83333333333331
				],
				[
					-4.166666666666686,
					140.83333333333331
				],
				[
					-3.3333333333333712,
					140.83333333333331
				],
				[
					-2.5,
					140.83333333333331
				],
				[
					-0.8333333333333712,
					139.16666666666663
				],
				[
					0.8333333333333144,
					138.33333333333331
				],
				[
					0.8333333333333144,
					138.33333333333331
				]
			],
			"lastCommittedPoint": null,
			"simulatePressure": false,
			"pressures": [
				0.048828125,
				0.3037109375,
				0.314453125,
				0.3203125,
				0.3271484375,
				0.3359375,
				0.3359375,
				0.337890625,
				0.337890625,
				0.33984375,
				0.3408203125,
				0.341796875,
				0.3427734375,
				0.341796875,
				0.3427734375,
				0.341796875,
				0.341796875,
				0.3427734375,
				0.3427734375,
				0.3427734375,
				0.3427734375,
				0.3427734375,
				0.3427734375,
				0.3427734375,
				0.3427734375,
				0.341796875,
				0.341796875,
				0.341796875,
				0.341796875,
				0.3427734375,
				0.3427734375,
				0.3427734375,
				0.3427734375,
				0.3447265625,
				0.3447265625,
				0.345703125,
				0.3515625,
				0.361328125,
				0.361328125,
				0.36328125,
				0.3671875,
				0.37890625,
				0.37890625,
				0.3798828125,
				0.3798828125,
				0.3642578125,
				0.3642578125,
				0.3603515625,
				0.3583984375,
				0.3583984375,
				0.3603515625,
				0.3642578125,
				0.365234375,
				0.3662109375,
				0.3671875,
				0.3671875,
				0.3681640625,
				0.369140625,
				0.369140625,
				0.37109375,
				0.37109375,
				0.37109375,
				0.37109375,
				0.37109375,
				0.3720703125,
				0.3720703125,
				0.373046875,
				0.3740234375,
				0.375,
				0.37890625,
				0.37890625,
				0.37890625,
				0.37890625,
				0.3779296875,
				0.3779296875,
				0.3798828125,
				0.380859375,
				0.380859375,
				0.3828125,
				0.3828125,
				0.375,
				0.27734375,
				0.19140625,
				0
			]
		},
		{
			"type": "freedraw",
			"version": 113,
			"versionNonce": 380421080,
			"isDeleted": false,
			"id": "BVNGIVseJ-iVphoqnmr9a",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 0,
			"opacity": 100,
			"angle": 0,
			"x": 210,
			"y": -31.868489583333314,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"width": 26.66666666666663,
			"height": 134.16666666666669,
			"seed": 1058889640,
			"groupIds": [],
			"strokeSharpness": "round",
			"boundElements": [],
			"updated": 1665930257082,
			"link": null,
			"locked": false,
			"points": [
				[
					0,
					0
				],
				[
					0.8333333333333712,
					0
				],
				[
					2.5,
					0
				],
				[
					3.3333333333333712,
					0
				],
				[
					5.833333333333371,
					0
				],
				[
					6.666666666666629,
					0
				],
				[
					8.333333333333371,
					0
				],
				[
					9.166666666666629,
					0
				],
				[
					10,
					1.6666666666666856
				],
				[
					11.666666666666629,
					3.3333333333333712
				],
				[
					12.5,
					5
				],
				[
					13.333333333333371,
					6.666666666666686
				],
				[
					14.166666666666629,
					8.333333333333371
				],
				[
					15,
					10
				],
				[
					15,
					10.833333333333371
				],
				[
					15.833333333333371,
					12.5
				],
				[
					15.833333333333371,
					13.333333333333371
				],
				[
					15.833333333333371,
					15
				],
				[
					15.833333333333371,
					17.5
				],
				[
					15,
					19.166666666666686
				],
				[
					15,
					20.83333333333337
				],
				[
					14.166666666666629,
					24.166666666666686
				],
				[
					13.333333333333371,
					26.666666666666686
				],
				[
					12.5,
					29.166666666666686
				],
				[
					11.666666666666629,
					30.83333333333337
				],
				[
					10.833333333333371,
					33.33333333333337
				],
				[
					9.166666666666629,
					35.83333333333337
				],
				[
					8.333333333333371,
					38.33333333333337
				],
				[
					8.333333333333371,
					40
				],
				[
					7.5,
					40.83333333333337
				],
				[
					7.5,
					43.33333333333337
				],
				[
					7.5,
					45
				],
				[
					7.5,
					47.5
				],
				[
					7.5,
					49.166666666666686
				],
				[
					7.5,
					50.83333333333337
				],
				[
					7.5,
					52.5
				],
				[
					9.166666666666629,
					53.33333333333337
				],
				[
					10,
					54.166666666666686
				],
				[
					11.666666666666629,
					55
				],
				[
					15,
					55
				],
				[
					15.833333333333371,
					55
				],
				[
					17.5,
					55.83333333333337
				],
				[
					19.16666666666663,
					55.83333333333337
				],
				[
					20,
					55.83333333333337
				],
				[
					20.83333333333337,
					55.83333333333337
				],
				[
					21.66666666666663,
					55.83333333333337
				],
				[
					20.83333333333337,
					56.666666666666686
				],
				[
					20,
					56.666666666666686
				],
				[
					18.33333333333337,
					57.5
				],
				[
					15.833333333333371,
					60
				],
				[
					12.5,
					63.33333333333337
				],
				[
					11.666666666666629,
					64.16666666666669
				],
				[
					10,
					66.66666666666669
				],
				[
					9.166666666666629,
					68.33333333333337
				],
				[
					7.5,
					71.66666666666669
				],
				[
					6.666666666666629,
					73.33333333333337
				],
				[
					6.666666666666629,
					75
				],
				[
					6.666666666666629,
					77.5
				],
				[
					6.666666666666629,
					80
				],
				[
					6.666666666666629,
					82.5
				],
				[
					7.5,
					86.66666666666669
				],
				[
					7.5,
					89.16666666666669
				],
				[
					8.333333333333371,
					91.66666666666669
				],
				[
					9.166666666666629,
					95
				],
				[
					10,
					96.66666666666669
				],
				[
					10.833333333333371,
					99.16666666666669
				],
				[
					11.666666666666629,
					100.83333333333337
				],
				[
					12.5,
					102.5
				],
				[
					12.5,
					104.16666666666669
				],
				[
					13.333333333333371,
					107.5
				],
				[
					13.333333333333371,
					109.16666666666669
				],
				[
					13.333333333333371,
					111.66666666666669
				],
				[
					13.333333333333371,
					114.16666666666669
				],
				[
					13.333333333333371,
					115.83333333333337
				],
				[
					13.333333333333371,
					117.5
				],
				[
					12.5,
					119.16666666666669
				],
				[
					12.5,
					120.83333333333337
				],
				[
					11.666666666666629,
					122.5
				],
				[
					10.833333333333371,
					125
				],
				[
					10,
					126.66666666666669
				],
				[
					8.333333333333371,
					129.16666666666669
				],
				[
					7.5,
					130.83333333333337
				],
				[
					6.666666666666629,
					131.66666666666669
				],
				[
					5.833333333333371,
					132.5
				],
				[
					5,
					132.5
				],
				[
					3.3333333333333712,
					133.33333333333337
				],
				[
					2.5,
					134.16666666666669
				],
				[
					0,
					134.16666666666669
				],
				[
					-1.6666666666666288,
					134.16666666666669
				],
				[
					-3.3333333333333712,
					133.33333333333337
				],
				[
					-5,
					132.5
				],
				[
					-5,
					132.5
				]
			],
			"lastCommittedPoint": null,
			"simulatePressure": false,
			"pressures": [
				0.08203125,
				0.2724609375,
				0.287109375,
				0.291015625,
				0.2939453125,
				0.2958984375,
				0.2998046875,
				0.302734375,
				0.3037109375,
				0.306640625,
				0.306640625,
				0.3076171875,
				0.3076171875,
				0.3076171875,
				0.30859375,
				0.3095703125,
				0.310546875,
				0.3115234375,
				0.3125,
				0.3134765625,
				0.3134765625,
				0.3125,
				0.3115234375,
				0.3095703125,
				0.3095703125,
				0.30859375,
				0.3076171875,
				0.30859375,
				0.30859375,
				0.30859375,
				0.30859375,
				0.30859375,
				0.310546875,
				0.3115234375,
				0.3125,
				0.314453125,
				0.3154296875,
				0.3154296875,
				0.3154296875,
				0.326171875,
				0.3291015625,
				0.3349609375,
				0.34375,
				0.3525390625,
				0.3603515625,
				0.3642578125,
				0.4169921875,
				0.419921875,
				0.423828125,
				0.42578125,
				0.4267578125,
				0.42578125,
				0.42578125,
				0.42578125,
				0.4248046875,
				0.4248046875,
				0.423828125,
				0.423828125,
				0.4228515625,
				0.4228515625,
				0.423828125,
				0.423828125,
				0.4248046875,
				0.423828125,
				0.423828125,
				0.421875,
				0.421875,
				0.4228515625,
				0.4228515625,
				0.4228515625,
				0.423828125,
				0.423828125,
				0.423828125,
				0.423828125,
				0.423828125,
				0.423828125,
				0.423828125,
				0.423828125,
				0.423828125,
				0.4248046875,
				0.423828125,
				0.423828125,
				0.423828125,
				0.421875,
				0.421875,
				0.4208984375,
				0.4208984375,
				0.416015625,
				0.38671875,
				0.2978515625,
				0.193359375,
				0
			]
		},
		{
			"type": "freedraw",
			"version": 103,
			"versionNonce": 173278632,
			"isDeleted": false,
			"id": "8X2vgaTHIOQnP-Gk-41l6",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 0,
			"opacity": 100,
			"angle": 0,
			"x": 157.5,
			"y": -248.53515624999994,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"width": 23.33333333333337,
			"height": 136.66666666666663,
			"seed": 1835472088,
			"groupIds": [],
			"strokeSharpness": "round",
			"boundElements": [],
			"updated": 1665930257082,
			"link": null,
			"locked": false,
			"points": [
				[
					0,
					0
				],
				[
					-0.8333333333333712,
					-0.8333333333333144
				],
				[
					0,
					-0.8333333333333144
				],
				[
					1.6666666666666288,
					-0.8333333333333144
				],
				[
					3.3333333333333712,
					-0.8333333333333144
				],
				[
					5,
					-0.8333333333333144
				],
				[
					6.666666666666629,
					0
				],
				[
					8.333333333333371,
					2.5
				],
				[
					11.666666666666629,
					4.166666666666686
				],
				[
					13.333333333333371,
					6.666666666666686
				],
				[
					15.833333333333371,
					8.333333333333343
				],
				[
					18.33333333333337,
					10.833333333333343
				],
				[
					19.16666666666663,
					12.5
				],
				[
					20,
					14.166666666666657
				],
				[
					20.83333333333337,
					16.666666666666657
				],
				[
					20.83333333333337,
					18.333333333333343
				],
				[
					21.66666666666663,
					20.833333333333343
				],
				[
					22.5,
					24.166666666666657
				],
				[
					22.5,
					27.5
				],
				[
					22.5,
					30
				],
				[
					22.5,
					34.16666666666666
				],
				[
					21.66666666666663,
					36.66666666666666
				],
				[
					20.83333333333337,
					39.16666666666666
				],
				[
					19.16666666666663,
					41.66666666666666
				],
				[
					17.5,
					45
				],
				[
					15.833333333333371,
					48.33333333333334
				],
				[
					13.333333333333371,
					52.5
				],
				[
					11.666666666666629,
					54.16666666666666
				],
				[
					10.833333333333371,
					55.83333333333334
				],
				[
					10,
					57.5
				],
				[
					10,
					59.16666666666666
				],
				[
					9.166666666666629,
					60.83333333333334
				],
				[
					9.166666666666629,
					62.5
				],
				[
					9.166666666666629,
					64.16666666666666
				],
				[
					9.166666666666629,
					66.66666666666666
				],
				[
					9.166666666666629,
					68.33333333333334
				],
				[
					10.833333333333371,
					69.16666666666666
				],
				[
					14.166666666666629,
					69.16666666666666
				],
				[
					16.66666666666663,
					70
				],
				[
					18.33333333333337,
					70
				],
				[
					19.16666666666663,
					70
				],
				[
					19.16666666666663,
					70.83333333333334
				],
				[
					17.5,
					71.66666666666666
				],
				[
					14.166666666666629,
					74.16666666666666
				],
				[
					12.5,
					76.66666666666666
				],
				[
					10.833333333333371,
					79.16666666666666
				],
				[
					9.166666666666629,
					81.66666666666666
				],
				[
					8.333333333333371,
					83.33333333333334
				],
				[
					7.5,
					85
				],
				[
					7.5,
					86.66666666666666
				],
				[
					7.5,
					88.33333333333334
				],
				[
					8.333333333333371,
					90
				],
				[
					10,
					94.16666666666666
				],
				[
					10.833333333333371,
					97.5
				],
				[
					12.5,
					100
				],
				[
					13.333333333333371,
					101.66666666666666
				],
				[
					14.166666666666629,
					103.33333333333334
				],
				[
					15,
					104.16666666666666
				],
				[
					15.833333333333371,
					106.66666666666666
				],
				[
					16.66666666666663,
					109.16666666666669
				],
				[
					17.5,
					110.83333333333331
				],
				[
					17.5,
					112.5
				],
				[
					17.5,
					115.83333333333331
				],
				[
					17.5,
					118.33333333333331
				],
				[
					17.5,
					120.83333333333331
				],
				[
					17.5,
					123.33333333333331
				],
				[
					17.5,
					124.16666666666669
				],
				[
					17.5,
					126.66666666666669
				],
				[
					17.5,
					128.33333333333331
				],
				[
					16.66666666666663,
					129.16666666666669
				],
				[
					15.833333333333371,
					131.66666666666669
				],
				[
					15,
					132.5
				],
				[
					14.166666666666629,
					134.16666666666669
				],
				[
					13.333333333333371,
					135
				],
				[
					12.5,
					135
				],
				[
					11.666666666666629,
					135.83333333333331
				],
				[
					9.166666666666629,
					135.83333333333331
				],
				[
					7.5,
					135.83333333333331
				],
				[
					5.833333333333371,
					135.83333333333331
				],
				[
					4.166666666666629,
					135.83333333333331
				],
				[
					2.5,
					135.83333333333331
				],
				[
					2.5,
					135.83333333333331
				]
			],
			"lastCommittedPoint": null,
			"simulatePressure": false,
			"pressures": [
				0.0458984375,
				0.2158203125,
				0.2802734375,
				0.2890625,
				0.291015625,
				0.2958984375,
				0.3037109375,
				0.3134765625,
				0.3212890625,
				0.3232421875,
				0.3271484375,
				0.333984375,
				0.3359375,
				0.3408203125,
				0.3466796875,
				0.3486328125,
				0.3486328125,
				0.3544921875,
				0.357421875,
				0.361328125,
				0.365234375,
				0.3681640625,
				0.3681640625,
				0.37109375,
				0.373046875,
				0.375,
				0.3740234375,
				0.375,
				0.375,
				0.375,
				0.373046875,
				0.373046875,
				0.3720703125,
				0.37109375,
				0.37109375,
				0.3662109375,
				0.361328125,
				0.361328125,
				0.361328125,
				0.3603515625,
				0.3642578125,
				0.4072265625,
				0.4140625,
				0.412109375,
				0.41015625,
				0.4091796875,
				0.4091796875,
				0.408203125,
				0.4072265625,
				0.4052734375,
				0.4052734375,
				0.404296875,
				0.4052734375,
				0.4052734375,
				0.408203125,
				0.408203125,
				0.4091796875,
				0.408203125,
				0.412109375,
				0.4130859375,
				0.4150390625,
				0.4169921875,
				0.419921875,
				0.4208984375,
				0.4208984375,
				0.4208984375,
				0.4208984375,
				0.421875,
				0.423828125,
				0.423828125,
				0.423828125,
				0.423828125,
				0.423828125,
				0.423828125,
				0.423828125,
				0.423828125,
				0.4248046875,
				0.4248046875,
				0.4228515625,
				0.38671875,
				0.0966796875,
				0
			]
		},
		{
			"type": "freedraw",
			"version": 44,
			"versionNonce": 2031519960,
			"isDeleted": false,
			"id": "PbDwu9eExomtd67RAZU5o",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 0,
			"opacity": 100,
			"angle": 0,
			"x": 175.83333333333337,
			"y": -179.3684895833333,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"width": 64.16666666666663,
			"height": 5,
			"seed": 1303095208,
			"groupIds": [],
			"strokeSharpness": "round",
			"boundElements": [],
			"updated": 1665930257082,
			"link": null,
			"locked": false,
			"points": [
				[
					0,
					0
				],
				[
					0.8333333333332575,
					0
				],
				[
					3.3333333333332575,
					0
				],
				[
					5.8333333333332575,
					0
				],
				[
					9.166666666666629,
					0
				],
				[
					12.5,
					0
				],
				[
					18.333333333333258,
					0.8333333333333428
				],
				[
					22.5,
					1.6666666666666856
				],
				[
					26.66666666666663,
					1.6666666666666856
				],
				[
					30,
					2.5
				],
				[
					34.16666666666663,
					2.5
				],
				[
					39.16666666666663,
					3.333333333333343
				],
				[
					43.33333333333326,
					3.333333333333343
				],
				[
					46.66666666666663,
					3.333333333333343
				],
				[
					49.16666666666663,
					3.333333333333343
				],
				[
					53.33333333333326,
					4.166666666666686
				],
				[
					55.83333333333326,
					4.166666666666686
				],
				[
					57.5,
					4.166666666666686
				],
				[
					60,
					4.166666666666686
				],
				[
					61.66666666666663,
					5
				],
				[
					63.33333333333326,
					5
				],
				[
					64.16666666666663,
					5
				],
				[
					64.16666666666663,
					5
				]
			],
			"lastCommittedPoint": null,
			"simulatePressure": false,
			"pressures": [
				0.0107421875,
				0.23046875,
				0.29296875,
				0.322265625,
				0.34375,
				0.369140625,
				0.3955078125,
				0.4111328125,
				0.4306640625,
				0.4375,
				0.443359375,
				0.4482421875,
				0.451171875,
				0.466796875,
				0.47265625,
				0.474609375,
				0.4765625,
				0.4765625,
				0.4765625,
				0.4755859375,
				0.42578125,
				0.3076171875,
				0
			]
		},
		{
			"type": "freedraw",
			"version": 38,
			"versionNonce": 806686888,
			"isDeleted": false,
			"id": "SBk-lQ6SmZW0IiiPRS-Ix",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 0,
			"opacity": 100,
			"angle": 0,
			"x": 233.33333333333337,
			"y": -183.53515624999994,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"width": 16.666666666666742,
			"height": 18.333333333333343,
			"seed": 1054501800,
			"groupIds": [],
			"strokeSharpness": "round",
			"boundElements": [],
			"updated": 1665930257082,
			"link": null,
			"locked": false,
			"points": [
				[
					0,
					0
				],
				[
					-1.6666666666667425,
					-0.8333333333333428
				],
				[
					-0.8333333333333712,
					0
				],
				[
					1.6666666666666288,
					0.8333333333333428
				],
				[
					5,
					2.5
				],
				[
					9.166666666666629,
					5
				],
				[
					11.666666666666629,
					5.833333333333343
				],
				[
					13.333333333333258,
					5.833333333333343
				],
				[
					14.166666666666629,
					6.666666666666657
				],
				[
					15,
					6.666666666666657
				],
				[
					15,
					7.5
				],
				[
					13.333333333333258,
					7.5
				],
				[
					10,
					10
				],
				[
					3.3333333333332575,
					14.166666666666657
				],
				[
					0.8333333333332575,
					16.666666666666657
				],
				[
					-0.8333333333333712,
					17.5
				],
				[
					-0.8333333333333712,
					17.5
				]
			],
			"lastCommittedPoint": null,
			"simulatePressure": false,
			"pressures": [
				0.12890625,
				0.3134765625,
				0.3212890625,
				0.32421875,
				0.328125,
				0.3310546875,
				0.33203125,
				0.33203125,
				0.33203125,
				0.3330078125,
				0.4140625,
				0.4267578125,
				0.4267578125,
				0.4150390625,
				0.3525390625,
				0.283203125,
				0
			]
		},
		{
			"type": "freedraw",
			"version": 49,
			"versionNonce": 1409265112,
			"isDeleted": false,
			"id": "OAKu6-TZrfYheVtQgbiHR",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 0,
			"opacity": 100,
			"angle": 0,
			"x": 270.83333333333337,
			"y": -207.7018229166666,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"width": 20,
			"height": 31.666666666666686,
			"seed": 2101317288,
			"groupIds": [],
			"strokeSharpness": "round",
			"boundElements": [],
			"updated": 1665930257082,
			"link": null,
			"locked": false,
			"points": [
				[
					0,
					0
				],
				[
					0,
					-0.8333333333333428
				],
				[
					0,
					6.666666666666657
				],
				[
					0,
					12.5
				],
				[
					-0.8333333333333712,
					19.166666666666657
				],
				[
					-0.8333333333333712,
					24.166666666666657
				],
				[
					-0.8333333333333712,
					27.5
				],
				[
					-1.6666666666667425,
					27.5
				],
				[
					-1.6666666666667425,
					25.833333333333314
				],
				[
					-1.6666666666667425,
					22.5
				],
				[
					-0.8333333333333712,
					16.666666666666657
				],
				[
					-0.8333333333333712,
					12.5
				],
				[
					-0.8333333333333712,
					9.166666666666657
				],
				[
					-0.8333333333333712,
					5.833333333333314
				],
				[
					-1.6666666666667425,
					1.6666666666666572
				],
				[
					-1.6666666666667425,
					-0.8333333333333428
				],
				[
					-1.6666666666667425,
					-2.5
				],
				[
					-1.6666666666667425,
					-3.333333333333343
				],
				[
					-1.6666666666667425,
					-4.166666666666686
				],
				[
					0.8333333333332575,
					-4.166666666666686
				],
				[
					3.3333333333332575,
					-4.166666666666686
				],
				[
					6.666666666666629,
					-2.5
				],
				[
					10.833333333333258,
					-1.6666666666666856
				],
				[
					13.333333333333258,
					-1.6666666666666856
				],
				[
					15.833333333333258,
					-1.6666666666666856
				],
				[
					17.5,
					-1.6666666666666856
				],
				[
					18.333333333333258,
					-2.5
				],
				[
					18.333333333333258,
					-2.5
				]
			],
			"lastCommittedPoint": null,
			"simulatePressure": false,
			"pressures": [
				0.0654296875,
				0.2734375,
				0.30859375,
				0.31640625,
				0.3173828125,
				0.3203125,
				0.3193359375,
				0.3193359375,
				0.2978515625,
				0.2998046875,
				0.2978515625,
				0.296875,
				0.296875,
				0.296875,
				0.2978515625,
				0.30078125,
				0.302734375,
				0.302734375,
				0.3037109375,
				0.3037109375,
				0.3046875,
				0.3095703125,
				0.31640625,
				0.3203125,
				0.322265625,
				0.3232421875,
				0.3173828125,
				0
			]
		},
		{
			"type": "freedraw",
			"version": 24,
			"versionNonce": 796703656,
			"isDeleted": false,
			"id": "4PFU9wII02T46c1lw-AOy",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 0,
			"opacity": 100,
			"angle": 0,
			"x": 270.83333333333337,
			"y": -190.2018229166666,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"width": 6.666666666666629,
			"height": 0.8333333333333428,
			"seed": 21951960,
			"groupIds": [],
			"strokeSharpness": "round",
			"boundElements": [],
			"updated": 1665930257082,
			"link": null,
			"locked": false,
			"points": [
				[
					0,
					0
				],
				[
					6.666666666666629,
					-0.8333333333333428
				],
				[
					6.666666666666629,
					-0.8333333333333428
				]
			],
			"lastCommittedPoint": null,
			"simulatePressure": false,
			"pressures": [
				0.1201171875,
				0.2470703125,
				0
			]
		},
		{
			"type": "freedraw",
			"version": 35,
			"versionNonce": 545010392,
			"isDeleted": false,
			"id": "4Lw3FKO_N1vpodBcIWn35",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 0,
			"opacity": 100,
			"angle": 0,
			"x": 287.5,
			"y": -182.7018229166666,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"width": 10,
			"height": 10.833333333333343,
			"seed": 490561496,
			"groupIds": [],
			"strokeSharpness": "round",
			"boundElements": [],
			"updated": 1665930257082,
			"link": null,
			"locked": false,
			"points": [
				[
					0,
					0
				],
				[
					-0.8333333333333712,
					-0.8333333333333428
				],
				[
					-2.5,
					-0.8333333333333428
				],
				[
					-4.166666666666629,
					-0.8333333333333428
				],
				[
					-7.5,
					0
				],
				[
					-9.166666666666629,
					1.6666666666666572
				],
				[
					-10,
					3.3333333333333144
				],
				[
					-10,
					5
				],
				[
					-10,
					8.333333333333314
				],
				[
					-7.5,
					9.166666666666657
				],
				[
					-5,
					10
				],
				[
					-1.6666666666666288,
					10
				],
				[
					0,
					10
				],
				[
					0,
					10
				]
			],
			"lastCommittedPoint": null,
			"simulatePressure": false,
			"pressures": [
				0.048828125,
				0.275390625,
				0.314453125,
				0.328125,
				0.3359375,
				0.3359375,
				0.3349609375,
				0.333984375,
				0.3310546875,
				0.3271484375,
				0.2939453125,
				0.1962890625,
				0.103515625,
				0
			]
		},
		{
			"type": "freedraw",
			"version": 39,
			"versionNonce": 1521944232,
			"isDeleted": false,
			"id": "Yz-cHUS-OJUdjLYJLEIbL",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 0,
			"opacity": 100,
			"angle": 0,
			"x": 235.83333333333337,
			"y": 24.79817708333337,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"width": 56.66666666666663,
			"height": 2.5,
			"seed": 978458280,
			"groupIds": [],
			"strokeSharpness": "round",
			"boundElements": [],
			"updated": 1665930257082,
			"link": null,
			"locked": false,
			"points": [
				[
					0,
					0
				],
				[
					-0.8333333333333712,
					0
				],
				[
					-0.8333333333333712,
					0.8333333333333144
				],
				[
					0,
					0.8333333333333144
				],
				[
					2.5,
					0.8333333333333144
				],
				[
					9.166666666666629,
					0
				],
				[
					13.333333333333258,
					0
				],
				[
					18.333333333333258,
					0
				],
				[
					24.16666666666663,
					0
				],
				[
					30.833333333333258,
					0.8333333333333144
				],
				[
					39.16666666666663,
					1.6666666666666856
				],
				[
					43.33333333333326,
					1.6666666666666856
				],
				[
					46.66666666666663,
					2.5
				],
				[
					50,
					2.5
				],
				[
					53.33333333333326,
					2.5
				],
				[
					55,
					2.5
				],
				[
					55.83333333333326,
					1.6666666666666856
				],
				[
					55.83333333333326,
					1.6666666666666856
				]
			],
			"lastCommittedPoint": null,
			"simulatePressure": false,
			"pressures": [
				0.044921875,
				0.240234375,
				0.2861328125,
				0.2958984375,
				0.3046875,
				0.3212890625,
				0.3349609375,
				0.3486328125,
				0.3720703125,
				0.384765625,
				0.396484375,
				0.4072265625,
				0.41796875,
				0.43359375,
				0.453125,
				0.455078125,
				0.455078125,
				0
			]
		},
		{
			"type": "freedraw",
			"version": 39,
			"versionNonce": 354424792,
			"isDeleted": false,
			"id": "gAGH4tbDoSyKe3wT6iDm1",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 0,
			"opacity": 100,
			"angle": 0,
			"x": 291.66666666666663,
			"y": 19.79817708333337,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"width": 23.33333333333337,
			"height": 15.833333333333314,
			"seed": 2028139736,
			"groupIds": [],
			"strokeSharpness": "round",
			"boundElements": [],
			"updated": 1665930257082,
			"link": null,
			"locked": false,
			"points": [
				[
					0,
					0
				],
				[
					-4.166666666666629,
					-0.8333333333333144
				],
				[
					-5,
					-0.8333333333333144
				],
				[
					-2.5,
					0
				],
				[
					0,
					1.6666666666666856
				],
				[
					3.3333333333333712,
					2.5
				],
				[
					5.833333333333371,
					3.3333333333333144
				],
				[
					10.833333333333371,
					5
				],
				[
					14.166666666666742,
					5.833333333333314
				],
				[
					16.666666666666742,
					5.833333333333314
				],
				[
					17.5,
					6.666666666666686
				],
				[
					18.33333333333337,
					6.666666666666686
				],
				[
					16.666666666666742,
					7.5
				],
				[
					12.5,
					9.166666666666686
				],
				[
					6.6666666666667425,
					11.666666666666686
				],
				[
					-0.8333333333332575,
					14.166666666666686
				],
				[
					-3.3333333333332575,
					15
				],
				[
					-3.3333333333332575,
					15
				]
			],
			"lastCommittedPoint": null,
			"simulatePressure": false,
			"pressures": [
				0.068359375,
				0.25390625,
				0.2939453125,
				0.34375,
				0.3466796875,
				0.3525390625,
				0.3583984375,
				0.373046875,
				0.3798828125,
				0.392578125,
				0.3955078125,
				0.3984375,
				0.4521484375,
				0.453125,
				0.453125,
				0.443359375,
				0.365234375,
				0
			]
		},
		{
			"type": "freedraw",
			"version": 51,
			"versionNonce": 1584394664,
			"isDeleted": false,
			"id": "y9D1mK_IfIpSQ9kBfAiPV",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 0,
			"opacity": 100,
			"angle": 0,
			"x": 329.16666666666663,
			"y": -6.868489583333314,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"width": 15.833333333333371,
			"height": 34.166666666666686,
			"seed": 1829398440,
			"groupIds": [],
			"strokeSharpness": "round",
			"boundElements": [],
			"updated": 1665930257082,
			"link": null,
			"locked": false,
			"points": [
				[
					0,
					0
				],
				[
					0,
					-1.6666666666666288
				],
				[
					0,
					-0.8333333333333144
				],
				[
					0,
					1.6666666666666856
				],
				[
					0,
					5.833333333333371
				],
				[
					0,
					10.833333333333371
				],
				[
					0,
					17.5
				],
				[
					0,
					20.83333333333337
				],
				[
					0,
					23.33333333333337
				],
				[
					0,
					25
				],
				[
					0,
					25.83333333333337
				],
				[
					0,
					25
				],
				[
					0,
					21.666666666666686
				],
				[
					0,
					18.33333333333337
				],
				[
					0,
					11.666666666666686
				],
				[
					0,
					6.666666666666686
				],
				[
					0,
					2.5
				],
				[
					-0.8333333333332575,
					-2.5
				],
				[
					-1.6666666666666288,
					-6.666666666666629
				],
				[
					-1.6666666666666288,
					-7.5
				],
				[
					-1.6666666666666288,
					-8.333333333333314
				],
				[
					-0.8333333333332575,
					-7.5
				],
				[
					1.6666666666667425,
					-6.666666666666629
				],
				[
					5,
					-5.833333333333314
				],
				[
					7.5,
					-5.833333333333314
				],
				[
					10.833333333333371,
					-5.833333333333314
				],
				[
					12.5,
					-5.833333333333314
				],
				[
					13.333333333333371,
					-5.833333333333314
				],
				[
					14.166666666666742,
					-5.833333333333314
				],
				[
					14.166666666666742,
					-5.833333333333314
				]
			],
			"lastCommittedPoint": null,
			"simulatePressure": false,
			"pressures": [
				0.087890625,
				0.25390625,
				0.3046875,
				0.310546875,
				0.3154296875,
				0.3232421875,
				0.326171875,
				0.3291015625,
				0.328125,
				0.328125,
				0.3291015625,
				0.3203125,
				0.3203125,
				0.3232421875,
				0.322265625,
				0.32421875,
				0.3251953125,
				0.322265625,
				0.3115234375,
				0.3115234375,
				0.3115234375,
				0.3115234375,
				0.3115234375,
				0.3125,
				0.310546875,
				0.310546875,
				0.310546875,
				0.310546875,
				0.2734375,
				0
			]
		},
		{
			"type": "freedraw",
			"version": 27,
			"versionNonce": 1979669720,
			"isDeleted": false,
			"id": "WX3Kul84xkwdzBI4ZlJEA",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 0,
			"opacity": 100,
			"angle": 0,
			"x": 330,
			"y": 7.298177083333371,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"width": 8.333333333333371,
			"height": 0.8333333333333144,
			"seed": 781881816,
			"groupIds": [],
			"strokeSharpness": "round",
			"boundElements": [],
			"updated": 1665930257082,
			"link": null,
			"locked": false,
			"points": [
				[
					0,
					0
				],
				[
					3.3333333333333712,
					-0.8333333333333144
				],
				[
					5,
					-0.8333333333333144
				],
				[
					6.666666666666629,
					-0.8333333333333144
				],
				[
					8.333333333333371,
					-0.8333333333333144
				],
				[
					8.333333333333371,
					-0.8333333333333144
				]
			],
			"lastCommittedPoint": null,
			"simulatePressure": false,
			"pressures": [
				0.0849609375,
				0.2568359375,
				0.259765625,
				0.2578125,
				0.1923828125,
				0
			]
		},
		{
			"type": "freedraw",
			"version": 35,
			"versionNonce": 1909685416,
			"isDeleted": false,
			"id": "scamRV6BjbwCv6mB0DAkc",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 0,
			"opacity": 100,
			"angle": 0,
			"x": 350.83333333333337,
			"y": 14.798177083333371,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"width": 12.5,
			"height": 10.833333333333314,
			"seed": 2061836760,
			"groupIds": [],
			"strokeSharpness": "round",
			"boundElements": [],
			"updated": 1665930257083,
			"link": null,
			"locked": false,
			"points": [
				[
					0,
					0
				],
				[
					-5.833333333333371,
					1.6666666666666856
				],
				[
					-6.6666666666667425,
					1.6666666666666856
				],
				[
					-7.5,
					2.5
				],
				[
					-7.5,
					4.166666666666686
				],
				[
					-8.333333333333371,
					5.833333333333314
				],
				[
					-8.333333333333371,
					8.333333333333314
				],
				[
					-8.333333333333371,
					10
				],
				[
					-8.333333333333371,
					10.833333333333314
				],
				[
					-6.6666666666667425,
					10.833333333333314
				],
				[
					-1.6666666666667425,
					10.833333333333314
				],
				[
					0.8333333333332575,
					10.833333333333314
				],
				[
					2.5,
					10
				],
				[
					3.3333333333332575,
					9.166666666666686
				],
				[
					4.166666666666629,
					9.166666666666686
				],
				[
					4.166666666666629,
					9.166666666666686
				]
			],
			"lastCommittedPoint": null,
			"simulatePressure": false,
			"pressures": [
				0.10546875,
				0.2646484375,
				0.2783203125,
				0.287109375,
				0.29296875,
				0.2958984375,
				0.2978515625,
				0.2978515625,
				0.2958984375,
				0.2919921875,
				0.2783203125,
				0.267578125,
				0.255859375,
				0.244140625,
				0.078125,
				0
			]
		},
		{
			"type": "freedraw",
			"version": 87,
			"versionNonce": 1689125336,
			"isDeleted": false,
			"id": "KAs67ojt51107RB2_-pOM",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 0,
			"opacity": 100,
			"angle": 0,
			"x": -402.49999999999994,
			"y": -186.03515624999983,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"width": 69.16666666666663,
			"height": 16.666666666666657,
			"seed": 1368940456,
			"groupIds": [],
			"strokeSharpness": "round",
			"boundElements": [],
			"updated": 1665930257083,
			"link": null,
			"locked": false,
			"points": [
				[
					0,
					0
				],
				[
					-1.6666666666666288,
					0
				],
				[
					-2.5,
					0
				],
				[
					-3.3333333333333144,
					0
				],
				[
					-4.166666666666629,
					0
				],
				[
					-5,
					0
				],
				[
					-6.666666666666629,
					0
				],
				[
					-9.166666666666629,
					0.8333333333333428
				],
				[
					-10.833333333333314,
					0.8333333333333428
				],
				[
					-12.5,
					0.8333333333333428
				],
				[
					-14.166666666666629,
					0.8333333333333428
				],
				[
					-15.833333333333314,
					0.8333333333333428
				],
				[
					-17.5,
					0.8333333333333428
				],
				[
					-19.16666666666663,
					1.6666666666666572
				],
				[
					-20,
					1.6666666666666572
				],
				[
					-21.66666666666663,
					1.6666666666666572
				],
				[
					-22.5,
					1.6666666666666572
				],
				[
					-25,
					1.6666666666666572
				],
				[
					-25.833333333333314,
					1.6666666666666572
				],
				[
					-28.333333333333314,
					1.6666666666666572
				],
				[
					-30.833333333333314,
					1.6666666666666572
				],
				[
					-33.333333333333314,
					1.6666666666666572
				],
				[
					-35,
					1.6666666666666572
				],
				[
					-37.5,
					1.6666666666666572
				],
				[
					-40,
					1.6666666666666572
				],
				[
					-43.333333333333314,
					1.6666666666666572
				],
				[
					-45,
					1.6666666666666572
				],
				[
					-47.5,
					1.6666666666666572
				],
				[
					-49.16666666666663,
					1.6666666666666572
				],
				[
					-52.5,
					1.6666666666666572
				],
				[
					-54.16666666666663,
					1.6666666666666572
				],
				[
					-55.833333333333314,
					1.6666666666666572
				],
				[
					-56.66666666666663,
					1.6666666666666572
				],
				[
					-58.333333333333314,
					1.6666666666666572
				],
				[
					-59.16666666666663,
					1.6666666666666572
				],
				[
					-60,
					1.6666666666666572
				],
				[
					-60.833333333333314,
					1.6666666666666572
				],
				[
					-61.66666666666663,
					1.6666666666666572
				],
				[
					-62.5,
					1.6666666666666572
				],
				[
					-63.333333333333314,
					1.6666666666666572
				],
				[
					-64.16666666666663,
					1.6666666666666572
				],
				[
					-64.16666666666663,
					0.8333333333333428
				],
				[
					-63.333333333333314,
					-0.8333333333333428
				],
				[
					-61.66666666666663,
					-3.333333333333343
				],
				[
					-59.16666666666663,
					-5
				],
				[
					-57.5,
					-5
				],
				[
					-56.66666666666663,
					-5.833333333333343
				],
				[
					-57.5,
					-5.833333333333343
				],
				[
					-59.16666666666663,
					-5.833333333333343
				],
				[
					-60.833333333333314,
					-4.166666666666657
				],
				[
					-62.5,
					-3.333333333333343
				],
				[
					-65.83333333333331,
					-1.6666666666666572
				],
				[
					-66.66666666666663,
					0
				],
				[
					-67.5,
					0.8333333333333428
				],
				[
					-68.33333333333331,
					1.6666666666666572
				],
				[
					-69.16666666666663,
					1.6666666666666572
				],
				[
					-69.16666666666663,
					2.5
				],
				[
					-68.33333333333331,
					2.5
				],
				[
					-66.66666666666663,
					4.166666666666657
				],
				[
					-64.16666666666663,
					4.166666666666657
				],
				[
					-61.66666666666663,
					5
				],
				[
					-60,
					5.833333333333343
				],
				[
					-58.333333333333314,
					5.833333333333343
				],
				[
					-56.66666666666663,
					6.666666666666657
				],
				[
					-55.833333333333314,
					7.5
				],
				[
					-55,
					9.166666666666657
				],
				[
					-55,
					10.833333333333314
				],
				[
					-55,
					10.833333333333314
				]
			],
			"lastCommittedPoint": null,
			"simulatePressure": false,
			"pressures": [
				0.15625,
				0.3251953125,
				0.3369140625,
				0.3583984375,
				0.3720703125,
				0.3837890625,
				0.3994140625,
				0.41796875,
				0.42578125,
				0.43359375,
				0.451171875,
				0.4541015625,
				0.45703125,
				0.4609375,
				0.4677734375,
				0.47265625,
				0.4794921875,
				0.48828125,
				0.4892578125,
				0.490234375,
				0.4912109375,
				0.4912109375,
				0.4912109375,
				0.4912109375,
				0.4921875,
				0.4931640625,
				0.4931640625,
				0.4931640625,
				0.494140625,
				0.4951171875,
				0.4951171875,
				0.4951171875,
				0.4951171875,
				0.4951171875,
				0.494140625,
				0.494140625,
				0.4921875,
				0.4921875,
				0.490234375,
				0.4892578125,
				0.4853515625,
				0.439453125,
				0.4267578125,
				0.4208984375,
				0.4169921875,
				0.4150390625,
				0.4150390625,
				0.4248046875,
				0.4248046875,
				0.423828125,
				0.4228515625,
				0.419921875,
				0.416015625,
				0.4130859375,
				0.4111328125,
				0.4091796875,
				0.40234375,
				0.3994140625,
				0.3984375,
				0.3984375,
				0.3984375,
				0.3984375,
				0.3984375,
				0.3984375,
				0.375,
				0.2939453125,
				0.1025390625,
				0
			]
		},
		{
			"type": "freedraw",
			"version": 89,
			"versionNonce": 109946792,
			"isDeleted": false,
			"id": "ThNdQGg1e6aLT937zLXj-",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 0,
			"opacity": 100,
			"angle": 0,
			"x": -401.6666666666666,
			"y": 37.298177083333485,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"width": 70.83333333333337,
			"height": 18.33333333333337,
			"seed": 1775737048,
			"groupIds": [],
			"strokeSharpness": "round",
			"boundElements": [],
			"updated": 1665930257083,
			"link": null,
			"locked": false,
			"points": [
				[
					0,
					0
				],
				[
					0,
					0.8333333333333144
				],
				[
					0,
					1.6666666666666856
				],
				[
					-0.8333333333333712,
					1.6666666666666856
				],
				[
					-0.8333333333333712,
					2.5
				],
				[
					-1.6666666666666856,
					2.5
				],
				[
					-3.3333333333333712,
					2.5
				],
				[
					-5,
					2.5
				],
				[
					-6.666666666666686,
					2.5
				],
				[
					-9.166666666666686,
					2.5
				],
				[
					-13.333333333333371,
					2.5
				],
				[
					-15.833333333333371,
					2.5
				],
				[
					-19.166666666666686,
					2.5
				],
				[
					-21.666666666666686,
					2.5
				],
				[
					-26.666666666666686,
					1.6666666666666856
				],
				[
					-30.83333333333337,
					0.8333333333333144
				],
				[
					-31.666666666666686,
					0.8333333333333144
				],
				[
					-34.166666666666686,
					0.8333333333333144
				],
				[
					-35.83333333333337,
					0.8333333333333144
				],
				[
					-38.33333333333337,
					0.8333333333333144
				],
				[
					-40,
					0.8333333333333144
				],
				[
					-41.666666666666686,
					0.8333333333333144
				],
				[
					-43.33333333333337,
					0.8333333333333144
				],
				[
					-45,
					0.8333333333333144
				],
				[
					-47.5,
					0.8333333333333144
				],
				[
					-50,
					0.8333333333333144
				],
				[
					-50.83333333333337,
					0.8333333333333144
				],
				[
					-51.666666666666686,
					0.8333333333333144
				],
				[
					-53.33333333333337,
					0.8333333333333144
				],
				[
					-54.166666666666686,
					0.8333333333333144
				],
				[
					-55.83333333333337,
					0.8333333333333144
				],
				[
					-56.666666666666686,
					0.8333333333333144
				],
				[
					-57.5,
					0.8333333333333144
				],
				[
					-59.166666666666686,
					0.8333333333333144
				],
				[
					-60.83333333333337,
					1.6666666666666856
				],
				[
					-61.666666666666686,
					1.6666666666666856
				],
				[
					-62.5,
					1.6666666666666856
				],
				[
					-64.16666666666669,
					1.6666666666666856
				],
				[
					-65,
					1.6666666666666856
				],
				[
					-65,
					2.5
				],
				[
					-65.83333333333337,
					2.5
				],
				[
					-66.66666666666669,
					2.5
				],
				[
					-67.5,
					2.5
				],
				[
					-68.33333333333337,
					2.5
				],
				[
					-67.5,
					1.6666666666666856
				],
				[
					-63.33333333333337,
					-1.6666666666666856
				],
				[
					-60.83333333333337,
					-4.166666666666686
				],
				[
					-58.33333333333337,
					-5.833333333333314
				],
				[
					-57.5,
					-6.666666666666686
				],
				[
					-57.5,
					-5.833333333333314
				],
				[
					-57.5,
					-5
				],
				[
					-59.166666666666686,
					-4.166666666666686
				],
				[
					-62.5,
					-2.5
				],
				[
					-64.16666666666669,
					-0.8333333333333144
				],
				[
					-66.66666666666669,
					0
				],
				[
					-69.16666666666669,
					0.8333333333333144
				],
				[
					-70,
					1.6666666666666856
				],
				[
					-70.83333333333337,
					1.6666666666666856
				],
				[
					-70.83333333333337,
					2.5
				],
				[
					-70.83333333333337,
					3.3333333333333144
				],
				[
					-70,
					4.166666666666686
				],
				[
					-69.16666666666669,
					5.833333333333314
				],
				[
					-66.66666666666669,
					6.666666666666686
				],
				[
					-64.16666666666669,
					7.5
				],
				[
					-61.666666666666686,
					8.333333333333314
				],
				[
					-58.33333333333337,
					10
				],
				[
					-55.83333333333337,
					10.833333333333314
				],
				[
					-53.33333333333337,
					11.666666666666686
				],
				[
					-54.166666666666686,
					11.666666666666686
				],
				[
					-54.166666666666686,
					11.666666666666686
				]
			],
			"lastCommittedPoint": null,
			"simulatePressure": false,
			"pressures": [
				0.0185546875,
				0.1904296875,
				0.23046875,
				0.2578125,
				0.2861328125,
				0.3212890625,
				0.36328125,
				0.38671875,
				0.392578125,
				0.40234375,
				0.4140625,
				0.41796875,
				0.419921875,
				0.421875,
				0.423828125,
				0.4248046875,
				0.4248046875,
				0.4248046875,
				0.4248046875,
				0.4267578125,
				0.4267578125,
				0.4248046875,
				0.4248046875,
				0.421875,
				0.419921875,
				0.4208984375,
				0.419921875,
				0.419921875,
				0.4189453125,
				0.4169921875,
				0.416015625,
				0.416015625,
				0.416015625,
				0.4150390625,
				0.4130859375,
				0.4111328125,
				0.41015625,
				0.4052734375,
				0.4052734375,
				0.4052734375,
				0.404296875,
				0.4033203125,
				0.3974609375,
				0.3955078125,
				0.3671875,
				0.3671875,
				0.3681640625,
				0.3681640625,
				0.3681640625,
				0.373046875,
				0.375,
				0.376953125,
				0.3779296875,
				0.3779296875,
				0.3779296875,
				0.37890625,
				0.376953125,
				0.375,
				0.359375,
				0.357421875,
				0.3564453125,
				0.3544921875,
				0.3525390625,
				0.3525390625,
				0.353515625,
				0.3564453125,
				0.3583984375,
				0.3642578125,
				0.25,
				0
			]
		},
		{
			"type": "freedraw",
			"version": 47,
			"versionNonce": 809502424,
			"isDeleted": false,
			"id": "-7mhlRWJPOgrhxL--a_A5",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 0,
			"opacity": 100,
			"angle": 0,
			"x": -518.3333333333333,
			"y": 15.6315104166668,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"width": 17.500000000000057,
			"height": 34.166666666666686,
			"seed": 13975976,
			"groupIds": [],
			"strokeSharpness": "round",
			"boundElements": [],
			"updated": 1665930257083,
			"link": null,
			"locked": false,
			"points": [
				[
					0,
					0
				],
				[
					-0.8333333333332575,
					0
				],
				[
					-0.8333333333332575,
					1.6666666666666856
				],
				[
					-0.8333333333332575,
					8.333333333333371
				],
				[
					-0.8333333333332575,
					13.333333333333371
				],
				[
					0,
					19.166666666666686
				],
				[
					0,
					24.166666666666686
				],
				[
					0,
					28.33333333333337
				],
				[
					0,
					30
				],
				[
					0,
					30.83333333333337
				],
				[
					0,
					30
				],
				[
					0,
					27.5
				],
				[
					-0.8333333333332575,
					20.83333333333337
				],
				[
					-2.5,
					15.833333333333371
				],
				[
					-3.3333333333332575,
					11.666666666666686
				],
				[
					-4.1666666666667425,
					7.5
				],
				[
					-4.1666666666667425,
					4.166666666666686
				],
				[
					-4.1666666666667425,
					0.8333333333333712
				],
				[
					-4.1666666666667425,
					-1.6666666666666288
				],
				[
					-2.5,
					-2.5
				],
				[
					-1.6666666666667425,
					-2.5
				],
				[
					1.6666666666667425,
					-2.5
				],
				[
					4.1666666666667425,
					-2.5
				],
				[
					7.5,
					-2.5
				],
				[
					10,
					-3.3333333333333144
				],
				[
					12.5,
					-3.3333333333333144
				],
				[
					13.333333333333314,
					-3.3333333333333144
				],
				[
					13.333333333333314,
					-3.3333333333333144
				]
			],
			"lastCommittedPoint": null,
			"simulatePressure": false,
			"pressures": [
				0.0986328125,
				0.25,
				0.265625,
				0.2822265625,
				0.29296875,
				0.30078125,
				0.3046875,
				0.306640625,
				0.3076171875,
				0.3076171875,
				0.2841796875,
				0.2861328125,
				0.287109375,
				0.28515625,
				0.2841796875,
				0.2841796875,
				0.2841796875,
				0.2841796875,
				0.2861328125,
				0.28515625,
				0.28515625,
				0.30078125,
				0.3076171875,
				0.314453125,
				0.31640625,
				0.3173828125,
				0.31640625,
				0
			]
		},
		{
			"type": "freedraw",
			"version": 26,
			"versionNonce": 2026533544,
			"isDeleted": false,
			"id": "bvd-UiCL2kIHofwxOsOLu",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 0,
			"opacity": 100,
			"angle": 0,
			"x": -520.8333333333333,
			"y": 33.1315104166668,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"width": 7.5,
			"height": 0.8333333333333712,
			"seed": 1165177560,
			"groupIds": [],
			"strokeSharpness": "round",
			"boundElements": [],
			"updated": 1665930257083,
			"link": null,
			"locked": false,
			"points": [
				[
					0,
					0
				],
				[
					1.6666666666667425,
					0.8333333333333712
				],
				[
					3.3333333333332575,
					0.8333333333333712
				],
				[
					5,
					0.8333333333333712
				],
				[
					6.6666666666667425,
					0.8333333333333712
				],
				[
					7.5,
					0.8333333333333712
				],
				[
					7.5,
					0.8333333333333712
				]
			],
			"lastCommittedPoint": null,
			"simulatePressure": false,
			"pressures": [
				0.1162109375,
				0.2470703125,
				0.265625,
				0.2734375,
				0.2724609375,
				0.25,
				0
			]
		},
		{
			"type": "freedraw",
			"version": 34,
			"versionNonce": 625568728,
			"isDeleted": false,
			"id": "7JfWzXdObrPbWU2M-7bnv",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 0,
			"opacity": 100,
			"angle": 0,
			"x": -499.1666666666666,
			"y": 45.6315104166668,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"width": 12.5,
			"height": 9.166666666666686,
			"seed": 565642968,
			"groupIds": [],
			"strokeSharpness": "round",
			"boundElements": [],
			"updated": 1665930257083,
			"link": null,
			"locked": false,
			"points": [
				[
					0,
					0
				],
				[
					-4.166666666666686,
					-0.8333333333333144
				],
				[
					-6.666666666666686,
					-0.8333333333333144
				],
				[
					-8.333333333333371,
					-0.8333333333333144
				],
				[
					-10,
					0.8333333333333712
				],
				[
					-10.833333333333371,
					1.6666666666666856
				],
				[
					-11.666666666666686,
					3.3333333333333712
				],
				[
					-12.5,
					4.166666666666686
				],
				[
					-12.5,
					6.666666666666686
				],
				[
					-10.833333333333371,
					7.5
				],
				[
					-10,
					8.333333333333371
				],
				[
					-6.666666666666686,
					8.333333333333371
				],
				[
					-4.166666666666686,
					7.5
				],
				[
					-2.5,
					7.5
				],
				[
					-2.5,
					7.5
				]
			],
			"lastCommittedPoint": null,
			"simulatePressure": false,
			"pressures": [
				0.0615234375,
				0.2568359375,
				0.287109375,
				0.2958984375,
				0.3076171875,
				0.310546875,
				0.3154296875,
				0.31640625,
				0.314453125,
				0.3134765625,
				0.3076171875,
				0.2861328125,
				0.2509765625,
				0.1376953125,
				0
			]
		},
		{
			"type": "freedraw",
			"version": 43,
			"versionNonce": 1060494760,
			"isDeleted": false,
			"id": "l88ShpiMV2rrX5EXmBv_3",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 0,
			"opacity": 100,
			"angle": 0,
			"x": -525,
			"y": -215.2018229166665,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"width": 16.666666666666686,
			"height": 31.666666666666657,
			"seed": 530624216,
			"groupIds": [],
			"strokeSharpness": "round",
			"boundElements": [],
			"updated": 1665930257083,
			"link": null,
			"locked": false,
			"points": [
				[
					0,
					0
				],
				[
					0,
					6.666666666666657
				],
				[
					0,
					10.833333333333314
				],
				[
					0,
					19.166666666666657
				],
				[
					0,
					23.333333333333314
				],
				[
					0,
					25.833333333333314
				],
				[
					0,
					26.666666666666657
				],
				[
					0,
					25.833333333333314
				],
				[
					0,
					21.666666666666657
				],
				[
					0,
					17.5
				],
				[
					0,
					13.333333333333314
				],
				[
					-0.8333333333332575,
					9.166666666666657
				],
				[
					-0.8333333333332575,
					2.5
				],
				[
					-0.8333333333332575,
					-0.8333333333333428
				],
				[
					-0.8333333333332575,
					-3.333333333333343
				],
				[
					0,
					-4.166666666666686
				],
				[
					1.6666666666667425,
					-4.166666666666686
				],
				[
					3.333333333333485,
					-4.166666666666686
				],
				[
					6.6666666666667425,
					-4.166666666666686
				],
				[
					9.166666666666742,
					-4.166666666666686
				],
				[
					13.333333333333428,
					-4.166666666666686
				],
				[
					15.000000000000057,
					-5
				],
				[
					15.833333333333428,
					-5
				],
				[
					15.833333333333428,
					-5
				]
			],
			"lastCommittedPoint": null,
			"simulatePressure": false,
			"pressures": [
				0.134765625,
				0.1826171875,
				0.1982421875,
				0.2236328125,
				0.2373046875,
				0.240234375,
				0.2421875,
				0.23046875,
				0.2353515625,
				0.23828125,
				0.2412109375,
				0.2412109375,
				0.2412109375,
				0.2421875,
				0.2431640625,
				0.2431640625,
				0.248046875,
				0.265625,
				0.27734375,
				0.283203125,
				0.2861328125,
				0.2841796875,
				0.28125,
				0
			]
		},
		{
			"type": "freedraw",
			"version": 25,
			"versionNonce": 1574842584,
			"isDeleted": false,
			"id": "mlSRpY8LUwd3EeCT8LZr7",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 0,
			"opacity": 100,
			"angle": 0,
			"x": -526.6666666666665,
			"y": -197.7018229166665,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"width": 11.666666666666515,
			"height": 0.8333333333333144,
			"seed": 333090472,
			"groupIds": [],
			"strokeSharpness": "round",
			"boundElements": [],
			"updated": 1665930257083,
			"link": null,
			"locked": false,
			"points": [
				[
					0,
					0
				],
				[
					5.8333333333332575,
					0.8333333333333144
				],
				[
					8.333333333333258,
					0.8333333333333144
				],
				[
					10.833333333333258,
					0.8333333333333144
				],
				[
					11.666666666666515,
					0.8333333333333144
				],
				[
					11.666666666666515,
					0.8333333333333144
				]
			],
			"lastCommittedPoint": null,
			"simulatePressure": false,
			"pressures": [
				0.1044921875,
				0.24609375,
				0.2470703125,
				0.2373046875,
				0.173828125,
				0
			]
		},
		{
			"type": "freedraw",
			"version": 35,
			"versionNonce": 452716712,
			"isDeleted": false,
			"id": "DfTo93JX5yljAyXiCgja7",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 0,
			"opacity": 100,
			"angle": 0,
			"x": -507.49999999999994,
			"y": -188.53515624999983,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"width": 8.333333333333314,
			"height": 10.833333333333343,
			"seed": 2096889560,
			"groupIds": [],
			"strokeSharpness": "round",
			"boundElements": [],
			"updated": 1665930257083,
			"link": null,
			"locked": false,
			"points": [
				[
					0,
					0
				],
				[
					-1.6666666666666288,
					-0.8333333333333428
				],
				[
					-2.5,
					-0.8333333333333428
				],
				[
					-3.3333333333333144,
					-0.8333333333333428
				],
				[
					-5.000000000000057,
					0
				],
				[
					-5.833333333333314,
					0.8333333333333428
				],
				[
					-7.500000000000057,
					3.333333333333343
				],
				[
					-8.333333333333314,
					5
				],
				[
					-8.333333333333314,
					6.666666666666657
				],
				[
					-8.333333333333314,
					8.333333333333343
				],
				[
					-7.500000000000057,
					10
				],
				[
					-6.666666666666572,
					10
				],
				[
					-3.3333333333333144,
					10
				],
				[
					-2.5,
					10
				],
				[
					0,
					10
				],
				[
					0,
					10
				]
			],
			"lastCommittedPoint": null,
			"simulatePressure": false,
			"pressures": [
				0.056640625,
				0.2392578125,
				0.2705078125,
				0.279296875,
				0.2841796875,
				0.2861328125,
				0.287109375,
				0.2890625,
				0.2900390625,
				0.2900390625,
				0.2900390625,
				0.2890625,
				0.2763671875,
				0.26171875,
				0.171875,
				0
			]
		},
		{
			"type": "freedraw",
			"version": 81,
			"versionNonce": 294802904,
			"isDeleted": false,
			"id": "NG_f-VX94HgKV51XgY3H-",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 0,
			"opacity": 100,
			"angle": 0,
			"x": 218.33333333333337,
			"y": -305.2018229166665,
			"strokeColor": "#2b8a3e",
			"backgroundColor": "transparent",
			"width": 65.83333333333337,
			"height": 54.16666666666666,
			"seed": 1472647848,
			"groupIds": [],
			"strokeSharpness": "round",
			"boundElements": [],
			"updated": 1665930257083,
			"link": null,
			"locked": false,
			"points": [
				[
					0,
					0
				],
				[
					-5.833333333333371,
					0
				],
				[
					-7.5,
					0
				],
				[
					-11.666666666666629,
					0.8333333333333144
				],
				[
					-16.66666666666663,
					1.6666666666666288
				],
				[
					-20,
					2.5
				],
				[
					-22.5,
					3.3333333333333144
				],
				[
					-25,
					5.833333333333314
				],
				[
					-26.66666666666663,
					7.5
				],
				[
					-28.33333333333337,
					9.166666666666629
				],
				[
					-29.16666666666663,
					10.833333333333314
				],
				[
					-30,
					12.5
				],
				[
					-31.66666666666663,
					15
				],
				[
					-32.5,
					18.333333333333314
				],
				[
					-32.5,
					20
				],
				[
					-33.33333333333337,
					21.66666666666663
				],
				[
					-33.33333333333337,
					24.16666666666663
				],
				[
					-35,
					26.66666666666663
				],
				[
					-35.83333333333337,
					29.16666666666663
				],
				[
					-36.66666666666663,
					30.833333333333314
				],
				[
					-37.5,
					33.333333333333314
				],
				[
					-39.16666666666663,
					35
				],
				[
					-40.83333333333337,
					37.5
				],
				[
					-42.5,
					39.16666666666663
				],
				[
					-44.16666666666663,
					40.833333333333314
				],
				[
					-45,
					41.66666666666663
				],
				[
					-47.5,
					44.16666666666663
				],
				[
					-48.33333333333337,
					45.833333333333314
				],
				[
					-50,
					46.66666666666663
				],
				[
					-50.83333333333337,
					47.5
				],
				[
					-52.5,
					48.333333333333314
				],
				[
					-55,
					49.16666666666663
				],
				[
					-56.66666666666663,
					49.99999999999997
				],
				[
					-57.5,
					49.99999999999997
				],
				[
					-58.33333333333337,
					50.833333333333314
				],
				[
					-60,
					50.833333333333314
				],
				[
					-60.83333333333337,
					50.833333333333314
				],
				[
					-60.83333333333337,
					49.99999999999997
				],
				[
					-60.83333333333337,
					49.16666666666663
				],
				[
					-59.16666666666663,
					46.66666666666663
				],
				[
					-59.16666666666663,
					45
				],
				[
					-58.33333333333337,
					44.16666666666663
				],
				[
					-58.33333333333337,
					42.5
				],
				[
					-57.5,
					42.5
				],
				[
					-57.5,
					41.66666666666663
				],
				[
					-57.5,
					42.5
				],
				[
					-58.33333333333337,
					42.5
				],
				[
					-59.16666666666663,
					44.16666666666663
				],
				[
					-60.83333333333337,
					45.833333333333314
				],
				[
					-62.5,
					48.333333333333314
				],
				[
					-64.16666666666663,
					50.833333333333314
				],
				[
					-65,
					51.66666666666666
				],
				[
					-65,
					53.333333333333314
				],
				[
					-65.83333333333337,
					54.16666666666666
				],
				[
					-65,
					54.16666666666666
				],
				[
					-63.33333333333337,
					54.16666666666666
				],
				[
					-60.83333333333337,
					53.333333333333314
				],
				[
					-58.33333333333337,
					53.333333333333314
				],
				[
					-56.66666666666663,
					53.333333333333314
				],
				[
					-53.33333333333337,
					53.333333333333314
				],
				[
					-51.66666666666663,
					53.333333333333314
				],
				[
					-51.66666666666663,
					53.333333333333314
				]
			],
			"lastCommittedPoint": null,
			"simulatePressure": false,
			"pressures": [
				0.078125,
				0.212890625,
				0.23828125,
				0.2587890625,
				0.2880859375,
				0.2998046875,
				0.3037109375,
				0.3056640625,
				0.3076171875,
				0.3095703125,
				0.3095703125,
				0.310546875,
				0.3115234375,
				0.3115234375,
				0.3115234375,
				0.3134765625,
				0.314453125,
				0.3154296875,
				0.3193359375,
				0.3212890625,
				0.3232421875,
				0.3212890625,
				0.3232421875,
				0.3232421875,
				0.3212890625,
				0.3232421875,
				0.32421875,
				0.32421875,
				0.32421875,
				0.3251953125,
				0.3251953125,
				0.3251953125,
				0.326171875,
				0.326171875,
				0.3271484375,
				0.3271484375,
				0.3271484375,
				0.3271484375,
				0.3271484375,
				0.326171875,
				0.3251953125,
				0.3251953125,
				0.32421875,
				0.32421875,
				0.32421875,
				0.3203125,
				0.3212890625,
				0.3251953125,
				0.3251953125,
				0.3251953125,
				0.326171875,
				0.3251953125,
				0.3251953125,
				0.3251953125,
				0.3203125,
				0.3203125,
				0.3212890625,
				0.3232421875,
				0.32421875,
				0.328125,
				0.3310546875,
				0
			]
		},
		{
			"type": "freedraw",
			"version": 49,
			"versionNonce": 1204327336,
			"isDeleted": false,
			"id": "BlG6VmS_xidO6WJmEXyXs",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 0,
			"opacity": 100,
			"angle": 0,
			"x": 227.5,
			"y": -304.3684895833332,
			"strokeColor": "#2b8a3e",
			"backgroundColor": "transparent",
			"width": 21.666666666666742,
			"height": 22.5,
			"seed": 346310360,
			"groupIds": [],
			"strokeSharpness": "round",
			"boundElements": [],
			"updated": 1665930257083,
			"link": null,
			"locked": false,
			"points": [
				[
					0,
					0
				],
				[
					0,
					-0.8333333333333144
				],
				[
					0,
					-1.6666666666666856
				],
				[
					0.8333333333333712,
					-4.166666666666686
				],
				[
					1.6666666666667425,
					-6.666666666666686
				],
				[
					2.5,
					-10.833333333333314
				],
				[
					5,
					-15.833333333333314
				],
				[
					6.6666666666667425,
					-19.166666666666686
				],
				[
					7.5,
					-20.833333333333314
				],
				[
					7.5,
					-20
				],
				[
					8.333333333333371,
					-19.166666666666686
				],
				[
					8.333333333333371,
					-17.5
				],
				[
					9.166666666666742,
					-15.833333333333314
				],
				[
					10.833333333333371,
					-14.166666666666686
				],
				[
					11.666666666666742,
					-13.333333333333314
				],
				[
					12.5,
					-13.333333333333314
				],
				[
					14.166666666666742,
					-13.333333333333314
				],
				[
					16.666666666666742,
					-16.666666666666686
				],
				[
					18.33333333333337,
					-19.166666666666686
				],
				[
					19.166666666666742,
					-21.666666666666686
				],
				[
					20,
					-22.5
				],
				[
					20.83333333333337,
					-22.5
				],
				[
					20.83333333333337,
					-21.666666666666686
				],
				[
					20.83333333333337,
					-20
				],
				[
					20.83333333333337,
					-15.833333333333314
				],
				[
					20.83333333333337,
					-11.666666666666686
				],
				[
					20.83333333333337,
					-5.833333333333314
				],
				[
					20.83333333333337,
					-3.3333333333333144
				],
				[
					21.666666666666742,
					-2.5
				],
				[
					21.666666666666742,
					-2.5
				]
			],
			"lastCommittedPoint": null,
			"simulatePressure": false,
			"pressures": [
				0.10546875,
				0.337890625,
				0.3408203125,
				0.3427734375,
				0.3427734375,
				0.34375,
				0.345703125,
				0.3466796875,
				0.3466796875,
				0.33984375,
				0.3427734375,
				0.3447265625,
				0.3447265625,
				0.3427734375,
				0.337890625,
				0.3310546875,
				0.31640625,
				0.296875,
				0.2939453125,
				0.2900390625,
				0.2890625,
				0.2890625,
				0.2841796875,
				0.2880859375,
				0.29296875,
				0.298828125,
				0.306640625,
				0.306640625,
				0.3046875,
				0
			]
		},
		{
			"type": "freedraw",
			"version": 48,
			"versionNonce": 1702484696,
			"isDeleted": false,
			"id": "BlCfLaVKvLkH6lOQs1Q0u",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 0,
			"opacity": 100,
			"angle": 0,
			"x": 263.33333333333337,
			"y": -311.0351562499999,
			"strokeColor": "#2b8a3e",
			"backgroundColor": "transparent",
			"width": 13.333333333333371,
			"height": 10.833333333333371,
			"seed": 2029271768,
			"groupIds": [],
			"strokeSharpness": "round",
			"boundElements": [],
			"updated": 1665930257083,
			"link": null,
			"locked": false,
			"points": [
				[
					0,
					0
				],
				[
					-0.8333333333333712,
					0
				],
				[
					-1.6666666666666288,
					0
				],
				[
					-2.5,
					0
				],
				[
					-2.5,
					0.8333333333333712
				],
				[
					-3.3333333333333712,
					0.8333333333333712
				],
				[
					-3.3333333333333712,
					1.6666666666666856
				],
				[
					-4.166666666666629,
					2.5
				],
				[
					-5,
					3.3333333333333712
				],
				[
					-5,
					5
				],
				[
					-5.833333333333371,
					5.833333333333371
				],
				[
					-5.833333333333371,
					8.333333333333371
				],
				[
					-5.833333333333371,
					9.166666666666686
				],
				[
					-5.833333333333371,
					10
				],
				[
					-4.166666666666629,
					10.833333333333371
				],
				[
					-3.3333333333333712,
					10.833333333333371
				],
				[
					-1.6666666666666288,
					10.833333333333371
				],
				[
					1.6666666666666288,
					10.833333333333371
				],
				[
					3.3333333333333712,
					10
				],
				[
					4.166666666666629,
					10
				],
				[
					5,
					9.166666666666686
				],
				[
					6.666666666666629,
					6.666666666666686
				],
				[
					6.666666666666629,
					5.833333333333371
				],
				[
					7.5,
					4.166666666666686
				],
				[
					7.5,
					3.3333333333333712
				],
				[
					7.5,
					2.5
				],
				[
					6.666666666666629,
					1.6666666666666856
				],
				[
					5.833333333333371,
					0.8333333333333712
				],
				[
					5,
					0.8333333333333712
				],
				[
					3.3333333333333712,
					0
				],
				[
					1.6666666666666288,
					0
				],
				[
					0,
					0
				],
				[
					-0.8333333333333712,
					0
				],
				[
					-2.5,
					0
				],
				[
					-3.3333333333333712,
					0.8333333333333712
				],
				[
					-3.3333333333333712,
					2.5
				],
				[
					0,
					0
				]
			],
			"lastCommittedPoint": null,
			"simulatePressure": false,
			"pressures": [
				0.0263671875,
				0.24609375,
				0.3251953125,
				0.328125,
				0.3291015625,
				0.3310546875,
				0.3310546875,
				0.3310546875,
				0.3310546875,
				0.330078125,
				0.330078125,
				0.3291015625,
				0.328125,
				0.3271484375,
				0.3232421875,
				0.3193359375,
				0.3046875,
				0.298828125,
				0.2978515625,
				0.296875,
				0.2939453125,
				0.2900390625,
				0.291015625,
				0.2900390625,
				0.2890625,
				0.2880859375,
				0.2900390625,
				0.291015625,
				0.2919921875,
				0.294921875,
				0.3046875,
				0.3076171875,
				0.3115234375,
				0.3115234375,
				0.2998046875,
				0.1484375,
				0
			]
		},
		{
			"type": "freedraw",
			"version": 23,
			"versionNonce": 734518952,
			"isDeleted": false,
			"id": "GYa0ZzHTLKF7kjAuJhuhw",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 0,
			"opacity": 100,
			"angle": 0,
			"x": 272.5,
			"y": -313.5351562499999,
			"strokeColor": "#2b8a3e",
			"backgroundColor": "transparent",
			"width": 4.166666666666629,
			"height": 12.5,
			"seed": 2049232808,
			"groupIds": [],
			"strokeSharpness": "round",
			"boundElements": [],
			"updated": 1665930257083,
			"link": null,
			"locked": false,
			"points": [
				[
					0,
					0
				],
				[
					0,
					0.8333333333333712
				],
				[
					-0.8333333333332575,
					1.6666666666666856
				],
				[
					-0.8333333333332575,
					3.3333333333333712
				],
				[
					-0.8333333333332575,
					5.833333333333371
				],
				[
					-0.8333333333332575,
					9.166666666666686
				],
				[
					-0.8333333333332575,
					10.833333333333371
				],
				[
					-0.8333333333332575,
					11.666666666666686
				],
				[
					-0.8333333333332575,
					12.5
				],
				[
					0.8333333333333712,
					12.5
				],
				[
					2.5,
					12.5
				],
				[
					3.3333333333333712,
					12.5
				],
				[
					3.3333333333333712,
					12.5
				]
			],
			"lastCommittedPoint": null,
			"simulatePressure": false,
			"pressures": [
				0.0029296875,
				0.1923828125,
				0.236328125,
				0.255859375,
				0.2705078125,
				0.2802734375,
				0.2822265625,
				0.2822265625,
				0.2802734375,
				0.267578125,
				0.2021484375,
				0.1220703125,
				0
			]
		},
		{
			"type": "freedraw",
			"version": 20,
			"versionNonce": 1009303512,
			"isDeleted": false,
			"id": "Jk2_ut-37lboUtO-2TmP9",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 0,
			"opacity": 100,
			"angle": 0,
			"x": 281.66666666666674,
			"y": -310.2018229166665,
			"strokeColor": "#2b8a3e",
			"backgroundColor": "transparent",
			"width": 13.333333333333258,
			"height": 1.6666666666666856,
			"seed": 188906664,
			"groupIds": [],
			"strokeSharpness": "round",
			"boundElements": [],
			"updated": 1665930257083,
			"link": null,
			"locked": false,
			"points": [
				[
					0,
					0
				],
				[
					0.8333333333332575,
					0
				],
				[
					2.5,
					0
				],
				[
					5,
					0
				],
				[
					7.5,
					0
				],
				[
					8.333333333333258,
					0
				],
				[
					10.833333333333258,
					0
				],
				[
					12.5,
					-0.8333333333333712
				],
				[
					13.333333333333258,
					-1.6666666666666856
				],
				[
					13.333333333333258,
					-1.6666666666666856
				]
			],
			"lastCommittedPoint": null,
			"simulatePressure": false,
			"pressures": [
				0.119140625,
				0.25390625,
				0.279296875,
				0.30078125,
				0.3046875,
				0.3046875,
				0.306640625,
				0.298828125,
				0.2158203125,
				0
			]
		},
		{
			"type": "freedraw",
			"version": 29,
			"versionNonce": 128305576,
			"isDeleted": false,
			"id": "lfE9yK5zTQ6Jk53UpPjn2",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 0,
			"opacity": 100,
			"angle": 0,
			"x": 290,
			"y": -323.5351562499999,
			"strokeColor": "#2b8a3e",
			"backgroundColor": "transparent",
			"width": 12.5,
			"height": 28.33333333333337,
			"seed": 1574623960,
			"groupIds": [],
			"strokeSharpness": "round",
			"boundElements": [],
			"updated": 1665930257083,
			"link": null,
			"locked": false,
			"points": [
				[
					0,
					0
				],
				[
					-0.8333333333332575,
					3.3333333333333712
				],
				[
					-1.6666666666666288,
					5
				],
				[
					-2.5,
					10
				],
				[
					-3.3333333333332575,
					12.5
				],
				[
					-3.3333333333332575,
					15.833333333333371
				],
				[
					-4.166666666666629,
					18.33333333333337
				],
				[
					-4.166666666666629,
					22.5
				],
				[
					-4.166666666666629,
					25
				],
				[
					-4.166666666666629,
					26.666666666666686
				],
				[
					-3.3333333333332575,
					27.5
				],
				[
					-1.6666666666666288,
					27.5
				],
				[
					-0.8333333333332575,
					28.33333333333337
				],
				[
					1.6666666666667425,
					28.33333333333337
				],
				[
					3.3333333333333712,
					28.33333333333337
				],
				[
					6.6666666666667425,
					25.83333333333337
				],
				[
					7.5,
					25
				],
				[
					8.333333333333371,
					24.166666666666686
				],
				[
					8.333333333333371,
					24.166666666666686
				]
			],
			"lastCommittedPoint": null,
			"simulatePressure": false,
			"pressures": [
				0.091796875,
				0.1953125,
				0.2041015625,
				0.2314453125,
				0.248046875,
				0.2578125,
				0.2666015625,
				0.2763671875,
				0.2802734375,
				0.2822265625,
				0.2802734375,
				0.28125,
				0.28125,
				0.283203125,
				0.283203125,
				0.2822265625,
				0.2666015625,
				0.2470703125,
				0
			]
		},
		{
			"type": "freedraw",
			"version": 33,
			"versionNonce": 95618264,
			"isDeleted": false,
			"id": "bly6NIn1bcnzj4RPFus3l",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 0,
			"opacity": 100,
			"angle": 0,
			"x": 305.83333333333337,
			"y": -311.0351562499999,
			"strokeColor": "#2b8a3e",
			"backgroundColor": "transparent",
			"width": 8.333333333333371,
			"height": 15.833333333333371,
			"seed": 207596760,
			"groupIds": [],
			"strokeSharpness": "round",
			"boundElements": [],
			"updated": 1665930257083,
			"link": null,
			"locked": false,
			"points": [
				[
					0,
					0
				],
				[
					-0.8333333333333712,
					0
				],
				[
					-0.8333333333333712,
					1.6666666666666856
				],
				[
					-0.8333333333333712,
					5
				],
				[
					-0.8333333333333712,
					7.5
				],
				[
					-0.8333333333333712,
					10
				],
				[
					0,
					12.5
				],
				[
					0,
					14.166666666666686
				],
				[
					0,
					15.833333333333371
				],
				[
					0,
					15
				],
				[
					0,
					14.166666666666686
				],
				[
					0,
					13.333333333333371
				],
				[
					0,
					10.833333333333371
				],
				[
					0,
					9.166666666666686
				],
				[
					0.8333333333333712,
					6.666666666666686
				],
				[
					1.6666666666666288,
					5
				],
				[
					3.3333333333333712,
					4.166666666666686
				],
				[
					4.166666666666629,
					3.3333333333333712
				],
				[
					5,
					2.5
				],
				[
					5.8333333333332575,
					1.6666666666666856
				],
				[
					6.6666666666667425,
					1.6666666666666856
				],
				[
					7.5,
					0.8333333333333712
				],
				[
					7.5,
					0.8333333333333712
				]
			],
			"lastCommittedPoint": null,
			"simulatePressure": false,
			"pressures": [
				0.0888671875,
				0.2578125,
				0.271484375,
				0.2822265625,
				0.291015625,
				0.296875,
				0.30078125,
				0.302734375,
				0.3037109375,
				0.302734375,
				0.302734375,
				0.2998046875,
				0.294921875,
				0.2919921875,
				0.28125,
				0.27734375,
				0.27734375,
				0.2763671875,
				0.2763671875,
				0.2763671875,
				0.26953125,
				0.22265625,
				0
			]
		},
		{
			"type": "freedraw",
			"version": 19,
			"versionNonce": 646861992,
			"isDeleted": false,
			"id": "r88pzXcF4szGyO0D5tyuK",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 0,
			"opacity": 100,
			"angle": 0,
			"x": 320.0000000000001,
			"y": -305.2018229166665,
			"strokeColor": "#2b8a3e",
			"backgroundColor": "transparent",
			"width": 0.8333333333332575,
			"height": 9.166666666666686,
			"seed": 1155934424,
			"groupIds": [],
			"strokeSharpness": "round",
			"boundElements": [],
			"updated": 1665930257083,
			"link": null,
			"locked": false,
			"points": [
				[
					0,
					0
				],
				[
					0.8333333333332575,
					-0.8333333333333712
				],
				[
					0.8333333333332575,
					0.8333333333333144
				],
				[
					0.8333333333332575,
					2.5
				],
				[
					0.8333333333332575,
					5
				],
				[
					0.8333333333332575,
					6.666666666666629
				],
				[
					0.8333333333332575,
					7.5
				],
				[
					0.8333333333332575,
					8.333333333333314
				],
				[
					0.8333333333332575,
					8.333333333333314
				]
			],
			"lastCommittedPoint": null,
			"simulatePressure": false,
			"pressures": [
				0.0859375,
				0.2119140625,
				0.2880859375,
				0.3095703125,
				0.314453125,
				0.31640625,
				0.3173828125,
				0.3154296875,
				0
			]
		},
		{
			"type": "freedraw",
			"version": 15,
			"versionNonce": 262973912,
			"isDeleted": false,
			"id": "c_vysvZ6gqzPdAuxKFywF",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 0,
			"opacity": 100,
			"angle": 0,
			"x": 322.5000000000001,
			"y": -313.5351562499999,
			"strokeColor": "#2b8a3e",
			"backgroundColor": "transparent",
			"width": 0,
			"height": 1.6666666666666856,
			"seed": 985244120,
			"groupIds": [],
			"strokeSharpness": "round",
			"boundElements": [],
			"updated": 1665930257083,
			"link": null,
			"locked": false,
			"points": [
				[
					0,
					0
				],
				[
					0,
					0.8333333333333712
				],
				[
					0,
					1.6666666666666856
				],
				[
					0,
					0
				]
			],
			"lastCommittedPoint": null,
			"simulatePressure": false,
			"pressures": [
				0.1904296875,
				0.2939453125,
				0.0126953125,
				0
			]
		},
		{
			"type": "freedraw",
			"version": 15,
			"versionNonce": 237862824,
			"isDeleted": false,
			"id": "LizAJazzNq4BzFAx0KgO1",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 0,
			"opacity": 100,
			"angle": 0,
			"x": 330.0000000000001,
			"y": -306.0351562499999,
			"strokeColor": "#2b8a3e",
			"backgroundColor": "transparent",
			"width": 14.166666666666742,
			"height": 10.833333333333371,
			"seed": 1185903272,
			"groupIds": [],
			"strokeSharpness": "round",
			"boundElements": [],
			"updated": 1665930257083,
			"link": null,
			"locked": false,
			"points": [
				[
					0,
					0
				],
				[
					-0.8333333333334849,
					0
				],
				[
					0,
					0
				],
				[
					2.5,
					2.5
				],
				[
					3.3333333333332575,
					3.3333333333333712
				],
				[
					5.8333333333332575,
					5.833333333333371
				],
				[
					8.333333333333258,
					7.5
				],
				[
					11.666666666666515,
					9.166666666666686
				],
				[
					13.333333333333258,
					10
				],
				[
					13.333333333333258,
					10.833333333333371
				],
				[
					12.5,
					10.833333333333371
				],
				[
					12.5,
					10.833333333333371
				]
			],
			"lastCommittedPoint": null,
			"simulatePressure": false,
			"pressures": [
				0.1083984375,
				0.2685546875,
				0.3916015625,
				0.4248046875,
				0.4326171875,
				0.443359375,
				0.4501953125,
				0.455078125,
				0.4560546875,
				0.365234375,
				0,
				0
			]
		},
		{
			"type": "freedraw",
			"version": 14,
			"versionNonce": 1428795096,
			"isDeleted": false,
			"id": "E4yq4qgKcHgALURrp88gj",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 0,
			"opacity": 100,
			"angle": 0,
			"x": 342.5000000000001,
			"y": -308.5351562499999,
			"strokeColor": "#2b8a3e",
			"backgroundColor": "transparent",
			"width": 13.333333333333485,
			"height": 13.333333333333371,
			"seed": 1381476824,
			"groupIds": [],
			"strokeSharpness": "round",
			"boundElements": [],
			"updated": 1665930257083,
			"link": null,
			"locked": false,
			"points": [
				[
					0,
					0
				],
				[
					0,
					0.8333333333333712
				],
				[
					-1.6666666666667425,
					2.5
				],
				[
					-5,
					5
				],
				[
					-7.5,
					7.5
				],
				[
					-9.166666666666742,
					10
				],
				[
					-10.833333333333485,
					11.666666666666686
				],
				[
					-12.5,
					12.5
				],
				[
					-12.5,
					13.333333333333371
				],
				[
					-13.333333333333485,
					13.333333333333371
				],
				[
					-13.333333333333485,
					13.333333333333371
				]
			],
			"lastCommittedPoint": null,
			"simulatePressure": false,
			"pressures": [
				0.10546875,
				0.3564453125,
				0.3662109375,
				0.384765625,
				0.392578125,
				0.3955078125,
				0.3955078125,
				0.3955078125,
				0.2998046875,
				0.1640625,
				0
			]
		},
		{
			"type": "freedraw",
			"version": 12,
			"versionNonce": 783239848,
			"isDeleted": false,
			"id": "ZtHIi6JtWNQigsXexQVw2",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 0,
			"opacity": 100,
			"angle": 0,
			"x": 361.66666666666663,
			"y": -296.0351562499999,
			"strokeColor": "#2b8a3e",
			"backgroundColor": "transparent",
			"width": 7.5,
			"height": 9.166666666666686,
			"seed": 1484023768,
			"groupIds": [],
			"strokeSharpness": "round",
			"boundElements": [],
			"updated": 1665930257083,
			"link": null,
			"locked": false,
			"points": [
				[
					0,
					0
				],
				[
					0,
					0.8333333333333712
				],
				[
					0,
					2.5
				],
				[
					0,
					3.3333333333333712
				],
				[
					-0.8333333333332575,
					5
				],
				[
					-3.3333333333332575,
					6.666666666666686
				],
				[
					-5.8333333333332575,
					8.333333333333371
				],
				[
					-7.5,
					9.166666666666686
				],
				[
					-7.5,
					9.166666666666686
				]
			],
			"lastCommittedPoint": null,
			"simulatePressure": false,
			"pressures": [
				0.0615234375,
				0.310546875,
				0.3310546875,
				0.3505859375,
				0.36328125,
				0.365234375,
				0.349609375,
				0.2451171875,
				0
			]
		},
		{
			"type": "freedraw",
			"version": 63,
			"versionNonce": 1321079768,
			"isDeleted": false,
			"id": "fQ2eM6jTLPU7kxm1HMDfM",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 0,
			"opacity": 100,
			"angle": 0,
			"x": 380.83333333333337,
			"y": -307.7018229166665,
			"strokeColor": "#2b8a3e",
			"backgroundColor": "transparent",
			"width": 26.666666666666742,
			"height": 17.5,
			"seed": 1921024216,
			"groupIds": [],
			"strokeSharpness": "round",
			"boundElements": [],
			"updated": 1665930257084,
			"link": null,
			"locked": false,
			"points": [
				[
					0,
					0
				],
				[
					0,
					-1.6666666666666856
				],
				[
					0,
					-0.8333333333333712
				],
				[
					0,
					2.5
				],
				[
					0,
					5
				],
				[
					0,
					7.5
				],
				[
					-0.8333333333332575,
					10
				],
				[
					-1.6666666666667425,
					12.5
				],
				[
					-1.6666666666667425,
					13.333333333333314
				],
				[
					-1.6666666666667425,
					12.5
				],
				[
					-0.8333333333332575,
					11.666666666666629
				],
				[
					0,
					10
				],
				[
					0.8333333333332575,
					8.333333333333314
				],
				[
					1.6666666666667425,
					6.666666666666629
				],
				[
					2.5,
					5.833333333333314
				],
				[
					2.5,
					5
				],
				[
					3.3333333333332575,
					4.166666666666629
				],
				[
					5,
					3.3333333333333144
				],
				[
					5.8333333333332575,
					3.3333333333333144
				],
				[
					6.6666666666667425,
					2.5
				],
				[
					7.5,
					1.6666666666666288
				],
				[
					8.333333333333258,
					1.6666666666666288
				],
				[
					9.166666666666742,
					1.6666666666666288
				],
				[
					10,
					1.6666666666666288
				],
				[
					11.666666666666742,
					1.6666666666666288
				],
				[
					12.5,
					1.6666666666666288
				],
				[
					12.5,
					2.5
				],
				[
					13.333333333333258,
					2.5
				],
				[
					13.333333333333258,
					4.166666666666629
				],
				[
					13.333333333333258,
					5.833333333333314
				],
				[
					13.333333333333258,
					7.5
				],
				[
					13.333333333333258,
					8.333333333333314
				],
				[
					13.333333333333258,
					9.166666666666629
				],
				[
					12.5,
					10
				],
				[
					12.5,
					9.166666666666629
				],
				[
					13.333333333333258,
					7.5
				],
				[
					13.333333333333258,
					6.666666666666629
				],
				[
					14.166666666666742,
					5
				],
				[
					15,
					3.3333333333333144
				],
				[
					15.833333333333258,
					3.3333333333333144
				],
				[
					16.666666666666742,
					2.5
				],
				[
					18.333333333333258,
					1.6666666666666288
				],
				[
					19.166666666666742,
					1.6666666666666288
				],
				[
					19.166666666666742,
					0.8333333333333144
				],
				[
					20.833333333333258,
					0.8333333333333144
				],
				[
					21.666666666666742,
					0.8333333333333144
				],
				[
					22.5,
					0.8333333333333144
				],
				[
					23.333333333333258,
					0.8333333333333144
				],
				[
					24.166666666666742,
					0.8333333333333144
				],
				[
					25,
					1.6666666666666288
				],
				[
					25,
					2.5
				],
				[
					25,
					4.166666666666629
				],
				[
					25,
					5.833333333333314
				],
				[
					25,
					7.5
				],
				[
					25,
					10
				],
				[
					23.333333333333258,
					13.333333333333314
				],
				[
					22.5,
					14.166666666666629
				],
				[
					22.5,
					15
				],
				[
					21.666666666666742,
					15.833333333333314
				],
				[
					21.666666666666742,
					15.833333333333314
				]
			],
			"lastCommittedPoint": null,
			"simulatePressure": false,
			"pressures": [
				0.0078125,
				0.2431640625,
				0.38671875,
				0.404296875,
				0.41015625,
				0.4130859375,
				0.4150390625,
				0.4150390625,
				0.4150390625,
				0.390625,
				0.38671875,
				0.37890625,
				0.3740234375,
				0.3671875,
				0.3662109375,
				0.3662109375,
				0.365234375,
				0.3642578125,
				0.361328125,
				0.3603515625,
				0.3603515625,
				0.3603515625,
				0.3583984375,
				0.357421875,
				0.3583984375,
				0.359375,
				0.3583984375,
				0.359375,
				0.3671875,
				0.3720703125,
				0.3740234375,
				0.375,
				0.375,
				0.376953125,
				0.330078125,
				0.3271484375,
				0.3251953125,
				0.3232421875,
				0.3203125,
				0.318359375,
				0.318359375,
				0.3193359375,
				0.3203125,
				0.3203125,
				0.3193359375,
				0.3193359375,
				0.3203125,
				0.32421875,
				0.328125,
				0.330078125,
				0.3310546875,
				0.3427734375,
				0.3505859375,
				0.35546875,
				0.3583984375,
				0.3642578125,
				0.3642578125,
				0.3623046875,
				0.3232421875,
				0
			]
		},
		{
			"type": "freedraw",
			"version": 68,
			"versionNonce": 314553768,
			"isDeleted": false,
			"id": "yhjqBO3RW1t5wvxJMNB6c",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 0,
			"opacity": 100,
			"angle": 0,
			"x": -472.4999999999999,
			"y": -321.8684895833332,
			"strokeColor": "#e67700",
			"backgroundColor": "transparent",
			"width": 95.83333333333331,
			"height": 107.5,
			"seed": 988054184,
			"groupIds": [],
			"strokeSharpness": "round",
			"boundElements": [],
			"updated": 1665930257084,
			"link": null,
			"locked": false,
			"points": [
				[
					0,
					0
				],
				[
					-0.8333333333333712,
					-0.8333333333333144
				],
				[
					1.6666666666666288,
					-1.6666666666666856
				],
				[
					3.3333333333333144,
					-2.5
				],
				[
					5.833333333333314,
					-3.3333333333333144
				],
				[
					10.833333333333314,
					-4.166666666666686
				],
				[
					14.166666666666629,
					-5
				],
				[
					17.5,
					-5
				],
				[
					20.833333333333314,
					-5
				],
				[
					23.333333333333314,
					-4.166666666666686
				],
				[
					29.16666666666663,
					-1.6666666666666856
				],
				[
					31.66666666666663,
					0
				],
				[
					35,
					0.8333333333333144
				],
				[
					36.66666666666663,
					1.6666666666666856
				],
				[
					40,
					5
				],
				[
					41.66666666666663,
					6.666666666666686
				],
				[
					43.333333333333314,
					9.166666666666686
				],
				[
					45,
					10.833333333333314
				],
				[
					46.66666666666663,
					13.333333333333314
				],
				[
					47.5,
					15.833333333333314
				],
				[
					49.16666666666663,
					20
				],
				[
					50.833333333333314,
					22.5
				],
				[
					50.833333333333314,
					25
				],
				[
					51.66666666666663,
					29.166666666666686
				],
				[
					52.5,
					32.5
				],
				[
					52.5,
					35.833333333333314
				],
				[
					52.5,
					39.166666666666686
				],
				[
					53.333333333333314,
					42.5
				],
				[
					53.333333333333314,
					45.833333333333314
				],
				[
					53.333333333333314,
					50
				],
				[
					53.333333333333314,
					52.5
				],
				[
					53.333333333333314,
					55
				],
				[
					52.5,
					58.333333333333314
				],
				[
					51.66666666666663,
					60
				],
				[
					51.66666666666663,
					62.5
				],
				[
					50.833333333333314,
					64.16666666666669
				],
				[
					50.833333333333314,
					66.66666666666666
				],
				[
					50,
					70.83333333333334
				],
				[
					50,
					74.16666666666666
				],
				[
					50,
					77.5
				],
				[
					50,
					80.83333333333334
				],
				[
					50.833333333333314,
					85.83333333333334
				],
				[
					51.66666666666663,
					87.5
				],
				[
					53.333333333333314,
					90
				],
				[
					54.16666666666663,
					92.5
				],
				[
					56.66666666666663,
					95
				],
				[
					58.333333333333314,
					96.66666666666666
				],
				[
					61.66666666666663,
					98.33333333333334
				],
				[
					64.16666666666663,
					99.16666666666666
				],
				[
					66.66666666666663,
					100.83333333333334
				],
				[
					70.83333333333331,
					101.66666666666666
				],
				[
					73.33333333333331,
					102.5
				],
				[
					75,
					102.5
				],
				[
					80,
					101.66666666666666
				],
				[
					81.66666666666663,
					100.83333333333334
				],
				[
					82.49999999999994,
					100.83333333333334
				],
				[
					84.99999999999994,
					100
				],
				[
					86.66666666666663,
					99.16666666666666
				],
				[
					87.49999999999994,
					99.16666666666666
				],
				[
					89.99999999999994,
					99.16666666666666
				],
				[
					91.66666666666663,
					99.16666666666666
				],
				[
					92.49999999999994,
					99.16666666666666
				],
				[
					94.16666666666663,
					98.33333333333334
				],
				[
					94.99999999999994,
					98.33333333333334
				],
				[
					94.99999999999994,
					98.33333333333334
				]
			],
			"lastCommittedPoint": null,
			"simulatePressure": false,
			"pressures": [
				0.0478515625,
				0.197265625,
				0.23828125,
				0.2451171875,
				0.251953125,
				0.2578125,
				0.2626953125,
				0.267578125,
				0.26953125,
				0.271484375,
				0.2744140625,
				0.2763671875,
				0.2763671875,
				0.27734375,
				0.279296875,
				0.2802734375,
				0.28125,
				0.28125,
				0.2822265625,
				0.283203125,
				0.2841796875,
				0.2841796875,
				0.2861328125,
				0.291015625,
				0.29296875,
				0.2939453125,
				0.2939453125,
				0.2939453125,
				0.2958984375,
				0.2958984375,
				0.2958984375,
				0.2958984375,
				0.296875,
				0.296875,
				0.296875,
				0.2958984375,
				0.2958984375,
				0.2958984375,
				0.2958984375,
				0.2958984375,
				0.2958984375,
				0.2939453125,
				0.2890625,
				0.2783203125,
				0.2763671875,
				0.2763671875,
				0.279296875,
				0.2802734375,
				0.2822265625,
				0.287109375,
				0.2958984375,
				0.3046875,
				0.3076171875,
				0.3095703125,
				0.3115234375,
				0.3134765625,
				0.3134765625,
				0.3134765625,
				0.3134765625,
				0.3154296875,
				0.318359375,
				0.3203125,
				0.3212890625,
				0.3212890625,
				0
			]
		},
		{
			"type": "freedraw",
			"version": 21,
			"versionNonce": 1115543768,
			"isDeleted": false,
			"id": "H-MmQQr_pkvmO4gkplz_1",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 0,
			"opacity": 100,
			"angle": 0,
			"x": -386.6666666666666,
			"y": -231.8684895833332,
			"strokeColor": "#e67700",
			"backgroundColor": "transparent",
			"width": 12.5,
			"height": 15,
			"seed": 358960552,
			"groupIds": [],
			"strokeSharpness": "round",
			"boundElements": [],
			"updated": 1665930257084,
			"link": null,
			"locked": false,
			"points": [
				[
					0,
					0
				],
				[
					1.6666666666666288,
					0
				],
				[
					4.166666666666629,
					0.8333333333333428
				],
				[
					5.833333333333314,
					0.8333333333333428
				],
				[
					7.5,
					0.8333333333333428
				],
				[
					9.166666666666629,
					0.8333333333333428
				],
				[
					10.833333333333314,
					1.6666666666666572
				],
				[
					11.666666666666629,
					2.5
				],
				[
					12.5,
					2.5
				],
				[
					12.5,
					4.166666666666657
				],
				[
					12.5,
					5
				],
				[
					12.5,
					6.666666666666657
				],
				[
					11.666666666666629,
					8.333333333333343
				],
				[
					10,
					10
				],
				[
					8.333333333333314,
					11.666666666666657
				],
				[
					5,
					14.166666666666657
				],
				[
					4.166666666666629,
					15
				],
				[
					4.166666666666629,
					15
				]
			],
			"lastCommittedPoint": null,
			"simulatePressure": false,
			"pressures": [
				0.1572265625,
				0.263671875,
				0.279296875,
				0.283203125,
				0.287109375,
				0.291015625,
				0.2939453125,
				0.2978515625,
				0.3017578125,
				0.3232421875,
				0.33203125,
				0.337890625,
				0.33984375,
				0.3408203125,
				0.3408203125,
				0.29296875,
				0.2119140625,
				0
			]
		},
		{
			"type": "freedraw",
			"version": 36,
			"versionNonce": 260751528,
			"isDeleted": false,
			"id": "1weA-wfuf4KufA4cqCXab",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 0,
			"opacity": 100,
			"angle": 0,
			"x": -595,
			"y": -351.8684895833332,
			"strokeColor": "#e67700",
			"backgroundColor": "transparent",
			"width": 22.5,
			"height": 35,
			"seed": 1572507096,
			"groupIds": [],
			"strokeSharpness": "round",
			"boundElements": [],
			"updated": 1665930257084,
			"link": null,
			"locked": false,
			"points": [
				[
					0,
					0
				],
				[
					0,
					0.8333333333333144
				],
				[
					0.8333333333333712,
					3.3333333333333144
				],
				[
					0.8333333333333712,
					6.666666666666686
				],
				[
					1.6666666666667425,
					12.5
				],
				[
					1.6666666666667425,
					17.5
				],
				[
					1.6666666666667425,
					21.666666666666686
				],
				[
					1.6666666666667425,
					25
				],
				[
					1.6666666666667425,
					28.333333333333314
				],
				[
					1.6666666666667425,
					30.833333333333314
				],
				[
					1.6666666666667425,
					25.833333333333314
				],
				[
					1.6666666666667425,
					21.666666666666686
				],
				[
					1.6666666666667425,
					19.166666666666686
				],
				[
					1.6666666666667425,
					15.833333333333314
				],
				[
					1.6666666666667425,
					12.5
				],
				[
					1.6666666666667425,
					8.333333333333314
				],
				[
					1.6666666666667425,
					5.833333333333314
				],
				[
					0.8333333333333712,
					3.3333333333333144
				],
				[
					0.8333333333333712,
					0.8333333333333144
				],
				[
					0.8333333333333712,
					-0.8333333333333144
				],
				[
					0.8333333333333712,
					-1.6666666666666856
				],
				[
					2.5,
					-2.5
				],
				[
					5,
					-3.3333333333333144
				],
				[
					7.5,
					-3.3333333333333144
				],
				[
					9.166666666666742,
					-4.166666666666686
				],
				[
					11.666666666666742,
					-4.166666666666686
				],
				[
					14.166666666666742,
					-4.166666666666686
				],
				[
					15.833333333333371,
					-4.166666666666686
				],
				[
					17.5,
					-3.3333333333333144
				],
				[
					19.166666666666742,
					-3.3333333333333144
				],
				[
					20.83333333333337,
					-3.3333333333333144
				],
				[
					22.5,
					-3.3333333333333144
				],
				[
					22.5,
					-3.3333333333333144
				]
			],
			"lastCommittedPoint": null,
			"simulatePressure": false,
			"pressures": [
				0.142578125,
				0.3310546875,
				0.3466796875,
				0.3525390625,
				0.3564453125,
				0.357421875,
				0.3564453125,
				0.353515625,
				0.34765625,
				0.3271484375,
				0.275390625,
				0.271484375,
				0.26953125,
				0.26953125,
				0.26953125,
				0.2646484375,
				0.263671875,
				0.2646484375,
				0.2646484375,
				0.2646484375,
				0.2705078125,
				0.28125,
				0.2900390625,
				0.294921875,
				0.2978515625,
				0.3076171875,
				0.328125,
				0.3349609375,
				0.33984375,
				0.34375,
				0.3447265625,
				0.3447265625,
				0
			]
		},
		{
			"type": "freedraw",
			"version": 11,
			"versionNonce": 335347160,
			"isDeleted": false,
			"id": "o9DvtLFXCWzC4m6dwxIzB",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 0,
			"opacity": 100,
			"angle": 0,
			"x": -591.6666666666666,
			"y": -335.2018229166665,
			"strokeColor": "#e67700",
			"backgroundColor": "transparent",
			"width": 9.166666666666629,
			"height": 0,
			"seed": 1508795096,
			"groupIds": [],
			"strokeSharpness": "round",
			"boundElements": [],
			"updated": 1665930257084,
			"link": null,
			"locked": false,
			"points": [
				[
					0,
					0
				],
				[
					1.6666666666666288,
					0
				],
				[
					3.3333333333333712,
					0
				],
				[
					5.833333333333371,
					0
				],
				[
					6.666666666666629,
					0
				],
				[
					8.333333333333371,
					0
				],
				[
					9.166666666666629,
					0
				],
				[
					9.166666666666629,
					0
				]
			],
			"lastCommittedPoint": null,
			"simulatePressure": false,
			"pressures": [
				0.0537109375,
				0.2578125,
				0.275390625,
				0.287109375,
				0.2919921875,
				0.29296875,
				0.29296875,
				0
			]
		},
		{
			"type": "freedraw",
			"version": 9,
			"versionNonce": 1958505384,
			"isDeleted": false,
			"id": "GJdCTdsROPQv4yhZccR5s",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 0,
			"opacity": 100,
			"angle": 0,
			"x": -576.6666666666666,
			"y": -326.8684895833332,
			"strokeColor": "#e67700",
			"backgroundColor": "transparent",
			"width": 0,
			"height": 7.5,
			"seed": 2011906728,
			"groupIds": [],
			"strokeSharpness": "round",
			"boundElements": [],
			"updated": 1665930257084,
			"link": null,
			"locked": false,
			"points": [
				[
					0,
					0
				],
				[
					0,
					2.5
				],
				[
					0,
					4.166666666666686
				],
				[
					0,
					5.833333333333314
				],
				[
					0,
					7.5
				],
				[
					0,
					7.5
				]
			],
			"lastCommittedPoint": null,
			"simulatePressure": false,
			"pressures": [
				0.078125,
				0.2705078125,
				0.283203125,
				0.287109375,
				0.2900390625,
				0
			]
		},
		{
			"type": "freedraw",
			"version": 8,
			"versionNonce": 340755160,
			"isDeleted": false,
			"id": "jWgD0GPAR12OpIoWTjrQu",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 0,
			"opacity": 100,
			"angle": 0,
			"x": -578.3333333333333,
			"y": -336.0351562499999,
			"strokeColor": "#e67700",
			"backgroundColor": "transparent",
			"width": 0,
			"height": 0.8333333333333144,
			"seed": 1706822360,
			"groupIds": [],
			"strokeSharpness": "round",
			"boundElements": [],
			"updated": 1665930257084,
			"link": null,
			"locked": false,
			"points": [
				[
					0,
					0
				],
				[
					0,
					-0.8333333333333144
				],
				[
					0,
					0
				],
				[
					0,
					0
				]
			],
			"lastCommittedPoint": null,
			"simulatePressure": false,
			"pressures": [
				0.1943359375,
				0.330078125,
				0.248046875,
				0
			]
		},
		{
			"type": "freedraw",
			"version": 39,
			"versionNonce": 1015068328,
			"isDeleted": false,
			"id": "OWjC_xQ7lC172CEoM65FM",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 0,
			"opacity": 100,
			"angle": 0,
			"x": -567.5,
			"y": -346.8684895833332,
			"strokeColor": "#e67700",
			"backgroundColor": "transparent",
			"width": 12.5,
			"height": 30.833333333333314,
			"seed": 2061294040,
			"groupIds": [],
			"strokeSharpness": "round",
			"boundElements": [],
			"updated": 1665930257084,
			"link": null,
			"locked": false,
			"points": [
				[
					0,
					0
				],
				[
					0.8333333333333712,
					-0.8333333333333144
				],
				[
					0.8333333333333712,
					0
				],
				[
					0.8333333333333712,
					2.5
				],
				[
					0.8333333333333712,
					6.666666666666686
				],
				[
					0.8333333333333712,
					12.5
				],
				[
					0.8333333333333712,
					17.5
				],
				[
					1.6666666666667425,
					20.833333333333314
				],
				[
					1.6666666666667425,
					24.166666666666686
				],
				[
					1.6666666666667425,
					25.833333333333314
				],
				[
					1.6666666666667425,
					26.666666666666686
				],
				[
					0.8333333333333712,
					26.666666666666686
				],
				[
					0.8333333333333712,
					25.833333333333314
				],
				[
					0.8333333333333712,
					24.166666666666686
				],
				[
					0.8333333333333712,
					22.5
				],
				[
					0.8333333333333712,
					20.833333333333314
				],
				[
					0.8333333333333712,
					19.166666666666686
				],
				[
					0.8333333333333712,
					18.333333333333314
				],
				[
					2.5,
					17.5
				],
				[
					4.1666666666667425,
					17.5
				],
				[
					6.6666666666667425,
					17.5
				],
				[
					7.5,
					18.333333333333314
				],
				[
					9.166666666666742,
					19.166666666666686
				],
				[
					10,
					20.833333333333314
				],
				[
					10.833333333333371,
					21.666666666666686
				],
				[
					11.666666666666742,
					23.333333333333314
				],
				[
					12.5,
					24.166666666666686
				],
				[
					12.5,
					25
				],
				[
					12.5,
					25.833333333333314
				],
				[
					10.833333333333371,
					26.666666666666686
				],
				[
					9.166666666666742,
					27.5
				],
				[
					7.5,
					27.5
				],
				[
					5,
					29.166666666666686
				],
				[
					3.3333333333333712,
					30
				],
				[
					1.6666666666667425,
					30
				],
				[
					1.6666666666667425,
					30
				]
			],
			"lastCommittedPoint": null,
			"simulatePressure": false,
			"pressures": [
				0.087890625,
				0.2119140625,
				0.26171875,
				0.2822265625,
				0.30078125,
				0.314453125,
				0.31640625,
				0.31640625,
				0.3154296875,
				0.3134765625,
				0.3125,
				0.27734375,
				0.2607421875,
				0.259765625,
				0.2587890625,
				0.259765625,
				0.2607421875,
				0.2607421875,
				0.2607421875,
				0.2607421875,
				0.2666015625,
				0.2783203125,
				0.2861328125,
				0.2890625,
				0.2900390625,
				0.291015625,
				0.291015625,
				0.29296875,
				0.2978515625,
				0.314453125,
				0.3203125,
				0.33203125,
				0.3408203125,
				0.341796875,
				0.259765625,
				0
			]
		},
		{
			"type": "freedraw",
			"version": 18,
			"versionNonce": 1376348120,
			"isDeleted": false,
			"id": "fo2HxqVuHfSL7lLYCPBVl",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 0,
			"opacity": 100,
			"angle": 0,
			"x": -550.8333333333333,
			"y": -330.2018229166665,
			"strokeColor": "#e67700",
			"backgroundColor": "transparent",
			"width": 7.5,
			"height": 13.333333333333314,
			"seed": 954348968,
			"groupIds": [],
			"strokeSharpness": "round",
			"boundElements": [],
			"updated": 1665930257084,
			"link": null,
			"locked": false,
			"points": [
				[
					0,
					0
				],
				[
					0.8333333333332575,
					2.5
				],
				[
					1.6666666666666288,
					5.833333333333314
				],
				[
					2.5,
					9.166666666666629
				],
				[
					3.3333333333332575,
					12.5
				],
				[
					3.3333333333332575,
					13.333333333333314
				],
				[
					2.5,
					13.333333333333314
				],
				[
					1.6666666666666288,
					11.666666666666629
				],
				[
					1.6666666666666288,
					8.333333333333314
				],
				[
					1.6666666666666288,
					5.833333333333314
				],
				[
					2.5,
					2.5
				],
				[
					4.166666666666629,
					0.8333333333333144
				],
				[
					5.8333333333332575,
					0.8333333333333144
				],
				[
					7.5,
					0.8333333333333144
				],
				[
					7.5,
					0.8333333333333144
				]
			],
			"lastCommittedPoint": null,
			"simulatePressure": false,
			"pressures": [
				0.1298828125,
				0.306640625,
				0.310546875,
				0.310546875,
				0.3115234375,
				0.3125,
				0.3115234375,
				0.3095703125,
				0.3076171875,
				0.306640625,
				0.3037109375,
				0.3017578125,
				0.2978515625,
				0.2578125,
				0
			]
		},
		{
			"type": "freedraw",
			"version": 24,
			"versionNonce": 998482344,
			"isDeleted": false,
			"id": "suszAvSWxqV9dKSFN55KM",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 0,
			"opacity": 100,
			"angle": 0,
			"x": -538.3333333333333,
			"y": -325.2018229166665,
			"strokeColor": "#e67700",
			"backgroundColor": "transparent",
			"width": 10.833333333333258,
			"height": 16.66666666666663,
			"seed": 1486097832,
			"groupIds": [],
			"strokeSharpness": "round",
			"boundElements": [],
			"updated": 1665930257084,
			"link": null,
			"locked": false,
			"points": [
				[
					0,
					0
				],
				[
					4.166666666666629,
					-0.8333333333333712
				],
				[
					5,
					-2.5
				],
				[
					5.8333333333332575,
					-4.166666666666686
				],
				[
					5.8333333333332575,
					-5
				],
				[
					5.8333333333332575,
					-6.666666666666686
				],
				[
					5.8333333333332575,
					-7.5
				],
				[
					5,
					-7.5
				],
				[
					3.3333333333332575,
					-7.5
				],
				[
					0.8333333333332575,
					-6.666666666666686
				],
				[
					-1.6666666666667425,
					-4.166666666666686
				],
				[
					-2.5,
					-1.6666666666666856
				],
				[
					-2.5,
					0.8333333333333144
				],
				[
					-2.5,
					4.166666666666629
				],
				[
					-2.5,
					5.833333333333314
				],
				[
					-1.6666666666667425,
					7.5
				],
				[
					0.8333333333332575,
					9.166666666666629
				],
				[
					3.3333333333332575,
					9.166666666666629
				],
				[
					6.666666666666629,
					7.5
				],
				[
					8.333333333333258,
					5.833333333333314
				],
				[
					8.333333333333258,
					5.833333333333314
				]
			],
			"lastCommittedPoint": null,
			"simulatePressure": false,
			"pressures": [
				0.0068359375,
				0.2236328125,
				0.2421875,
				0.2490234375,
				0.25390625,
				0.2802734375,
				0.291015625,
				0.298828125,
				0.302734375,
				0.306640625,
				0.3076171875,
				0.3095703125,
				0.3115234375,
				0.3154296875,
				0.3193359375,
				0.3203125,
				0.3203125,
				0.3203125,
				0.3154296875,
				0.21484375,
				0
			]
		},
		{
			"type": "freedraw",
			"version": 10,
			"versionNonce": 1765715160,
			"isDeleted": false,
			"id": "CJ9XFTo75kTVthlklxwdw",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 0,
			"opacity": 100,
			"angle": 0,
			"x": -521.6666666666666,
			"y": -315.2018229166665,
			"strokeColor": "#e67700",
			"backgroundColor": "transparent",
			"width": 5,
			"height": 8.333333333333314,
			"seed": 1394402984,
			"groupIds": [],
			"strokeSharpness": "round",
			"boundElements": [],
			"updated": 1665930257084,
			"link": null,
			"locked": false,
			"points": [
				[
					0,
					0
				],
				[
					0.8333333333333712,
					2.5
				],
				[
					0.8333333333333712,
					5
				],
				[
					-1.6666666666666288,
					7.5
				],
				[
					-3.3333333333333712,
					8.333333333333314
				],
				[
					-4.166666666666629,
					8.333333333333314
				],
				[
					-4.166666666666629,
					8.333333333333314
				]
			],
			"lastCommittedPoint": null,
			"simulatePressure": false,
			"pressures": [
				0.150390625,
				0.3095703125,
				0.3310546875,
				0.3291015625,
				0.2841796875,
				0.19921875,
				0
			]
		},
		{
			"type": "freedraw",
			"version": 29,
			"versionNonce": 1357982888,
			"isDeleted": false,
			"id": "5W1mZEdLSyxsL1eNbXIWW",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 0,
			"opacity": 100,
			"angle": 0,
			"x": -480,
			"y": -351.0351562499999,
			"strokeColor": "#e67700",
			"backgroundColor": "transparent",
			"width": 23.333333333333314,
			"height": 36.666666666666686,
			"seed": 524309160,
			"groupIds": [],
			"strokeSharpness": "round",
			"boundElements": [],
			"updated": 1665930257084,
			"link": null,
			"locked": false,
			"points": [
				[
					0,
					0
				],
				[
					-1.6666666666666288,
					-0.8333333333333144
				],
				[
					-3.3333333333332575,
					-0.8333333333333144
				],
				[
					-4.166666666666629,
					-0.8333333333333144
				],
				[
					-5,
					-0.8333333333333144
				],
				[
					-7.5,
					1.6666666666666856
				],
				[
					-9.166666666666629,
					4.166666666666686
				],
				[
					-10,
					6.666666666666686
				],
				[
					-10,
					10
				],
				[
					-10.833333333333258,
					12.5
				],
				[
					-10.833333333333258,
					15.833333333333371
				],
				[
					-10.833333333333258,
					20
				],
				[
					-10.833333333333258,
					22.5
				],
				[
					-10.833333333333258,
					25
				],
				[
					-11.666666666666629,
					29.166666666666686
				],
				[
					-12.5,
					30.83333333333337
				],
				[
					-13.333333333333258,
					32.5
				],
				[
					-14.999999999999943,
					34.166666666666686
				],
				[
					-15.833333333333314,
					35
				],
				[
					-16.66666666666663,
					35
				],
				[
					-18.333333333333314,
					35.83333333333337
				],
				[
					-19.16666666666663,
					35.83333333333337
				],
				[
					-20.833333333333314,
					35.83333333333337
				],
				[
					-22.499999999999943,
					35.83333333333337
				],
				[
					-23.333333333333314,
					35
				],
				[
					-23.333333333333314,
					35
				]
			],
			"lastCommittedPoint": null,
			"simulatePressure": false,
			"pressures": [
				0.064453125,
				0.236328125,
				0.267578125,
				0.2861328125,
				0.296875,
				0.30078125,
				0.2998046875,
				0.298828125,
				0.2978515625,
				0.296875,
				0.296875,
				0.296875,
				0.2958984375,
				0.2958984375,
				0.306640625,
				0.3134765625,
				0.31640625,
				0.3212890625,
				0.3232421875,
				0.3232421875,
				0.3251953125,
				0.3251953125,
				0.32421875,
				0.2705078125,
				0.1962890625,
				0
			]
		},
		{
			"type": "freedraw",
			"version": 12,
			"versionNonce": 1401115096,
			"isDeleted": false,
			"id": "Kj2vohm6jRgg4fpeoU-ak",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 0,
			"opacity": 100,
			"angle": 0,
			"x": -498.3333333333333,
			"y": -330.2018229166665,
			"strokeColor": "#e67700",
			"backgroundColor": "transparent",
			"width": 10.833333333333371,
			"height": 0.8333333333333144,
			"seed": 2086849752,
			"groupIds": [],
			"strokeSharpness": "round",
			"boundElements": [],
			"updated": 1665930257084,
			"link": null,
			"locked": false,
			"points": [
				[
					0,
					0
				],
				[
					-0.8333333333333144,
					0
				],
				[
					0.8333333333333712,
					0
				],
				[
					3.3333333333333712,
					0
				],
				[
					5.833333333333314,
					0
				],
				[
					9.166666666666686,
					0
				],
				[
					10.000000000000057,
					0
				],
				[
					10.000000000000057,
					0.8333333333333144
				],
				[
					10.000000000000057,
					0.8333333333333144
				]
			],
			"lastCommittedPoint": null,
			"simulatePressure": false,
			"pressures": [
				0.0458984375,
				0.24609375,
				0.2734375,
				0.3232421875,
				0.3447265625,
				0.357421875,
				0.357421875,
				0.2578125,
				0
			]
		},
		{
			"id": "RecmaoFXZebShVHCVwsHZ",
			"type": "freedraw",
			"x": -607.6867816091952,
			"y": -54.19068785919529,
			"width": 25.51724137931035,
			"height": 0.6896551724138362,
			"angle": 0,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 0,
			"opacity": 100,
			"groupIds": [],
			"strokeSharpness": "round",
			"seed": 938170072,
			"version": 14,
			"versionNonce": 1224475816,
			"isDeleted": false,
			"boundElements": null,
			"updated": 1665930258348,
			"link": null,
			"locked": false,
			"points": [
				[
					0,
					0
				],
				[
					-0.6896551724138362,
					0
				],
				[
					0.6896551724137225,
					0
				],
				[
					1.3793103448275588,
					0
				],
				[
					4.137931034482676,
					0
				],
				[
					7.586206896551744,
					0
				],
				[
					13.103448275861979,
					0
				],
				[
					16.551724137931046,
					0
				],
				[
					19.310344827586164,
					0
				],
				[
					21.37931034482756,
					0
				],
				[
					24.137931034482676,
					-0.6896551724138362
				],
				[
					24.827586206896513,
					-0.6896551724138362
				],
				[
					24.827586206896513,
					-0.6896551724138362
				]
			],
			"pressures": [
				0.05078125,
				0.2353515625,
				0.2919921875,
				0.2958984375,
				0.3046875,
				0.3134765625,
				0.3212890625,
				0.3232421875,
				0.32421875,
				0.32421875,
				0.3173828125,
				0.2802734375,
				0
			],
			"simulatePressure": false,
			"lastCommittedPoint": [
				24.827586206896513,
				-0.6896551724138362
			]
		},
		{
			"id": "ybaTujbuC8hga6xdKSvfS",
			"type": "freedraw",
			"x": -593.2040229885056,
			"y": -52.121722341953955,
			"width": 0.6896551724138362,
			"height": 31.724137931034477,
			"angle": 0,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 0,
			"opacity": 100,
			"groupIds": [],
			"strokeSharpness": "round",
			"seed": 1479670232,
			"version": 12,
			"versionNonce": 2095736488,
			"isDeleted": false,
			"boundElements": null,
			"updated": 1665930258857,
			"link": null,
			"locked": false,
			"points": [
				[
					0,
					0
				],
				[
					0,
					0.6896551724138362
				],
				[
					0,
					2.068965517241395
				],
				[
					0,
					5.517241379310349
				],
				[
					-0.6896551724138362,
					12.413793103448313
				],
				[
					-0.6896551724138362,
					17.241379310344826
				],
				[
					-0.6896551724138362,
					22.068965517241395
				],
				[
					-0.6896551724138362,
					26.206896551724185
				],
				[
					-0.6896551724138362,
					30.344827586206918
				],
				[
					-0.6896551724138362,
					31.724137931034477
				],
				[
					-0.6896551724138362,
					31.724137931034477
				]
			],
			"pressures": [
				0.03125,
				0.234375,
				0.23828125,
				0.2431640625,
				0.244140625,
				0.244140625,
				0.244140625,
				0.23828125,
				0.21484375,
				0.0771484375,
				0
			],
			"simulatePressure": false,
			"lastCommittedPoint": [
				-0.6896551724138362,
				31.724137931034477
			]
		},
		{
			"id": "3U9061B9ImJMTt-Om6uUF",
			"type": "freedraw",
			"x": -586.3074712643677,
			"y": -25.22517061781599,
			"width": 15.172413793103374,
			"height": 15.17241379310343,
			"angle": 0,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 0,
			"opacity": 100,
			"groupIds": [],
			"strokeSharpness": "round",
			"seed": 1783805912,
			"version": 30,
			"versionNonce": 1461439400,
			"isDeleted": false,
			"boundElements": null,
			"updated": 1665930259782,
			"link": null,
			"locked": false,
			"points": [
				[
					0,
					0
				],
				[
					3.4482758620689538,
					-0.6896551724137794
				],
				[
					5.517241379310349,
					-1.3793103448276156
				],
				[
					7.586206896551744,
					-2.068965517241395
				],
				[
					9.655172413793139,
					-3.4482758620689538
				],
				[
					10.344827586206861,
					-4.13793103448279
				],
				[
					10.344827586206861,
					-5.517241379310349
				],
				[
					10.344827586206861,
					-6.206896551724128
				],
				[
					8.965517241379303,
					-6.896551724137964
				],
				[
					7.586206896551744,
					-7.586206896551744
				],
				[
					6.206896551724185,
					-7.586206896551744
				],
				[
					4.8275862068965125,
					-7.586206896551744
				],
				[
					2.7586206896551175,
					-7.586206896551744
				],
				[
					1.3793103448275588,
					-6.206896551724128
				],
				[
					0,
					-4.827586206896569
				],
				[
					-0.6896551724138362,
					-3.4482758620689538
				],
				[
					-1.3793103448275588,
					-1.3793103448276156
				],
				[
					-1.3793103448275588,
					1.3793103448275588
				],
				[
					-1.3793103448275588,
					3.4482758620689538
				],
				[
					-1.3793103448275588,
					4.8275862068965125
				],
				[
					-0.6896551724138362,
					6.206896551724128
				],
				[
					1.3793103448275588,
					7.586206896551687
				],
				[
					3.4482758620689538,
					7.586206896551687
				],
				[
					6.206896551724185,
					7.586206896551687
				],
				[
					8.275862068965466,
					6.8965517241379075
				],
				[
					10.344827586206861,
					5.517241379310349
				],
				[
					12.413793103448256,
					4.137931034482733
				],
				[
					13.793103448275815,
					3.4482758620689538
				],
				[
					13.793103448275815,
					3.4482758620689538
				]
			],
			"pressures": [
				0.060546875,
				0.1552734375,
				0.1796875,
				0.1953125,
				0.2275390625,
				0.240234375,
				0.2509765625,
				0.2607421875,
				0.275390625,
				0.2822265625,
				0.2890625,
				0.2890625,
				0.2900390625,
				0.283203125,
				0.27734375,
				0.2744140625,
				0.2734375,
				0.2724609375,
				0.2705078125,
				0.26953125,
				0.2685546875,
				0.267578125,
				0.265625,
				0.263671875,
				0.2626953125,
				0.2578125,
				0.2421875,
				0.0166015625,
				0
			],
			"simulatePressure": false,
			"lastCommittedPoint": [
				13.793103448275815,
				3.4482758620689538
			]
		},
		{
			"id": "n49ksR6C8GCepI5fN4u2w",
			"type": "freedraw",
			"x": -569.0660919540229,
			"y": -31.43206716954012,
			"width": 10.344827586206975,
			"height": 11.03448275862064,
			"angle": 0,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 0,
			"opacity": 100,
			"groupIds": [],
			"strokeSharpness": "round",
			"seed": 303440600,
			"version": 23,
			"versionNonce": 324835800,
			"isDeleted": false,
			"boundElements": null,
			"updated": 1665930260404,
			"link": null,
			"locked": false,
			"points": [
				[
					0,
					0
				],
				[
					0,
					2.068965517241338
				],
				[
					0,
					4.137931034482733
				],
				[
					0,
					6.8965517241379075
				],
				[
					0.6896551724138362,
					8.275862068965466
				],
				[
					0.6896551724138362,
					9.655172413793082
				],
				[
					1.3793103448276725,
					9.655172413793082
				],
				[
					1.3793103448276725,
					8.965517241379303
				],
				[
					1.3793103448276725,
					7.586206896551687
				],
				[
					2.758620689655231,
					4.8275862068965125
				],
				[
					3.4482758620689538,
					3.4482758620689538
				],
				[
					4.13793103448279,
					2.7586206896551744
				],
				[
					4.827586206896626,
					2.7586206896551744
				],
				[
					5.517241379310349,
					2.7586206896551744
				],
				[
					6.896551724138021,
					2.7586206896551744
				],
				[
					7.586206896551744,
					3.4482758620689538
				],
				[
					8.27586206896558,
					4.8275862068965125
				],
				[
					8.965517241379416,
					6.8965517241379075
				],
				[
					9.655172413793139,
					8.965517241379303
				],
				[
					9.655172413793139,
					10.344827586206861
				],
				[
					10.344827586206975,
					11.03448275862064
				],
				[
					10.344827586206975,
					11.03448275862064
				]
			],
			"pressures": [
				0.078125,
				0.26953125,
				0.2802734375,
				0.28515625,
				0.2880859375,
				0.2890625,
				0.248046875,
				0.240234375,
				0.2373046875,
				0.22265625,
				0.2216796875,
				0.22265625,
				0.2236328125,
				0.2275390625,
				0.244140625,
				0.255859375,
				0.2646484375,
				0.26953125,
				0.271484375,
				0.2421875,
				0.2080078125,
				0
			],
			"simulatePressure": false,
			"lastCommittedPoint": [
				10.344827586206975,
				11.03448275862064
			]
		},
		{
			"id": "mF4ZV2SuwSOSCvaEJasdE",
			"type": "freedraw",
			"x": -546.9971264367815,
			"y": -30.74241199712634,
			"width": 7.586206896551744,
			"height": 11.724137931034477,
			"angle": 0,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 0,
			"opacity": 100,
			"groupIds": [],
			"strokeSharpness": "round",
			"seed": 1370787752,
			"version": 28,
			"versionNonce": 718143448,
			"isDeleted": false,
			"boundElements": null,
			"updated": 1665930261201,
			"link": null,
			"locked": false,
			"points": [
				[
					0,
					0
				],
				[
					-0.6896551724137225,
					0
				],
				[
					-1.3793103448275588,
					0
				],
				[
					-2.068965517241395,
					0
				],
				[
					-3.4482758620689538,
					0
				],
				[
					-4.137931034482676,
					0.6896551724137794
				],
				[
					-4.8275862068965125,
					1.3793103448275588
				],
				[
					-4.8275862068965125,
					2.068965517241395
				],
				[
					-4.137931034482676,
					2.7586206896551744
				],
				[
					-3.4482758620689538,
					3.4482758620689538
				],
				[
					-2.068965517241395,
					3.4482758620689538
				],
				[
					-1.3793103448275588,
					4.137931034482733
				],
				[
					0,
					5.517241379310349
				],
				[
					0.6896551724138362,
					6.206896551724128
				],
				[
					1.3793103448275588,
					6.206896551724128
				],
				[
					1.3793103448275588,
					6.8965517241379075
				],
				[
					2.758620689655231,
					7.586206896551687
				],
				[
					2.758620689655231,
					8.275862068965523
				],
				[
					2.758620689655231,
					8.965517241379303
				],
				[
					2.758620689655231,
					9.655172413793082
				],
				[
					2.068965517241395,
					10.344827586206861
				],
				[
					1.3793103448275588,
					10.344827586206861
				],
				[
					0,
					11.034482758620697
				],
				[
					-0.6896551724137225,
					11.034482758620697
				],
				[
					-2.068965517241395,
					11.724137931034477
				],
				[
					-2.7586206896551175,
					11.724137931034477
				],
				[
					-2.7586206896551175,
					11.724137931034477
				]
			],
			"pressures": [
				0.0302734375,
				0.2958984375,
				0.3076171875,
				0.31640625,
				0.337890625,
				0.33984375,
				0.33984375,
				0.3388671875,
				0.3359375,
				0.3359375,
				0.3359375,
				0.3359375,
				0.3359375,
				0.3369140625,
				0.3359375,
				0.3359375,
				0.3388671875,
				0.3388671875,
				0.33984375,
				0.353515625,
				0.357421875,
				0.3583984375,
				0.357421875,
				0.3544921875,
				0.3232421875,
				0.12109375,
				0
			],
			"simulatePressure": false,
			"lastCommittedPoint": [
				-2.7586206896551175,
				11.724137931034477
			]
		},
		{
			"id": "lEApmmrcy-OSL445iQu3S",
			"type": "freedraw",
			"x": -535.272988505747,
			"y": -25.22517061781599,
			"width": 0,
			"height": 8.275862068965523,
			"angle": 0,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 0,
			"opacity": 100,
			"groupIds": [],
			"strokeSharpness": "round",
			"seed": 821332392,
			"version": 8,
			"versionNonce": 549671896,
			"isDeleted": false,
			"boundElements": null,
			"updated": 1665930261679,
			"link": null,
			"locked": false,
			"points": [
				[
					0,
					0
				],
				[
					0,
					1.3793103448275588
				],
				[
					0,
					3.4482758620689538
				],
				[
					0,
					5.517241379310349
				],
				[
					0,
					6.8965517241379075
				],
				[
					0,
					8.275862068965523
				],
				[
					0,
					8.275862068965523
				]
			],
			"pressures": [
				0.076171875,
				0.306640625,
				0.314453125,
				0.31640625,
				0.31640625,
				0.2705078125,
				0
			],
			"simulatePressure": false,
			"lastCommittedPoint": [
				0,
				8.275862068965523
			]
		},
		{
			"id": "D1aujx37yUnplUjIB0fnv",
			"type": "freedraw",
			"x": -535.9626436781608,
			"y": -35.56999820402291,
			"width": 0,
			"height": 0.6896551724137794,
			"angle": 0,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 0,
			"opacity": 100,
			"groupIds": [],
			"strokeSharpness": "round",
			"seed": 1051620776,
			"version": 5,
			"versionNonce": 66792360,
			"isDeleted": false,
			"boundElements": null,
			"updated": 1665930261919,
			"link": null,
			"locked": false,
			"points": [
				[
					0,
					0
				],
				[
					0,
					0.6896551724137794
				],
				[
					0,
					0
				]
			],
			"pressures": [
				0.181640625,
				0.2333984375,
				0
			],
			"simulatePressure": false,
			"lastCommittedPoint": [
				0,
				0.6896551724137794
			]
		},
		{
			"id": "Yklb6vn3wQbAS_22zsJOn",
			"type": "freedraw",
			"x": -526.9971264367815,
			"y": -17.638963721264304,
			"width": 7.586206896551744,
			"height": 43.448275862068954,
			"angle": 0,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 0,
			"opacity": 100,
			"groupIds": [],
			"strokeSharpness": "round",
			"seed": 761053912,
			"version": 21,
			"versionNonce": 1847563480,
			"isDeleted": false,
			"boundElements": null,
			"updated": 1665930262533,
			"link": null,
			"locked": false,
			"points": [
				[
					0,
					0
				],
				[
					1.3793103448275588,
					-2.068965517241338
				],
				[
					2.068965517241395,
					-5.517241379310349
				],
				[
					3.4482758620689538,
					-9.655172413793082
				],
				[
					4.827586206896626,
					-15.86206896551721
				],
				[
					4.827586206896626,
					-27.586206896551687
				],
				[
					4.827586206896626,
					-34.48275862068965
				],
				[
					4.827586206896626,
					-40
				],
				[
					4.13793103448279,
					-42.75862068965512
				],
				[
					3.4482758620689538,
					-43.448275862068954
				],
				[
					1.3793103448275588,
					-41.37931034482756
				],
				[
					0.6896551724138362,
					-35.86206896551721
				],
				[
					0,
					-28.275862068965466
				],
				[
					0,
					-19.310344827586164
				],
				[
					1.3793103448275588,
					-6.206896551724128
				],
				[
					2.758620689655231,
					-4.137931034482733
				],
				[
					4.13793103448279,
					-2.068965517241338
				],
				[
					6.206896551724185,
					-1.3793103448275588
				],
				[
					7.586206896551744,
					-3.4482758620689538
				],
				[
					7.586206896551744,
					-3.4482758620689538
				]
			],
			"pressures": [
				0.048828125,
				0.244140625,
				0.2548828125,
				0.2626953125,
				0.2666015625,
				0.2685546875,
				0.2578125,
				0.2509765625,
				0.2509765625,
				0.25390625,
				0.287109375,
				0.287109375,
				0.287109375,
				0.2861328125,
				0.28515625,
				0.2861328125,
				0.2861328125,
				0.2841796875,
				0.169921875,
				0
			],
			"simulatePressure": false,
			"lastCommittedPoint": [
				7.586206896551744,
				-3.4482758620689538
			]
		},
		{
			"id": "yFfCDkSWXC6xJacO4mNtd",
			"type": "freedraw",
			"x": -513.8936781609194,
			"y": -23.845860272988432,
			"width": 14.482758620689651,
			"height": 14.482758620689651,
			"angle": 0,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 0,
			"opacity": 100,
			"groupIds": [],
			"strokeSharpness": "round",
			"seed": 888325288,
			"version": 24,
			"versionNonce": 990515416,
			"isDeleted": false,
			"boundElements": null,
			"updated": 1665930263265,
			"link": null,
			"locked": false,
			"points": [
				[
					0,
					0
				],
				[
					0.6896551724138362,
					0
				],
				[
					1.3793103448275588,
					0
				],
				[
					2.068965517241395,
					0
				],
				[
					3.4482758620689538,
					-0.6896551724137794
				],
				[
					5.517241379310349,
					-2.7586206896551744
				],
				[
					6.206896551724128,
					-4.137931034482733
				],
				[
					6.206896551724128,
					-4.8275862068965125
				],
				[
					6.206896551724128,
					-5.517241379310349
				],
				[
					4.13793103448279,
					-6.206896551724128
				],
				[
					2.7586206896551744,
					-6.206896551724128
				],
				[
					1.3793103448275588,
					-6.206896551724128
				],
				[
					0,
					-4.8275862068965125
				],
				[
					-1.3793103448275588,
					-0.6896551724137794
				],
				[
					-2.068965517241395,
					2.068965517241395
				],
				[
					-2.068965517241395,
					4.13793103448279
				],
				[
					-1.3793103448275588,
					6.206896551724128
				],
				[
					0.6896551724138362,
					7.586206896551744
				],
				[
					4.13793103448279,
					8.275862068965523
				],
				[
					6.8965517241379075,
					8.275862068965523
				],
				[
					9.655172413793139,
					6.896551724137964
				],
				[
					12.413793103448256,
					4.827586206896569
				],
				[
					12.413793103448256,
					4.827586206896569
				]
			],
			"pressures": [
				0.03515625,
				0.2529296875,
				0.2666015625,
				0.2763671875,
				0.28125,
				0.287109375,
				0.2890625,
				0.291015625,
				0.2939453125,
				0.3125,
				0.3125,
				0.3115234375,
				0.3056640625,
				0.3037109375,
				0.3046875,
				0.3046875,
				0.306640625,
				0.3125,
				0.3154296875,
				0.310546875,
				0.27734375,
				0.0234375,
				0
			],
			"simulatePressure": false,
			"lastCommittedPoint": [
				12.413793103448256,
				4.827586206896569
			]
		},
		{
			"id": "AanBXsjeRNJ8lwzMRNRc4",
			"type": "freedraw",
			"x": -473.8936781609194,
			"y": -20.397584410919478,
			"width": 13.793103448275815,
			"height": 42.06896551724134,
			"angle": 0,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 0,
			"opacity": 100,
			"groupIds": [],
			"strokeSharpness": "round",
			"seed": 1593706664,
			"version": 27,
			"versionNonce": 1292072872,
			"isDeleted": false,
			"boundElements": null,
			"updated": 1665930264169,
			"link": null,
			"locked": false,
			"points": [
				[
					0,
					0
				],
				[
					0.6896551724137794,
					0
				],
				[
					2.068965517241395,
					-2.068965517241338
				],
				[
					3.4482758620689538,
					-6.206896551724128
				],
				[
					5.517241379310349,
					-12.413793103448256
				],
				[
					6.206896551724128,
					-19.310344827586164
				],
				[
					6.8965517241379075,
					-25.517241379310292
				],
				[
					6.206896551724128,
					-34.48275862068965
				],
				[
					4.13793103448279,
					-37.931034482758605
				],
				[
					2.7586206896551744,
					-39.310344827586164
				],
				[
					2.068965517241395,
					-39.310344827586164
				],
				[
					0.6896551724137794,
					-34.48275862068965
				],
				[
					0,
					-28.275862068965466
				],
				[
					0,
					-20
				],
				[
					0,
					-12.413793103448256
				],
				[
					1.3793103448275588,
					-5.517241379310292
				],
				[
					3.4482758620689538,
					1.3793103448276156
				],
				[
					4.8275862068965125,
					2.7586206896551744
				],
				[
					6.206896551724128,
					2.7586206896551744
				],
				[
					7.586206896551744,
					2.7586206896551744
				],
				[
					9.655172413793139,
					1.3793103448276156
				],
				[
					11.034482758620697,
					0.6896551724138362
				],
				[
					12.413793103448256,
					-0.6896551724137794
				],
				[
					13.103448275862092,
					-1.3793103448275588
				],
				[
					13.793103448275815,
					-1.3793103448275588
				],
				[
					13.793103448275815,
					-1.3793103448275588
				]
			],
			"pressures": [
				0.0703125,
				0.2705078125,
				0.3095703125,
				0.318359375,
				0.322265625,
				0.3232421875,
				0.32421875,
				0.3212890625,
				0.314453125,
				0.30859375,
				0.3095703125,
				0.302734375,
				0.3017578125,
				0.3017578125,
				0.302734375,
				0.3046875,
				0.3173828125,
				0.3173828125,
				0.314453125,
				0.3134765625,
				0.2841796875,
				0.1943359375,
				0.119140625,
				0.0673828125,
				0.0517578125,
				0
			],
			"simulatePressure": false,
			"lastCommittedPoint": [
				13.793103448275815,
				-1.3793103448275588
			]
		},
		{
			"id": "5H0RHIwW59GGNJRCF-S6Q",
			"type": "freedraw",
			"x": -458.721264367816,
			"y": -22.466549928160816,
			"width": 7.586206896551744,
			"height": 8.965517241379303,
			"angle": 0,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 0,
			"opacity": 100,
			"groupIds": [],
			"strokeSharpness": "round",
			"seed": 282015448,
			"version": 21,
			"versionNonce": 852560088,
			"isDeleted": false,
			"boundElements": null,
			"updated": 1665930264551,
			"link": null,
			"locked": false,
			"points": [
				[
					0,
					0
				],
				[
					0,
					0.6896551724137794
				],
				[
					0,
					1.3793103448275588
				],
				[
					0,
					2.068965517241338
				],
				[
					0.6896551724137794,
					4.137931034482733
				],
				[
					2.068965517241395,
					4.8275862068965125
				],
				[
					3.4482758620690106,
					4.8275862068965125
				],
				[
					4.827586206896569,
					4.137931034482733
				],
				[
					6.206896551724128,
					2.068965517241338
				],
				[
					6.896551724137964,
					-2.068965517241395
				],
				[
					6.896551724137964,
					-3.4482758620689538
				],
				[
					6.206896551724128,
					-4.13793103448279
				],
				[
					4.137931034482733,
					-4.13793103448279
				],
				[
					0.6896551724137794,
					-2.7586206896551744
				],
				[
					-0.6896551724137794,
					-1.3793103448276156
				],
				[
					-0.6896551724137794,
					1.3793103448275588
				],
				[
					-0.6896551724137794,
					4.137931034482733
				],
				[
					1.3793103448276156,
					4.8275862068965125
				],
				[
					0,
					0
				]
			],
			"pressures": [
				0.0185546875,
				0.318359375,
				0.3212890625,
				0.3212890625,
				0.322265625,
				0.3193359375,
				0.31640625,
				0.314453125,
				0.314453125,
				0.3173828125,
				0.3203125,
				0.333984375,
				0.3720703125,
				0.40234375,
				0.40234375,
				0.3955078125,
				0.3330078125,
				0.0634765625,
				0
			],
			"simulatePressure": false,
			"lastCommittedPoint": [
				1.3793103448276156,
				4.8275862068965125
			]
		},
		{
			"id": "OZz_eJ_ItsmTPqQFRDaY6",
			"type": "freedraw",
			"x": -444.2385057471263,
			"y": -25.22517061781599,
			"width": 11.034482758620697,
			"height": 10.344827586206918,
			"angle": 0,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 0,
			"opacity": 100,
			"groupIds": [],
			"strokeSharpness": "round",
			"seed": 1698861224,
			"version": 21,
			"versionNonce": 1310023336,
			"isDeleted": false,
			"boundElements": null,
			"updated": 1665930265063,
			"link": null,
			"locked": false,
			"points": [
				[
					0,
					0
				],
				[
					-0.6896551724138362,
					0
				],
				[
					-1.3793103448276156,
					1.3793103448275588
				],
				[
					-2.068965517241395,
					4.137931034482733
				],
				[
					-2.068965517241395,
					6.8965517241379075
				],
				[
					-0.6896551724138362,
					8.275862068965523
				],
				[
					2.7586206896551175,
					8.275862068965523
				],
				[
					5.517241379310292,
					6.206896551724128
				],
				[
					8.275862068965466,
					2.7586206896551744
				],
				[
					8.965517241379303,
					0
				],
				[
					8.965517241379303,
					-1.3793103448276156
				],
				[
					6.206896551724071,
					-2.068965517241395
				],
				[
					4.137931034482676,
					-2.068965517241395
				],
				[
					2.068965517241338,
					0
				],
				[
					0,
					2.7586206896551744
				],
				[
					-0.6896551724138362,
					6.206896551724128
				],
				[
					1.3793103448275588,
					5.517241379310349
				],
				[
					2.7586206896551175,
					4.137931034482733
				],
				[
					0,
					0
				]
			],
			"pressures": [
				0.115234375,
				0.3046875,
				0.3232421875,
				0.3330078125,
				0.337890625,
				0.337890625,
				0.328125,
				0.328125,
				0.3291015625,
				0.3291015625,
				0.3291015625,
				0.3544921875,
				0.3681640625,
				0.3701171875,
				0.3701171875,
				0.3447265625,
				0.2109375,
				0.1435546875,
				0
			],
			"simulatePressure": false,
			"lastCommittedPoint": [
				2.7586206896551175,
				4.137931034482733
			]
		},
		{
			"id": "jS8kiY0I3tVotbn_CSe2z",
			"type": "freedraw",
			"x": -436.6522988505746,
			"y": -27.983791307471165,
			"width": 6.206896551724128,
			"height": 11.034482758620697,
			"angle": 0,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 0,
			"opacity": 100,
			"groupIds": [],
			"strokeSharpness": "round",
			"seed": 1332953048,
			"version": 8,
			"versionNonce": 2117913256,
			"isDeleted": false,
			"boundElements": null,
			"updated": 1665930265231,
			"link": null,
			"locked": false,
			"points": [
				[
					0,
					0
				],
				[
					0.6896551724137794,
					5.517241379310349
				],
				[
					1.3793103448276156,
					8.275862068965523
				],
				[
					3.4482758620689538,
					10.344827586206861
				],
				[
					4.827586206896569,
					11.034482758620697
				],
				[
					6.206896551724128,
					9.655172413793082
				],
				[
					6.206896551724128,
					9.655172413793082
				]
			],
			"pressures": [
				0.166015625,
				0.396484375,
				0.4033203125,
				0.40234375,
				0.3662109375,
				0.1923828125,
				0
			],
			"simulatePressure": false,
			"lastCommittedPoint": [
				6.206896551724128,
				9.655172413793082
			]
		},
		{
			"id": "_uoxF-ajB8IJQdsBS_G4h",
			"type": "freedraw",
			"x": -421.47988505747117,
			"y": -56.94930854885047,
			"width": 11.724137931034477,
			"height": 40.68965517241378,
			"angle": 0,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 0,
			"opacity": 100,
			"groupIds": [],
			"strokeSharpness": "round",
			"seed": 684233688,
			"version": 28,
			"versionNonce": 183076008,
			"isDeleted": false,
			"boundElements": null,
			"updated": 1665930266037,
			"link": null,
			"locked": false,
			"points": [
				[
					0,
					0
				],
				[
					0,
					4.137931034482733
				],
				[
					0,
					8.275862068965523
				],
				[
					0,
					14.482758620689651
				],
				[
					0.6896551724138362,
					22.068965517241338
				],
				[
					2.7586206896551744,
					31.034482758620697
				],
				[
					4.13793103448279,
					35.86206896551721
				],
				[
					4.827586206896569,
					38.620689655172384
				],
				[
					5.517241379310349,
					39.310344827586164
				],
				[
					5.517241379310349,
					38.620689655172384
				],
				[
					5.517241379310349,
					37.241379310344826
				],
				[
					4.13793103448279,
					35.86206896551721
				],
				[
					3.4482758620689538,
					34.48275862068965
				],
				[
					1.3793103448275588,
					33.103448275862036
				],
				[
					0,
					33.103448275862036
				],
				[
					-2.068965517241395,
					33.103448275862036
				],
				[
					-4.137931034482733,
					33.793103448275815
				],
				[
					-6.206896551724128,
					35.17241379310343
				],
				[
					-6.206896551724128,
					36.55172413793099
				],
				[
					-6.206896551724128,
					37.931034482758605
				],
				[
					-6.206896551724128,
					39.310344827586164
				],
				[
					-4.8275862068965125,
					40
				],
				[
					-2.068965517241395,
					40.68965517241378
				],
				[
					0,
					40
				],
				[
					1.3793103448275588,
					39.310344827586164
				],
				[
					2.068965517241395,
					38.620689655172384
				],
				[
					2.068965517241395,
					38.620689655172384
				]
			],
			"pressures": [
				0.126953125,
				0.26953125,
				0.2998046875,
				0.3173828125,
				0.3427734375,
				0.3681640625,
				0.369140625,
				0.369140625,
				0.369140625,
				0.341796875,
				0.3310546875,
				0.326171875,
				0.3203125,
				0.3203125,
				0.3232421875,
				0.32421875,
				0.328125,
				0.333984375,
				0.341796875,
				0.3427734375,
				0.3427734375,
				0.3447265625,
				0.3388671875,
				0.3359375,
				0.2841796875,
				0.21484375,
				0
			],
			"simulatePressure": false,
			"lastCommittedPoint": [
				2.068965517241395,
				38.620689655172384
			]
		},
		{
			"id": "r81b3Frx2YS5Febr3wUyM",
			"type": "freedraw",
			"x": -195.27298850574704,
			"y": -373.5010326867815,
			"width": 21.37931034482756,
			"height": 28.965517241379303,
			"angle": 0,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 0,
			"opacity": 100,
			"groupIds": [],
			"strokeSharpness": "round",
			"seed": 366770648,
			"version": 17,
			"versionNonce": 1357062616,
			"isDeleted": false,
			"boundElements": null,
			"updated": 1665930268248,
			"link": null,
			"locked": false,
			"points": [
				[
					0,
					0
				],
				[
					-0.6896551724137794,
					0
				],
				[
					-3.4482758620689538,
					0
				],
				[
					-6.206896551724128,
					0.6896551724138362
				],
				[
					-9.655172413793082,
					2.068965517241395
				],
				[
					-14.482758620689651,
					6.206896551724185
				],
				[
					-16.551724137931046,
					9.655172413793139
				],
				[
					-17.931034482758605,
					13.793103448275872
				],
				[
					-17.931034482758605,
					19.31034482758622
				],
				[
					-15.17241379310343,
					25.51724137931035
				],
				[
					-11.034482758620697,
					27.586206896551744
				],
				[
					-7.586206896551744,
					28.275862068965523
				],
				[
					-3.4482758620689538,
					28.965517241379303
				],
				[
					-0.6896551724137794,
					28.965517241379303
				],
				[
					3.4482758620689538,
					27.586206896551744
				],
				[
					3.4482758620689538,
					27.586206896551744
				]
			],
			"pressures": [
				0.11328125,
				0.3798828125,
				0.404296875,
				0.4130859375,
				0.4169921875,
				0.419921875,
				0.419921875,
				0.4169921875,
				0.4150390625,
				0.412109375,
				0.4072265625,
				0.390625,
				0.3671875,
				0.32421875,
				0.115234375,
				0
			],
			"simulatePressure": false,
			"lastCommittedPoint": [
				3.4482758620689538,
				27.586206896551744
			]
		},
		{
			"id": "qfrz2yN4Lu58NKsSGxhqY",
			"type": "freedraw",
			"x": -180.79022988505739,
			"y": -347.29413613505733,
			"width": 8.275862068965523,
			"height": 8.275862068965523,
			"angle": 0,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 0,
			"opacity": 100,
			"groupIds": [],
			"strokeSharpness": "round",
			"seed": 2075821992,
			"version": 19,
			"versionNonce": 1684023976,
			"isDeleted": false,
			"boundElements": null,
			"updated": 1665930268720,
			"link": null,
			"locked": false,
			"points": [
				[
					0,
					0
				],
				[
					-1.3793103448275588,
					0
				],
				[
					-2.068965517241395,
					1.3793103448275588
				],
				[
					-2.7586206896551744,
					3.4482758620689538
				],
				[
					-2.7586206896551744,
					4.8275862068965125
				],
				[
					-1.3793103448275588,
					6.206896551724128
				],
				[
					0.6896551724137794,
					6.206896551724128
				],
				[
					2.068965517241395,
					5.517241379310349
				],
				[
					4.137931034482733,
					3.4482758620689538
				],
				[
					5.517241379310349,
					0.6896551724137794
				],
				[
					5.517241379310349,
					-0.6896551724138362
				],
				[
					4.137931034482733,
					-2.068965517241395
				],
				[
					2.068965517241395,
					-2.068965517241395
				],
				[
					-0.6896551724137794,
					-2.068965517241395
				],
				[
					-2.7586206896551744,
					0.6896551724137794
				],
				[
					-2.7586206896551744,
					3.4482758620689538
				],
				[
					-2.068965517241395,
					5.517241379310349
				],
				[
					-2.068965517241395,
					5.517241379310349
				]
			],
			"pressures": [
				0.109375,
				0.3203125,
				0.341796875,
				0.3427734375,
				0.3427734375,
				0.3359375,
				0.3251953125,
				0.3046875,
				0.30078125,
				0.30078125,
				0.3037109375,
				0.3271484375,
				0.3583984375,
				0.361328125,
				0.357421875,
				0.3095703125,
				0.2265625,
				0
			],
			"simulatePressure": false,
			"lastCommittedPoint": [
				-2.068965517241395,
				5.517241379310349
			]
		},
		{
			"id": "RrPncxAtHaW8L5NMJfAHh",
			"type": "freedraw",
			"x": -166.9971264367815,
			"y": -348.67344647988494,
			"width": 19.310344827586164,
			"height": 8.275862068965523,
			"angle": 0,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 0,
			"opacity": 100,
			"groupIds": [],
			"strokeSharpness": "round",
			"seed": 822817752,
			"version": 28,
			"versionNonce": 1034216616,
			"isDeleted": false,
			"boundElements": null,
			"updated": 1665930269435,
			"link": null,
			"locked": false,
			"points": [
				[
					0,
					0
				],
				[
					-0.6896551724137794,
					0
				],
				[
					-0.6896551724137794,
					0.6896551724137794
				],
				[
					-0.6896551724137794,
					3.4482758620689538
				],
				[
					0,
					4.827586206896569
				],
				[
					0,
					5.517241379310349
				],
				[
					0.6896551724137794,
					5.517241379310349
				],
				[
					2.7586206896551744,
					2.7586206896551744
				],
				[
					3.4482758620689538,
					1.3793103448276156
				],
				[
					4.137931034482733,
					0.6896551724137794
				],
				[
					4.827586206896569,
					0
				],
				[
					5.517241379310349,
					0
				],
				[
					6.206896551724128,
					1.3793103448276156
				],
				[
					6.8965517241379075,
					3.4482758620689538
				],
				[
					6.8965517241379075,
					4.827586206896569
				],
				[
					7.586206896551687,
					5.517241379310349
				],
				[
					8.965517241379303,
					5.517241379310349
				],
				[
					10.344827586206861,
					4.137931034482733
				],
				[
					12.413793103448256,
					2.068965517241395
				],
				[
					13.793103448275872,
					0.6896551724137794
				],
				[
					15.17241379310343,
					0.6896551724137794
				],
				[
					16.551724137931046,
					0.6896551724137794
				],
				[
					17.241379310344826,
					2.068965517241395
				],
				[
					17.931034482758605,
					4.827586206896569
				],
				[
					18.620689655172384,
					7.586206896551744
				],
				[
					18.620689655172384,
					8.275862068965523
				],
				[
					18.620689655172384,
					8.275862068965523
				]
			],
			"pressures": [
				0.126953125,
				0.2861328125,
				0.306640625,
				0.3291015625,
				0.33203125,
				0.3330078125,
				0.330078125,
				0.3017578125,
				0.29296875,
				0.291015625,
				0.291015625,
				0.2919921875,
				0.30859375,
				0.3330078125,
				0.3359375,
				0.3349609375,
				0.3251953125,
				0.3232421875,
				0.314453125,
				0.3115234375,
				0.3095703125,
				0.30859375,
				0.3134765625,
				0.3271484375,
				0.3232421875,
				0.26953125,
				0
			],
			"simulatePressure": false,
			"lastCommittedPoint": [
				18.620689655172384,
				8.275862068965523
			]
		},
		{
			"id": "bYdHgO98WvFDtxBl6-avc",
			"type": "freedraw",
			"x": -141.47988505747117,
			"y": -350.05275682471256,
			"width": 0.6896551724137794,
			"height": 27.586206896551744,
			"angle": 0,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 0,
			"opacity": 100,
			"groupIds": [],
			"strokeSharpness": "round",
			"seed": 117905880,
			"version": 11,
			"versionNonce": 1965290200,
			"isDeleted": false,
			"boundElements": null,
			"updated": 1665930269869,
			"link": null,
			"locked": false,
			"points": [
				[
					0,
					0
				],
				[
					0,
					4.13793103448279
				],
				[
					0,
					8.96551724137936
				],
				[
					0,
					14.482758620689708
				],
				[
					0,
					22.068965517241395
				],
				[
					0,
					26.206896551724185
				],
				[
					0,
					27.586206896551744
				],
				[
					0.6896551724137794,
					27.586206896551744
				],
				[
					0.6896551724137794,
					26.896551724137964
				],
				[
					0.6896551724137794,
					26.896551724137964
				]
			],
			"pressures": [
				0.1298828125,
				0.2744140625,
				0.306640625,
				0.3291015625,
				0.33984375,
				0.3408203125,
				0.328125,
				0.240234375,
				0.1044921875,
				0
			],
			"simulatePressure": false,
			"lastCommittedPoint": [
				0.6896551724137794,
				26.896551724137964
			]
		},
		{
			"id": "EcuJrvHjKfhJ4-NXHwb9Z",
			"type": "freedraw",
			"x": -137.34195402298843,
			"y": -342.4665499281608,
			"width": 9.655172413793139,
			"height": 10.344827586206918,
			"angle": 0,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 0,
			"opacity": 100,
			"groupIds": [],
			"strokeSharpness": "round",
			"seed": 1433093800,
			"version": 18,
			"versionNonce": 706248664,
			"isDeleted": false,
			"boundElements": null,
			"updated": 1665930270247,
			"link": null,
			"locked": false,
			"points": [
				[
					0,
					0
				],
				[
					-0.6896551724137794,
					0.6896551724138362
				],
				[
					-0.6896551724137794,
					3.4482758620689538
				],
				[
					0.6896551724137794,
					4.13793103448279
				],
				[
					2.068965517241395,
					4.13793103448279
				],
				[
					4.827586206896569,
					3.4482758620689538
				],
				[
					6.896551724137964,
					0.6896551724138362
				],
				[
					6.896551724137964,
					-1.3793103448275588
				],
				[
					6.896551724137964,
					-4.137931034482733
				],
				[
					6.206896551724128,
					-5.517241379310349
				],
				[
					4.13793103448279,
					-6.206896551724128
				],
				[
					-0.6896551724137794,
					-4.8275862068965125
				],
				[
					-2.7586206896551744,
					-2.068965517241395
				],
				[
					-2.7586206896551744,
					0.6896551724138362
				],
				[
					-2.068965517241395,
					2.7586206896551744
				],
				[
					0,
					0
				]
			],
			"pressures": [
				0.1279296875,
				0.3076171875,
				0.3115234375,
				0.3115234375,
				0.306640625,
				0.30078125,
				0.30078125,
				0.30078125,
				0.3037109375,
				0.3076171875,
				0.3291015625,
				0.33984375,
				0.3427734375,
				0.3173828125,
				0.1162109375,
				0
			],
			"simulatePressure": false,
			"lastCommittedPoint": [
				-2.068965517241395,
				2.7586206896551744
			]
		},
		{
			"id": "2uRV-0IjxokMZOp-6FS2A",
			"type": "freedraw",
			"x": -119.41091954022977,
			"y": -345.91482579022977,
			"width": 8.965517241379303,
			"height": 7.586206896551687,
			"angle": 0,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 0,
			"opacity": 100,
			"groupIds": [],
			"strokeSharpness": "round",
			"seed": 26347944,
			"version": 17,
			"versionNonce": 1429201320,
			"isDeleted": false,
			"boundElements": null,
			"updated": 1665930270730,
			"link": null,
			"locked": false,
			"points": [
				[
					0,
					0
				],
				[
					-4.13793103448279,
					1.3793103448275588
				],
				[
					-4.827586206896626,
					2.7586206896551744
				],
				[
					-4.827586206896626,
					4.827586206896569
				],
				[
					-4.13793103448279,
					6.8965517241379075
				],
				[
					-2.068965517241395,
					6.8965517241379075
				],
				[
					0.6896551724137225,
					6.8965517241379075
				],
				[
					2.7586206896551175,
					4.827586206896569
				],
				[
					4.137931034482676,
					3.4482758620689538
				],
				[
					4.137931034482676,
					0.6896551724137794
				],
				[
					3.4482758620689538,
					-0.6896551724137794
				],
				[
					1.3793103448275588,
					-0.6896551724137794
				],
				[
					-0.6896551724138362,
					0
				],
				[
					-2.758620689655231,
					3.4482758620689538
				],
				[
					-2.758620689655231,
					4.827586206896569
				],
				[
					-2.758620689655231,
					4.827586206896569
				]
			],
			"pressures": [
				0.0859375,
				0.326171875,
				0.33984375,
				0.3466796875,
				0.3447265625,
				0.3388671875,
				0.3359375,
				0.3369140625,
				0.3388671875,
				0.345703125,
				0.3564453125,
				0.373046875,
				0.3798828125,
				0.357421875,
				0.228515625,
				0
			],
			"simulatePressure": false,
			"lastCommittedPoint": [
				-2.758620689655231,
				4.827586206896569
			]
		},
		{
			"id": "E3NeF-6KNgCZHICX7Euhf",
			"type": "freedraw",
			"x": -103.54885057471256,
			"y": -347.29413613505733,
			"width": 8.965517241379303,
			"height": 11.034482758620697,
			"angle": 0,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 0,
			"opacity": 100,
			"groupIds": [],
			"strokeSharpness": "round",
			"seed": 1064725720,
			"version": 22,
			"versionNonce": 2068134568,
			"isDeleted": false,
			"boundElements": null,
			"updated": 1665930271248,
			"link": null,
			"locked": false,
			"points": [
				[
					0,
					0
				],
				[
					-0.6896551724138362,
					-0.6896551724138362
				],
				[
					-2.068965517241395,
					-0.6896551724138362
				],
				[
					-2.758620689655231,
					-0.6896551724138362
				],
				[
					-4.8275862068965125,
					-0.6896551724138362
				],
				[
					-6.206896551724185,
					0.6896551724137794
				],
				[
					-6.8965517241379075,
					1.3793103448275588
				],
				[
					-6.8965517241379075,
					2.7586206896551175
				],
				[
					-4.8275862068965125,
					3.4482758620689538
				],
				[
					-2.758620689655231,
					4.137931034482733
				],
				[
					0,
					5.517241379310349
				],
				[
					1.3793103448275588,
					6.206896551724128
				],
				[
					2.068965517241395,
					6.206896551724128
				],
				[
					2.068965517241395,
					6.8965517241379075
				],
				[
					2.068965517241395,
					8.965517241379303
				],
				[
					0,
					9.655172413793082
				],
				[
					-2.068965517241395,
					10.344827586206861
				],
				[
					-3.4482758620689538,
					10.344827586206861
				],
				[
					-4.13793103448279,
					10.344827586206861
				],
				[
					-4.8275862068965125,
					10.344827586206861
				],
				[
					-4.8275862068965125,
					10.344827586206861
				]
			],
			"pressures": [
				0.0703125,
				0.306640625,
				0.337890625,
				0.3642578125,
				0.376953125,
				0.3837890625,
				0.384765625,
				0.380859375,
				0.375,
				0.3740234375,
				0.3740234375,
				0.375,
				0.3759765625,
				0.3798828125,
				0.416015625,
				0.421875,
				0.423828125,
				0.419921875,
				0.412109375,
				0.3369140625,
				0
			],
			"simulatePressure": false,
			"lastCommittedPoint": [
				-4.8275862068965125,
				10.344827586206861
			]
		},
		{
			"id": "JRkZsKLJQNT_VaypBnKGX",
			"type": "freedraw",
			"x": -93.89367816091942,
			"y": -342.4665499281608,
			"width": 0.6896551724137225,
			"height": 6.896551724137964,
			"angle": 0,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 0,
			"opacity": 100,
			"groupIds": [],
			"strokeSharpness": "round",
			"seed": 851387352,
			"version": 8,
			"versionNonce": 1453237928,
			"isDeleted": false,
			"boundElements": null,
			"updated": 1665930271527,
			"link": null,
			"locked": false,
			"points": [
				[
					0,
					0
				],
				[
					0.6896551724137225,
					1.3793103448276156
				],
				[
					0.6896551724137225,
					4.13793103448279
				],
				[
					0.6896551724137225,
					5.517241379310349
				],
				[
					0.6896551724137225,
					6.206896551724185
				],
				[
					0,
					6.896551724137964
				],
				[
					0,
					6.896551724137964
				]
			],
			"pressures": [
				0.06640625,
				0.3125,
				0.3544921875,
				0.361328125,
				0.361328125,
				0.35546875,
				0
			],
			"simulatePressure": false,
			"lastCommittedPoint": [
				0,
				6.896551724137964
			]
		},
		{
			"id": "3MRGFnKI8m7wdWTJpEdS6",
			"type": "freedraw",
			"x": -93.89367816091942,
			"y": -348.67344647988494,
			"width": 0,
			"height": 0.6896551724137794,
			"angle": 0,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 0,
			"opacity": 100,
			"groupIds": [],
			"strokeSharpness": "round",
			"seed": 1694660568,
			"version": 5,
			"versionNonce": 662200792,
			"isDeleted": false,
			"boundElements": null,
			"updated": 1665930271714,
			"link": null,
			"locked": false,
			"points": [
				[
					0,
					0
				],
				[
					0,
					-0.6896551724137794
				],
				[
					0,
					0
				]
			],
			"pressures": [
				0.2109375,
				0.2939453125,
				0
			],
			"simulatePressure": false,
			"lastCommittedPoint": [
				0,
				-0.6896551724137794
			]
		},
		{
			"id": "I47pHo5PWH_bVe5tu4ctr",
			"type": "freedraw",
			"x": -86.99712643678151,
			"y": -345.91482579022977,
			"width": 10.344827586206861,
			"height": 0.6896551724137794,
			"angle": 0,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 0,
			"opacity": 100,
			"groupIds": [],
			"strokeSharpness": "round",
			"seed": 1947813800,
			"version": 6,
			"versionNonce": 340037848,
			"isDeleted": false,
			"boundElements": null,
			"updated": 1665930272177,
			"link": null,
			"locked": false,
			"points": [
				[
					0,
					0
				],
				[
					5.517241379310349,
					0
				],
				[
					8.275862068965466,
					0
				],
				[
					10.344827586206861,
					-0.6896551724137794
				],
				[
					10.344827586206861,
					-0.6896551724137794
				]
			],
			"pressures": [
				0.130859375,
				0.3505859375,
				0.359375,
				0.361328125,
				0
			],
			"simulatePressure": false,
			"lastCommittedPoint": [
				10.344827586206861,
				-0.6896551724137794
			]
		},
		{
			"id": "I1M2nfWdhL7RckOV8_uRK",
			"type": "freedraw",
			"x": -80.79022988505744,
			"y": -355.56999820402285,
			"width": 10.344827586206861,
			"height": 20.68965517241378,
			"angle": 0,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 0,
			"opacity": 100,
			"groupIds": [],
			"strokeSharpness": "round",
			"seed": 749439144,
			"version": 11,
			"versionNonce": 2115575720,
			"isDeleted": false,
			"boundElements": null,
			"updated": 1665930272471,
			"link": null,
			"locked": false,
			"points": [
				[
					0,
					0
				],
				[
					-0.6896551724137225,
					8.965517241379303
				],
				[
					-1.3793103448275588,
					13.793103448275872
				],
				[
					-2.0689655172412813,
					17.931034482758605
				],
				[
					-1.3793103448275588,
					20
				],
				[
					0.6896551724138362,
					20.68965517241378
				],
				[
					4.827586206896626,
					20
				],
				[
					6.896551724138021,
					18.620689655172384
				],
				[
					8.27586206896558,
					17.241379310344826
				],
				[
					8.27586206896558,
					17.241379310344826
				]
			],
			"pressures": [
				0.1240234375,
				0.3095703125,
				0.3203125,
				0.326171875,
				0.3251953125,
				0.3154296875,
				0.2939453125,
				0.2255859375,
				0.162109375,
				0
			],
			"simulatePressure": false,
			"lastCommittedPoint": [
				8.27586206896558,
				17.241379310344826
			]
		},
		{
			"id": "GQxz01FBuIo9ioa9E3aJX",
			"type": "freedraw",
			"x": -69.06609195402291,
			"y": -341.776894755747,
			"width": 10.344827586206861,
			"height": 13.103448275862036,
			"angle": 0,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 0,
			"opacity": 100,
			"groupIds": [],
			"strokeSharpness": "round",
			"seed": 1665666776,
			"version": 19,
			"versionNonce": 1979522008,
			"isDeleted": false,
			"boundElements": null,
			"updated": 1665930272898,
			"link": null,
			"locked": false,
			"points": [
				[
					0,
					0
				],
				[
					4.13793103448279,
					0
				],
				[
					6.206896551724185,
					-1.3793103448276156
				],
				[
					7.586206896551744,
					-2.758620689655231
				],
				[
					7.586206896551744,
					-3.4482758620690106
				],
				[
					7.586206896551744,
					-4.827586206896569
				],
				[
					6.206896551724185,
					-5.517241379310349
				],
				[
					2.7586206896551175,
					-5.517241379310349
				],
				[
					0.6896551724138362,
					-4.827586206896569
				],
				[
					-1.3793103448275588,
					-2.758620689655231
				],
				[
					-2.068965517241395,
					0.6896551724137794
				],
				[
					-2.068965517241395,
					3.4482758620689538
				],
				[
					-2.068965517241395,
					6.8965517241379075
				],
				[
					0,
					7.586206896551687
				],
				[
					2.068965517241395,
					7.586206896551687
				],
				[
					5.517241379310349,
					6.206896551724128
				],
				[
					8.275862068965466,
					4.137931034482733
				],
				[
					8.275862068965466,
					4.137931034482733
				]
			],
			"pressures": [
				0.138671875,
				0.263671875,
				0.27734375,
				0.2841796875,
				0.2890625,
				0.3310546875,
				0.3447265625,
				0.3486328125,
				0.3486328125,
				0.349609375,
				0.349609375,
				0.3486328125,
				0.34765625,
				0.3447265625,
				0.3427734375,
				0.3212890625,
				0.109375,
				0
			],
			"simulatePressure": false,
			"lastCommittedPoint": [
				8.275862068965466,
				4.137931034482733
			]
		},
		{
			"id": "UcHm-QYfBvgwdSnDgEcbC",
			"type": "freedraw",
			"x": -112.51436781609186,
			"y": -327.98379130747117,
			"width": 21.379310344827672,
			"height": 69.65517241379308,
			"angle": 0,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 0,
			"opacity": 100,
			"groupIds": [],
			"strokeSharpness": "round",
			"seed": 1221930408,
			"version": 51,
			"versionNonce": 199606440,
			"isDeleted": false,
			"boundElements": null,
			"updated": 1665930274690,
			"link": null,
			"locked": false,
			"points": [
				[
					0,
					0
				],
				[
					0.6896551724138362,
					-0.6896551724137794
				],
				[
					2.7586206896551175,
					-0.6896551724137794
				],
				[
					5.517241379310349,
					0.6896551724138362
				],
				[
					7.586206896551744,
					2.068965517241395
				],
				[
					9.655172413793139,
					4.827586206896569
				],
				[
					12.413793103448256,
					8.965517241379303
				],
				[
					13.103448275862092,
					13.103448275862092
				],
				[
					13.793103448275815,
					17.241379310344882
				],
				[
					13.793103448275815,
					21.379310344827616
				],
				[
					13.793103448275815,
					25.51724137931035
				],
				[
					13.103448275862092,
					28.275862068965523
				],
				[
					11.72413793103442,
					30.344827586206918
				],
				[
					10.344827586206861,
					31.724137931034534
				],
				[
					7.586206896551744,
					33.79310344827587
				],
				[
					5.517241379310349,
					35.17241379310349
				],
				[
					4.8275862068965125,
					37.24137931034488
				],
				[
					3.4482758620689538,
					39.31034482758622
				],
				[
					2.7586206896551175,
					42.068965517241395
				],
				[
					2.068965517241395,
					46.206896551724185
				],
				[
					2.068965517241395,
					48.9655172413793
				],
				[
					2.068965517241395,
					51.0344827586207
				],
				[
					2.068965517241395,
					53.10344827586209
				],
				[
					2.7586206896551175,
					55.86206896551727
				],
				[
					3.4482758620689538,
					57.931034482758605
				],
				[
					4.13793103448279,
					58.62068965517244
				],
				[
					4.8275862068965125,
					59.31034482758622
				],
				[
					4.13793103448279,
					59.31034482758622
				],
				[
					2.7586206896551175,
					59.31034482758622
				],
				[
					0.6896551724138362,
					57.931034482758605
				],
				[
					-2.068965517241395,
					55.86206896551727
				],
				[
					-4.13793103448279,
					53.79310344827587
				],
				[
					-5.517241379310349,
					52.413793103448256
				],
				[
					-6.206896551724185,
					51.724137931034534
				],
				[
					-6.206896551724185,
					51.0344827586207
				],
				[
					-5.517241379310349,
					51.724137931034534
				],
				[
					-4.13793103448279,
					54.48275862068965
				],
				[
					-2.068965517241395,
					57.931034482758605
				],
				[
					0.6896551724138362,
					62.758620689655174
				],
				[
					2.7586206896551175,
					66.89655172413791
				],
				[
					2.7586206896551175,
					68.27586206896552
				],
				[
					3.4482758620689538,
					68.9655172413793
				],
				[
					4.13793103448279,
					68.9655172413793
				],
				[
					4.8275862068965125,
					68.27586206896552
				],
				[
					8.275862068965466,
					62.758620689655174
				],
				[
					10.344827586206861,
					58.62068965517244
				],
				[
					12.413793103448256,
					55.17241379310349
				],
				[
					14.482758620689651,
					53.10344827586209
				],
				[
					15.172413793103487,
					52.413793103448256
				],
				[
					15.172413793103487,
					52.413793103448256
				]
			],
			"pressures": [
				0.1259765625,
				0.2978515625,
				0.310546875,
				0.3203125,
				0.3330078125,
				0.3408203125,
				0.3603515625,
				0.365234375,
				0.3671875,
				0.3681640625,
				0.369140625,
				0.37109375,
				0.373046875,
				0.3740234375,
				0.375,
				0.3759765625,
				0.3759765625,
				0.376953125,
				0.376953125,
				0.37890625,
				0.3798828125,
				0.3837890625,
				0.384765625,
				0.38671875,
				0.384765625,
				0.3857421875,
				0.384765625,
				0.4150390625,
				0.4140625,
				0.4140625,
				0.412109375,
				0.41015625,
				0.4091796875,
				0.41015625,
				0.4111328125,
				0.404296875,
				0.404296875,
				0.40625,
				0.4072265625,
				0.4072265625,
				0.4091796875,
				0.4091796875,
				0.4033203125,
				0.40234375,
				0.4052734375,
				0.4111328125,
				0.412109375,
				0.384765625,
				0.220703125,
				0
			],
			"simulatePressure": false,
			"lastCommittedPoint": [
				15.172413793103487,
				52.413793103448256
			]
		},
		{
			"id": "QY-ejl30dNJVdk6NAeb3F",
			"type": "freedraw",
			"x": -591.824712643678,
			"y": -42.466549928160816,
			"width": 13.103448275862092,
			"height": 1.3793103448276156,
			"angle": 0,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 0,
			"opacity": 100,
			"groupIds": [],
			"strokeSharpness": "round",
			"seed": 2077214168,
			"version": 11,
			"versionNonce": 376788904,
			"isDeleted": true,
			"boundElements": null,
			"updated": 1665930257084,
			"link": null,
			"locked": false,
			"points": [
				[
					0,
					0
				],
				[
					-0.6896551724138362,
					0
				],
				[
					3.4482758620689538,
					0
				],
				[
					6.206896551724071,
					-0.6896551724137794
				],
				[
					9.655172413793139,
					-1.3793103448276156
				],
				[
					11.034482758620697,
					-1.3793103448276156
				],
				[
					12.413793103448256,
					-1.3793103448276156
				],
				[
					12.413793103448256,
					-1.3793103448276156
				]
			],
			"pressures": [
				0,
				0.27734375,
				0.3310546875,
				0.333984375,
				0.33984375,
				0.3427734375,
				0.3427734375,
				0
			],
			"simulatePressure": false,
			"lastCommittedPoint": [
				12.413793103448256,
				-1.3793103448276156
			]
		},
		{
			"id": "iw69rJWvfX7FFK0IYREkn",
			"type": "freedraw",
			"x": -586.3074712643677,
			"y": -47.294136135057386,
			"width": 2.7586206896551175,
			"height": 30.344827586206918,
			"angle": 0,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 0,
			"opacity": 100,
			"groupIds": [],
			"strokeSharpness": "round",
			"seed": 662114216,
			"version": 13,
			"versionNonce": 630387880,
			"isDeleted": true,
			"boundElements": null,
			"updated": 1665930256500,
			"link": null,
			"locked": false,
			"points": [
				[
					0,
					0
				],
				[
					0,
					0.6896551724137794
				],
				[
					0,
					1.3793103448276156
				],
				[
					0.6896551724137225,
					3.4482758620689538
				],
				[
					0.6896551724137225,
					7.586206896551744
				],
				[
					1.3793103448275588,
					13.103448275862092
				],
				[
					1.3793103448275588,
					22.068965517241395
				],
				[
					2.068965517241395,
					26.896551724137908
				],
				[
					2.068965517241395,
					29.655172413793082
				],
				[
					2.7586206896551175,
					30.344827586206918
				],
				[
					2.7586206896551175,
					30.344827586206918
				]
			],
			"pressures": [
				0.0244140625,
				0.25,
				0.2685546875,
				0.2861328125,
				0.3037109375,
				0.3095703125,
				0.3125,
				0.3134765625,
				0.296875,
				0.2509765625,
				0
			],
			"simulatePressure": false,
			"lastCommittedPoint": [
				2.7586206896551175,
				30.344827586206918
			]
		}
	],
	"appState": {
		"theme": "light",
		"viewBackgroundColor": "#ffffff",
		"currentItemStrokeColor": "#000000",
		"currentItemBackgroundColor": "transparent",
		"currentItemFillStyle": "hachure",
		"currentItemStrokeWidth": 1,
		"currentItemStrokeStyle": "solid",
		"currentItemRoughness": 0,
		"currentItemOpacity": 100,
		"currentItemFontFamily": 1,
		"currentItemFontSize": 28,
		"currentItemTextAlign": "left",
		"currentItemStrokeSharpness": "sharp",
		"currentItemStartArrowhead": null,
		"currentItemEndArrowhead": "arrow",
		"currentItemLinearStrokeSharpness": "round",
		"gridSize": null,
		"colorPalette": {}
	},
	"files": {}
}
```
%%